<?php
error_reporting(E_ALL);
include_once("config.php");
$yourSiteContent = array();
//$base_url = "http://www.vatsnew.co.in/";
$base_url = 'http://localhost/vats2';
 $res1 = "SELECT slug,date FROM post";
 $result1=mysqli_query($dbcon,$res1);
 while($data=mysqli_fetch_array($result1))
{
		   $url = $base_url.$data['slug'].".html";
		   $yourSiteContent[] = array(
			"loc" => $url, 
			"lastmod" => date($data['date']), 
			"changefreq" => "No",
			"priority" => 0.5
			 );
}

$tagArray = array();
 $res2 = "SELECT * from tag";
 $result2=mysqli_query($dbcon,$res2);
 while($data=mysqli_fetch_array($result2))
{
		   $url = $base_url.lcfirst($data['tag_name'])."-jobs".".html";
		   $tagArray[] = array(
			"loc" => $url, 
			"lastmod" => '',//date($data['date']), 
			"changefreq" => "No",
			"priority" => 0.5
			 );
}
$merge = array_merge($yourSiteContent, $tagArray); 



//Category Sitemap
$catArray = array();
$td_date = date("Y-m-d  h:m:s"); 
 $res3  = "SELECT DISTINCT category.cat_id,category.slug FROM `post` LEFT join category on post.cat_id = category.cat_id";
 $result3 = mysqli_query($dbcon,$res3);
 while($data=mysqli_fetch_array($result3))
{
		   $url = $base_url."jobs/".lcfirst($data['slug']).".html";
		   $catArray[] = array(
			"loc" => $url, 
			"lastmod" => $td_date,
			"changefreq" => "No",
			"priority" => 0.5
			 );
}
$merge2 = array_merge($merge, $catArray); 


//Qualification Sitemap
$qualifiArray = array();
$td_date = date("Y-m-d  h:m:s"); 
 $res4  = "SELECT * FROM `qualification`";
 $result4 = mysqli_query($dbcon,$res4);
 while($data=mysqli_fetch_array($result4))
{
		   $url = $base_url."hiring/".lcfirst($data['slug']).".html";
		   $qualifiArray[] = array(
			"loc" => $url, 
			"lastmod" => $td_date,
			"changefreq" => "No",
			"priority" => 0.5
			 );
}
$merge3 = array_merge($merge2, $qualifiArray);


//Area Sitemap
$areaArray = array();
$td_date = date("Y-m-d  h:m:s"); 
 $res5  = "SELECT * FROM `area`";
 $result5 = mysqli_query($dbcon,$res5);
 while($data=mysqli_fetch_array($result5))
{
		   $url = $base_url."career/".lcfirst($data['slug']).".html";
		   $areaArray[] = array(
			"loc" => $url, 
			"lastmod" => $td_date,
			"changefreq" => "No",
			"priority" => 0.5
			 );
}
$merge4 = array_merge($merge3, $areaArray);

//Role Sitemap
$roleArray = array();
$td_date = date("Y-m-d  h:m:s"); 
 $res6  = "SELECT * FROM `role`";
 $result6 = mysqli_query($dbcon,$res6);
 while($data=mysqli_fetch_array($result6))
{
		   $url = $base_url."opening/".lcfirst($data['slug']).".html";
		   $roleArray[] = array(
			"loc" => $url, 
			"lastmod" => $td_date,
			"changefreq" => "No",
			"priority" => 0.5
			 );
}
$merge5 = array_merge($merge4, $roleArray);


/*echo "<pre>";
print_r($yourSiteContent);*/

$xml = new DomDocument('1.0', 'utf-8'); 
$xml->formatOutput = true; 

// creating base node
$urlset = $xml->createElement('urlset'); 
$urlset -> appendChild(
    new DomAttr('xmlns', 'http://www.sitemaps.org/schemas/sitemap/0.9')
);

    // appending it to document
$xml -> appendChild($urlset);

// building the xml document with your website content
foreach($merge5  as $entry)
{

    //Creating single url node
    $url = $xml->createElement('url'); 

    //Filling node with entry info
    $url -> appendChild( $xml->createElement('loc', $entry['loc']) ); 
    $url -> appendChild( $lastmod = $xml->createElement('lastmod', $entry['lastmod']) ); 
    $url -> appendChild( $changefreq = $xml->createElement('changefreq', 'always') ); 
    $url -> appendChild( $priority = $xml->createElement('priority', '0.5') ); 

    // append url to urlset node
    $urlset -> appendChild($url);

}

$xml->save("sitemap.xml");

echo '<h3>Sitemap has been updated. Check the sitemap.xml file for urls or <a href="http://www.vatsnew.co.in/sitemap.xml">click here.</h3>';
?>
