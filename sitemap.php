<?php include_once('config.php');
	  include_once('function.php');
	  include_once('header.php');
	  include_once('abc.php');
?>
<html>
<head>
<title>Site Map</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<style>
* {
  box-sizing: border-box;
}
.menu {
  float: left;
  width: 20%;
}
.menuitem {
  padding: 8px;
  margin-top: 7px;
  border-bottom: 1px solid #f1f1f1;
}
.main2 {
  float: left;
  width: 60%;
  padding: 62px 33px;
  overflow: hidden;
  font-size: 12px;
  background: white;
  width: 100%;
}
.right {
  background-color: lightblue;
  float: left;
  width: 20%;
  padding: 10px 15px;
  margin-top: 7px;
}

@media only screen and (max-width:800px) {
  /* For tablets: */
  .main {
    width: 80%;
    padding: 0;
  }
  .right {
    width: 100%;
  }
}
@media only screen and (max-width:500px) {
  /* For mobile phones: */
  .menu, .main, .right {
    width: 100%;
  }
}
.title {
    text-decoration: underline;

}
 
</style>
</head>
<body>
<br>
<br>
<center><h1 class="title">SITE MAP</h1></center>
 <div class="main2">
<h2 class="title">POST</h2>
<br>
<div>
<?php
// start pagination
	 $per_page=50;
	 if(isset($_GET['page'])){
		 $page=$_GET['page'];
	 }else{
		$page=1; 
	 }
	 $start_form=($page-1) * $per_page;
 // end pagination
 
$res="SELECT * FROM post limit $start_form,$per_page";	
//echo $res; die;
$run=mysqli_query($dbcon,$res);
while($data=mysqli_fetch_assoc($run))
	
{
	?>
	<a href="single1.php?post_id=<?php echo $data['slug']; ?>"style="color:black" title="<?php echo $data['title'];?>"><?php echo $data['title'];?></a>
	<?php
	echo "|";
}
?>
<br>
<!--pagination Start-->
 <ul class="pagination">
	   <?php
	   $pagination_sql="SELECT * FROM post ";
	   $run_pagination=mysqli_query($dbcon,$pagination_sql);
	   $count=mysqli_num_rows($run_pagination);
	   $total_pages=ceil($count/$per_page);
	   for($i=1;$i<=$total_pages;$i++)
	   {
		   echo '<li><a href="?page='.$i.'">'.$i.'</a></li>';
	   }
	   ?>
			
 </ul>
			<!--End pagination -->
<br>
</div>
<div>
<h2 class="title">CATEGORY</h2>
<br>
<?php 
// start pagination
	 $per_page2=50;
	 if(isset($_GET['page2'])){
		 $page2=$_GET['page2'];
	 }else{
		$page2=1; 
	 }
	 $start_form=($page2-1) * $per_page2;
 // end pagination
$res="SELECT * FROM category limit $start_form,$per_page2";	
$run=mysqli_query($dbcon,$res);
while($data=mysqli_fetch_assoc($run))
		
{
	?>
	<a href="single.php?cat_id=<?php echo $data['slug']; ?>"style="color:black" title="<?php echo $data['cat_title'];?>"><?php echo $data['cat_title'];?></a>
	<?php
		echo "|";
}

?>
<br>
<!--pagination Start-->
 <ul class="pagination">
	   <?php
	   $pagination_sql="SELECT * FROM category ";
	   $run_pagination=mysqli_query($dbcon,$pagination_sql);
	   $count=mysqli_num_rows($run_pagination);
	   $total_pages=ceil($count/$per_page2);
	   for($i=1;$i<=$total_pages;$i++)
	   {
		   echo '<li><a href="?page2='.$i.'">'.$i.'</a></li>';
	   }
	   ?>
			
 </ul>
			<!--End pagination -->
<br>
</div>
<div>	
<h2 class="title">AREA</h2>
<br>
<?php 
// start pagination
	 $per_page3=50;
	 if(isset($_GET['page3'])){
		 $page3=$_GET['page3'];
	 }else{
		$page3=1; 
	 }
	 $start_form=($page3-1) * $per_page3;
 // end pagination
$res="SELECT * FROM area limit $start_form,$per_page3";	
$run=mysqli_query($dbcon,$res);
while($data=mysqli_fetch_assoc($run))
	
{
	 ?>
	<a href="single.php?area_id=<?php echo $data['slug']; ?>"style="color:black" title="<?php echo $data['area_name'];?>"><?php echo $data['area_name'];?></a>
	<?php 
	echo "|";
}
?>
<br>
<!--pagination Start-->
 <ul class="pagination">
	   <?php
	   $pagination_sql="SELECT * FROM area ";
	   $run_pagination=mysqli_query($dbcon,$pagination_sql);
	   $count=mysqli_num_rows($run_pagination);
	   $total_pages=ceil($count/$per_page3);
	   for($i=1;$i<=$total_pages;$i++)
	   {
		   echo '<li><a href="?page3='.$i.'">'.$i.'</a></li>';
	   }
	   ?>
			
 </ul>
			<!--End pagination -->
<br>
</div>
<div>
<h2 class="title">ROLE</h2>
<br>
<?php 
// start pagination
	 $per_page4=50;
	 if(isset($_GET['page4'])){
		 $page4=$_GET['page4'];
	 }else{
		$page4=1; 
	 }
	 $start_form=($page4-1) * $per_page4;
 // end pagination
$res="SELECT * FROM role limit $start_form,$per_page4";	
$run=mysqli_query($dbcon,$res);
while($data=mysqli_fetch_assoc($run))
	
{
	 ?>
	<a href="single.php?role_id=<?php echo $data['slug']; ?>"style="color:black" title="<?php echo $data['role_name'];?>"><?php echo $data['role_name'];?></a>
	<?php
		echo "|";
}
?>
<br>
<!--pagination Start-->
 <ul class="pagination">
	   <?php
	   $pagination_sql="SELECT * FROM role ";
	   $run_pagination=mysqli_query($dbcon,$pagination_sql);
	   $count=mysqli_num_rows($run_pagination);
	   $total_pages=ceil($count/$per_page4);
	   for($i=1;$i<=$total_pages;$i++)
	   {
		   echo '<li><a href="?page4='.$i.'">'.$i.'</a></li>';
	   }
	   ?>
			
 </ul>
			<!--End pagination -->
<br>
</div>
<div>
<h2 class="title">QUALIFICATION</h2>
<br>
<?php 
// start pagination
	 $per_page5=50;
	 if(isset($_GET['page5'])){
		 $page5=$_GET['page5'];
	 }else{
		$page5=1; 
	 }
	 $start_form=($page5-1) * $per_page5;
 // end pagination
$res="SELECT * FROM qualification limit $start_form,$per_page";	
$run=mysqli_query($dbcon,$res);
while($data=mysqli_fetch_assoc($run))
	
{
	 ?>
	<a href="single.php?qualification_id=<?php echo $data['slug']; ?>"style="color:black" title="<?php echo $data['qualification_name'];?>"><?php echo $data['qualification_name'];?></a>
	<?php
		echo "|";
}
?>
<br>
<!--pagination Start-->
 <ul class="pagination">
	   <?php
	   $pagination_sql="SELECT * FROM qualification ";
	   $run_pagination=mysqli_query($dbcon,$pagination_sql);
	   $count=mysqli_num_rows($run_pagination);
	   $total_pages=ceil($count/$per_page5);
	   for($i=1;$i<=$total_pages;$i++)
	   {
		   echo '<li><a href="?page5='.$i.'">'.$i.'</a></li>';
	   }
	   ?>
			
 </ul>
			<!--End pagination -->
<br>
</div>
<div>
<h2 class="title">TAG</h2>
<br>
<?php 
// start pagination
	 $per_page6=50;
	 if(isset($_GET['page6'])){
		 $page6=$_GET['page6'];
	 }else{
		$page6=1; 
	 }
	 $start_form=($page6-1) * $per_page6;
 // end pagination
$res="SELECT * FROM tag limit $start_form,$per_page";	
$run=mysqli_query($dbcon,$res);
while($data=mysqli_fetch_assoc($run))
	
{
	 ?>
	<a href="single.php?t_id=<?php echo  str_replace (' ', '-',rtrim($data['tag_name'])); ?>"style="color:black"title="<?php echo $data['tag_name'];?>"><?php echo $data['tag_name'];?></a>
	<?php echo " | "; ?>
	<?php $childtags = explode(",",$data['child_tags']);
	  //print_r($childtags);
	foreach($childtags as $val){ 
	?>
<a href="single.php?child_tag_id=<?php str_replace(' ', '-',$data['tag_name'])."-"; ?><?php echo strtolower($val); ?>"style="color:black"style="font-size: 12px;"><?php echo $val; ?></a>
			
<?php echo " | "; }} ?>
	
<br>
<!--pagination Start-->
 <ul class="pagination">
	   <?php
	   $pagination_sql="SELECT * FROM tag ";
	   $run_pagination=mysqli_query($dbcon,$pagination_sql);
	   $count=mysqli_num_rows($run_pagination);
	   $total_pages=ceil($count/$per_page6);
	   for($i=1;$i<=$total_pages;$i++)
	   {
		   echo '<li><a href="?page6='.$i.'">'.$i.'</a></li>';
	   }
	   ?>
			
 </ul>
			<!--End pagination -->
<br>
</div>
<div>
<h2 class="title">page</h2>
<br>
<?php 
$res="SELECT * FROM page";	
$run=mysqli_query($dbcon,$res);
while($data=mysqli_fetch_assoc($run))
	
{
	 ?>
	<a href="single.php?page_id=<?php echo $data['slug']; ?>"style="color:black" title="<?php echo $data['page_name'];?>"><?php echo $data['page_name'];?></a>
	<?php
		echo "|";
}

?>
</div>
</div>
<div>
<font color="white">.</font>
</div>

</body>
<?php require_once('footer-new.php');?>
</html>
