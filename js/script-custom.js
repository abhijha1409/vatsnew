
jQuery.noConflict();

(function($) {
	$(function() {

		//Hack royal slider				
		$('.royalSlider').css('display', 'block');

		
		
				// Top ticker
		window.setInterval(function() { 
			/* $('.ticker').each(function () {
				var ticker = $(this);
				ticker.find('li:first').animate( {marginTop: '-' + ticker.height() + 'px'}, 800, function() {
					$(this).detach().appendTo(ticker);
					removeAttr('style');	
				});	
			});		 */		

			}, 4000
		);
		
		// Main feature slider
		$('#main-slider').royalSlider({
			autoHeight: true,
			arrowsNav: false,
			fadeinLoadedSlide: false,
			//controlNavigationSpacing: 4,
			controlNavigation: 'thumbnails',
			//controlsInside: true,
			imageScaleMode: 'none',
			imageAlignCenter:false,
			slidesSpacing: 15,
			loop: true,
			loopRewind: true,
			numImagesToPreload: 1,
			keyboardNavEnabled: true,
			usePreloader: true,
			thumbs: {
				// thumbnails options go gere
				spacing: 0,
				arrows: true,
				arrowsAutoHide: false//,
				//appendSpan: true,
			}
			
		});
		
		// Secondary slider
		$('#secondary-slider').royalSlider({
			autoHeight: true,
			arrowsNav: false,
			fadeinLoadedSlide: false,
			controlNavigationSpacing: 4,
			controlNavigation: 'tabs',
			controlsInside: true,
			imageScaleMode: 'none',
			imageAlignCenter:false,
			slidesSpacing: 15,
			loop: true,
			loopRewind: true,
			numImagesToPreload: 1,
			keyboardNavEnabled: false,
			usePreloader: true,
			transitionType: 'fade'
						, autoPlay: {
				// autoplay options go gere
				enabled: true,
				pauseOnHover: true,
				delay: 60000			}
						

		});
		
		$('#archive-slider').royalSlider({
			autoHeight: true,
			arrowsNav: false,
			fadeinLoadedSlide: false,
			//controlNavigationSpacing: 4,
			controlNavigation: 'thumbnails',
			//controlsInside: true,
			imageScaleMode: 'none',
			imageAlignCenter:false,
			slidesSpacing: 15,
			loop: true,
			loopRewind: true,
			numImagesToPreload: 1,
			keyboardNavEnabled: true,
			usePreloader: true,
			thumbs: {
				// thumbnails options go gere
				spacing: 10,
				arrows: true,
				arrowsAutoHide: false//,
				//appendSpan: true,
			}
						, autoPlay: {
				// autoplay options go gere
				enabled: true,
				pauseOnHover: true,
				delay: 60000			}
			
		});

				$('#related-post').royalSlider({
			autoHeight: true,
			arrowsNav: false,
			fadeinLoadedSlide: false,
			//controlNavigationSpacing: 4,
			controlNavigation: 'thumbnails',
			//controlsInside: true,
			startSlideId: 2,
			imageScaleMode: 'none',
			imageAlignCenter:false,
			slidesSpacing: 15,
			loop: true,
			loopRewind: true,
			numImagesToPreload: 1,
			keyboardNavEnabled: true,
			usePreloader: true,
			thumbs: {
				// thumbnails options go gere
				spacing: 15,
				arrows: true,
				arrowsAutoHide: false//,
				//appendSpan: true,
			}			

		});
		

		convert_to_slug = function (text) {
		    return text.toLowerCase().replace(/[^\w ]+/g,'').replace(/ +/g,'-');
		}


		isScrolledIntoView = function (elem) {
		    var docViewTop = $(window).scrollTop();
		    var docViewBottom = docViewTop + $(window).height();

		    var elemTop = $(elem).offset().top;
		    var elemBottom = elemTop + $(elem).height();

		    return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
		}
		
					$top_menu_indicator = $('#main-menu-indicator');
			$top_menu_container = $('#main-menu-container');

			if ($top_menu_indicator.length > 0) {
				$(window).scroll(function() {
					
					
					if (!isScrolledIntoView($top_menu_indicator)) 
						$top_menu_container.addClass('fixed');
					else 
						$top_menu_container.removeClass('fixed');
				});
			}
		
				$single_popular_ajax_load = true;
		$popular_ajax_indicator = $('#popular-ajax-indicator');
		$popular_ajax_container = $('#popular-ajax-container');
		if ($popular_ajax_indicator.length > 0) {

			$(window).scroll(function() {
				
				
				if (isScrolledIntoView($popular_ajax_indicator) && $single_popular_ajax_load) {

					$obj = $popular_ajax_container;

					$data = {
		                'action': 'dlh_popular_ajax',
		                'nonce': dlh_ajax.ajax_nonce,
		                'already_posted': $obj.data('already-posted'),
		                'post_per_page': $obj.data('post-per-page'),
		                'paged': $obj.data('paged')                          
	                };

	                //alert(dlh_ajax.ajax_url + '?' + $.param ($data));

					$.ajax({
	                    url: dlh_ajax.ajax_url,
	                    type : 'post',
	                    dataType : 'json',
	                    data : $data,
	                    beforeSend: function () {
	                    	$single_popular_ajax_load = false; //preverent multiple load in scroll until ajax success
	                    	//Loading here
	                    	$popular_ajax_indicator.find('.loading').show();
	                    },
	                    success: function(response) {
	                        if(response.type == "success") {
	                        	if ($obj.data('paged') == 1) {
	                         		$obj.find('h5').show();
	                         	}
	                         	$obj.find('ul').append(response.output);  
	                         	$obj.data('paged', $obj.data('paged') + 1);   
	                         	$single_popular_ajax_load = true;      
	                         	$popular_ajax_indicator.find('.loading').hide();          
	                        } else {
	                        	$popular_ajax_indicator.find('.loading').hide();
	                        	$single_popular_ajax_load = false; 
	                        }
	                    }
	                });
				}			
				
			});
		}
		
				$archive_ajax_load = true;
		$archive_ajax_indicator = $('#archive-ajax-indicator');
		$archive_ajax_container = $('#archive-ajax-container');
		$archive_pagenavi = $('.pagenavi');
		
		if ($archive_ajax_indicator.length > 0) {
			
			$archive_pagenavi.remove();

			$(window).scroll(function() {				
				
				if (isScrolledIntoView($archive_ajax_indicator) && $archive_ajax_load) {

					$obj = $archive_ajax_container;

					$global_query_var = jQuery.parseJSON( $obj.find('#global-query-var').html());

					//alert($global_query_var);
					//alert($obj.data('paged'));

					$data = {
		                'action': 'dlh_archive_ajax',
		                'nonce': dlh_ajax.ajax_nonce,
		                'paged': $obj.data('paged'),
		                'global_query_var': $global_query_var         
	                };

	                //alert(dlh_ajax.ajax_url + '?' + $.param ($data));

					$.ajax({
	                    url: dlh_ajax.ajax_url,
	                    type : 'post',
	                    dataType : 'json',
	                    data : $data,
	                    beforeSend: function () {
	                    	$archive_ajax_load = false; //preverent multiple load in scroll until ajax success
	                    	//Loading here
	                    	$archive_ajax_indicator.find('.loading').show();
	                    },
	                    success: function(response) {
	                        if(response.type == "success") {
	                        
	                         	$obj.append(response.output);  
	                         	$obj.data('paged', $obj.data('paged') + 1);   
	                         	$archive_ajax_load = true;      
	                         	$archive_ajax_indicator.find('.loading').hide();          
	                        } else {
	                        	$archive_ajax_indicator.find('.loading').hide();
	                        	$archive_ajax_load = false; 	                        	
	                        }
	                    }
	                });
				}			
				
			});
		}
		
		$window = $(window);
		$is_push_menu_init = false;
		mobile_menu_init = function () {
			windowsize = $window.width();
	        if (windowsize <= 767 && !$is_push_menu_init) {
	        	
				$mobile_menu = $( '#mobile-menu' );
				$mobile_menu_nav = $( '#mobile-menu nav' );
				$mobile_menu_expand = $( '#mobile-menu-expand' );

				$cloned_menu = $('#main-nav').clone(true).off().removeAttr('id').removeAttr('class').appendTo($mobile_menu_nav );
				
				$cloned_sf_megas = $cloned_menu.find('.sf-mega');
				$cloned_sf_megas.each(function () {
					var $cloned_sf_mega = $(this);
					var $cloned_sf_mega_ul = $cloned_sf_mega.find('ul');
					$cloned_sf_mega.after($cloned_sf_mega_ul);
					$cloned_sf_mega.remove();
				});			

				$mobile_menu.multilevelpushmenu({
					containersToPush: [$( '#main-wrapper' )],
										collapsed: true,
					fullCollapse: true,
					mode: 'cover',
					backItemIcon: 'small icon', 
					groupIcon: 'small icon', 
					preventItemClick: false					
				});

				$mobile_menu_expand.click(function(){

					$root_menu = $mobile_menu.multilevelpushmenu( 'findmenusbytitle' , 'mobile-root-menu' ).first();
					
					if ($mobile_menu.multilevelpushmenu('menuexpanded', $root_menu )) {
						$mobile_menu.multilevelpushmenu( 'collapse' );
						$( '#main-wrapper' ).css({
							'height': '', 
							'overflow': ''
						});
						$( 'body' ).css({
							'overflow-x': ''
						});
					} else {
						$mobile_menu.multilevelpushmenu( 'expand' );
						$( '#main-wrapper' ).css({
							'height': $mobile_menu.height(), 
							'overflow': 'hidden'
						});
						$( 'body' ).css({
							'overflow-x': 'hidden'
						});
					}

				});

				$is_push_menu_init = true;
				//$window.off("resize", mobile_menu_init);
			}

			if (windowsize > 767) {
				$( '#main-wrapper' ).attr('data-style', $( '#main-wrapper' ).attr('style'));
				$( '#main-wrapper' ).removeAttr('style');
			} else if (windowsize <= 480) {
				$( '#main-wrapper' ).attr('style', $( '#main-wrapper' ).attr('data-style'));
				$( '#main-wrapper' ).removeAttr('data-style');
			}

		}

		mobile_menu_init();	   

	    $(window).resize(mobile_menu_init);

	   	$superfish_selector = '#main-nav';
		$($superfish_selector).superfish({
			'onShow' : function () {

				$content_menu = $( this ).find( ".content-menu" );
				
				$categories = $( this ).find( "#column-0 a" );
				
				$data_categories = new Array();

				//$i = 0;
				$categories.each(function () {
					$data_categories.push(convert_to_slug($(this).text()));
					//if ($i == 3) { return false;}
					//else { $i++; }					
				});

				$loading = $content_menu.find('.loading');
				
				//alert(decodeURIComponent($.param({'action': 'dlh_custom_mega_menu', 'nonce': dlh_ajax.ajax_nonce, 'data_categories': $data_categories})));
				
				if ($loading.length > 0 && $loading.css('display') == 'none') {

					//alert($content_menu.closest('li').attr('id'));

					$data = {
						'action': 'dlh_custom_mega_menu',
						'nonce': dlh_ajax.ajax_nonce,
						'data_categories': $data_categories,
						'selector': $content_menu.closest('li').attr('id')
					};
					
					//alert(dlh_ajax.ajax_url + '?' + $.param ($data));
					
					$.ajax({
						url: dlh_ajax.ajax_url,
						type : 'post',
						dataType : 'json',
						//async: false,
						data : $data,
						beforeSend: function () {
	                    	$loading.show();	                    	
	                    },
						success: function(response) {
							if(response.type == "success") {
								$content_menu_selector = $superfish_selector + ' #' + response.selector + ' ' + '.content-menu';
								
								//alert($content_menu_selector);
								
								$( $content_menu_selector).append(response.content_menu);
								
								//$content_menu.append(response.content_menu);
								
								$( $content_menu_selector).find('.loading').remove();
							}
						}
					});
				}
			}
		});

		// Read more
		dlh_readmore_init = function () {
			$readmores = $('.readmore:not(.readmore-done)');
			$readmores.each(function () {
				$readmore = $(this);
				$radmore_text = $readmore.text();
				$replace_readmore = $('<a class="readmore readmore-done" href="' + $readmore.data('permalink') + '">' + $radmore_text + '</a>');
				$readmore.after($replace_readmore);
				$readmore.remove();
			});
		}
		jQuery(document)
		    .ready(dlh_readmore_init)
		    .ajaxStop(dlh_readmore_init);
		
		
	    		//Post meta ajax
		dlh_meta_init = function () {
			
			$dlh_titles = $('.dlh-title:not(.dlh-meta-done)');
			$dlh_titles.each(function (index) {
				$dlh_title = $(this);
				
				$dlh_title_a = $dlh_title.find('a');

				if ($dlh_title_a.length > 0) {

					$data_post_id = $dlh_title.data('post-id');
					$more_icon = $('<span id="dlh-meta-trigger-'+$data_post_id+'" class="small icon dlh-meta-trigger"></span>');
					
					$dlh_title_a.append($more_icon);
					
					$dlh_title_a.find('.dlh-meta-trigger').mouseenter(
						function() {				  
							$data_post_id = $(this).closest('.dlh-title').data('post-id');
							
							$dlh_meta = $('#dlh-meta-' + $data_post_id);

							$dlh_meta_trigger = $('#dlh-meta-trigger-' + $data_post_id);

							if (!$dlh_meta_trigger.hasClass('trigger-done') && $dlh_meta.length == 0) {

								$dlh_meta_trigger_x = $dlh_meta_trigger.offset().left;
								$dlh_meta_trigger_y = $dlh_meta_trigger.offset().top;
								$dlh_meta_trigger_width = $dlh_meta_trigger.outerWidth();

								$data = {
									'action': 'dlh_meta_posts',
									'nonce': dlh_ajax.ajax_nonce,
									'data_post_id': $data_post_id
								};

								//alert(dlh_ajax.ajax_url + '?' + $.param ($data));

								$.ajax({
									url: dlh_ajax.ajax_url,
									type : 'post',
									dataType : 'json',
									data : $data,
									beforeSend: function () {

				                    },
									success: function(response) {
										if(response.type == "success") {
											
											$('body').append(response.content_menu);
											
											$dlh_meta = $('#dlh-meta-' + response.post_id);

											$dlh_meta_trigger = $('#dlh-meta-trigger' + response.post_id);
											
											$dlh_meta_height = $dlh_meta.outerHeight();
											$dlh_meta_width = $dlh_meta.outerWidth();
											$dlh_meta_x = $dlh_meta_trigger_x - ($dlh_meta_width / 2) + ($dlh_meta_trigger_width / 2);
											$dlh_meta_y = $dlh_meta_trigger_y - $dlh_meta_height;

											$dlh_meta.css({
												left: $dlh_meta_x,
												top: $dlh_meta_y
											});

											$dlh_meta_trigger = $('#dlh-meta-trigger-' + response.post_id);
											$dlh_meta_trigger.addClass('trigger-done');	
											
											$hide_dlh_meta = function () {
												$('.dlh-meta-trigger').removeClass('active');
												$('.dlh-meta').hide();
											}

											$show_dlh_meta = function () {
												$dlh_meta_trigger.addClass('active');
												$dlh_meta.show();
											}

											$hide_dlh_meta();
											$show_dlh_meta();

											$dlh_meta.find('.dlh-meta-remove').click($hide_dlh_meta);

																						FB.XFBML.parse();
											
																						$.ajax({ url: 'http://platform.twitter.com/widgets.js', dataType: 'script', cache:true});
											
											
											
										}
									}
								});
							} else {
								//$dlh_meta = $('#dlh-meta-' + $data_post_id);
								$dlh_meta_y = $dlh_meta_trigger.offset().top - $dlh_meta_height;

								$dlh_meta.css({
									top: $dlh_meta_y
								});
								
								$hide_dlh_meta = function () {
									$('.dlh-meta-trigger').removeClass('active');
									$('.dlh-meta').hide();
								}
								$show_dlh_meta = function () {
									$dlh_meta_trigger.addClass('active');
									$dlh_meta.show();
								}

								$hide_dlh_meta();

								$show_dlh_meta();

								$dlh_meta.find('.dlh-meta-remove').click($hide_dlh_meta);
							}

						}
					);		
				} 

				$dlh_title.addClass('dlh-meta-done');
			
			});
			
		}

		jQuery(document)
		    .ready(dlh_meta_init)
		    .ajaxStop(dlh_meta_init);

	    

		$(".dlh-weather").each(function () {
			$(this).dlh_weather({
				translation: [
	                'Updating',
	                'Change City',
	                'today',
	                'tomorrow',
	                'Forecast',
	                'Details',
	                'pressure',
	                'hPa',
	                'humidity',
	                'speed',
	                'm/s',
	                'deg',
	                'clouds',
	                'Sun',
	                'Mon',
	                'Tue',
	                'Wed',
	                'Thu',
	                'Fri',
	                'Sat',
	                'no weather report was found for that place!',
	                'something went wrong!'
	            ],
				units: 'metric'
			});
		});		

		/**
	     * Generate star rating
	     */
	    function dlh_star_rating() {
	        jQuery(".dlh-star-rating").each(function() {
	            var star_rating = jQuery(this);
	            star_rating.find('.rating').rating({    
	                callback: function(value, link){  

                        postID = jQuery(link).parents(".dlh-star-rating").data("post_id");    
                        $data = {
                            action: "dlh-star-rating",
                            nonce : dlh_ajax.ajax_nonce,
                            post_rate: value,
                            post_id:postID
                        };
                        console.log (dlh_ajax.ajax_url+'?'+jQuery.param( $data ));
                        jQuery.ajax({    
	                        type: "post",    
	                        url: dlh_ajax.ajax_url,
	                        dataType : "json",
	                        data : $data,  
	                        beforeSend: function() {
	                            star_rating.find(".dlh-star-rating-loading").show();
	                            star_rating.find(".dlh-star-rating-container").hide();

	                        },
	                        complete: function(){
	                            star_rating.find(".dlh-star-rating-loading").hide();
	                            star_rating.find(".dlh-star-rating-container").show();                    
	                        },

	                        success: function(response){ 
	                        	//alert(JSON.stringify(response));
	                            if (response.status == 'voted') {
	                            	star_rating.find(".dlh-star-rating-average").text(response.average);
	                                star_rating.find(".dlh-star-rating-vote-count").text(response.votes_count);	                                
	                            } else if (response.status == 'already voted') {
	                                var star_rating_alert = star_rating.find(".dlh-star-rating-alert");
	                               star_rating_alert.text('You have already voted.');
	                               star_rating_alert.fadeIn(400, function() {
	                                        star_rating_alert.delay(2000).fadeOut(400);
	                                });
	                            } 	                            
	                        }    
	                    });    
	                }    

	            });
	        });
	    }
	    dlh_star_rating();	
	  	
	});
})(jQuery);