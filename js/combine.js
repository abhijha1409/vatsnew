(function( $ ){
    $.fn.dlh_weather = function(options){
        var defaults = {
            translation: [
                'Updating weather data',
                'Change City',
                'today',
                'tomorrow',
                'Next 4 Day Forecast',
                'Current Details',
                'pressure',
                'hPa',
                'humidity',
                'speed',
                'm/s',
                'deg',
                'clouds',
                'Sun',
                'Mon',
                'Tue',
                'Wed',
                'Thu',
                'Fri',
                'Sat',
                'no weather report was found for that place!',
                'something went wrong!'
            ],
            // noweather   : '<p>no weather report was found for that place!</p>',
            // error       : '<p>something went wrong!</p>',
            // latitude    : 0,
            // longitude   : 0,
            units       : 'imperial',
            lang        : 'en',
            // iconset     : 'http://openweathermap.org/img/w/'
        };

        var settings = $.extend(defaults, options);

        var logic = function (item) {
            // var item = $(this);
            var openweathermap_url = "http://api.openweathermap.org/data/2.5/forecast/daily";

            if(item.attr("data-dlh-weather-city")){
                openweathermap_url += "?q=" + item.attr("data-dlh-weather-city");
            } 
            // else if(item.attr("data-dlh-weather-coord")){
            //     latlon = item.attr("data-dlh-weather-coord").split(',');
            //     openweathermap_url += "?lat="+latlon[0]+"&lon="+latlon[1];
            // } else if(settings.latitude != 0 && settings.longitude != 0){
            //     openweathermap_url += "?lat="+settings.latitude+"&lon="+settings.longitude;
            // }

            if(settings.lang != ''){
                openweathermap_url += "&lang="+settings.lang;
            }

            if(settings.units == 'metric'){
                openweathermap_url += "&units="+'metric';
            } else{
                openweathermap_url += "&units="+'imperial';
            }

            openweathermap_url += "&cnt=7&mode=json";            

            console.log(openweathermap_url);
            
            $.ajax({
                type : "GET",
                dataType : "jsonp",
                url : openweathermap_url,
                cache: true,
                beforeSend: function () {
                    var template = 
                    '<div>'+
                        '<span class="loading"></span><span class="loading-text">'+settings.translation[0]+'</span>'+
                        '<div class="clear"></div>'+
                    '</div>';
                    item.html(template);            
                },
                success : function(weather){
                    if(!weather){
                        item.html(settings.translation[20]);
                        return;
                    }

                    item.addClass('dlh-weather');
                    if (weather.city.name.length == 0) logic(item);

                    var template = 
                    '<div>'+
//                        '<div class="place">'+
//                            weather.city.name +                            
//                        '</div>'+
                        '<div class="change-city">'+
                            '<span class="trigger">'+
                                '<span class="icon small"></span> '+weather.city.name+
                            '</span>'+
                            '<ul class="city-lists">';
                                // console.log(item.attr("data-dlh-weather-city-lists"));
                                var city_lists = item.attr("data-dlh-weather-city-lists");
                                // alert(city_lists);
                                city_lists = city_lists.split(',');
                                $.each(city_lists, function (index, value) {
                                    template += '<li>'+value+'</li>';
                                });
                            
                            template +=
                            '</ul>'+
                        '</div>'+
                        '<div class="clear"></div>'+
                    '</div>'+
                    '<div>';

                    for (var i = 0; i <= 1; i++) {
                        var day = 'today';
                        if (i == 1) day = 'tomorrow';
                        var day_translation = settings.translation[2];
                        if (i == 1) day_translation = settings.translation[3];
                        

                        var temperature, temp_min, temp_max; 
                        temp_min = weather.list[i].temp.min.toFixed(0);
                        temp_max = weather.list[i].temp.max.toFixed(0);

                        if (temp_min == temp_max) {
                            temperature = temp_max+ 'ÂºC';
                        } else {
                            var separator = '-';
                            if (temp_min < 0 || temp_max < 0)  separator = '/';
                            temperature = temp_min + 'ÂºC'+ separator + temp_max+ 'ÂºC';
                        }

                        template += 
                        '<div class="'+day+'">'+
                            '<span class="w-'+weather.list[i].weather[0].icon+' w-icon"></span>'+       
                            '<div class="temperature">' + temperature + '</div>'+
                            '<div class="day">'+day_translation+'</div>'+
                            '<div class="description">' + weather.list[i].weather[0].description + '</div>'+
                            '<div class="clear"></div>'+               
                        '</div>';
//                        if (i == 0)  template += '<span class="separator"></span>';
                    };

                    template += 
                        '<div class="clear"></div>'+
                    '</div>'+
                    '<div class="clear"></div>'+
                    '<ul class="tabs-link">'+
                        '<li id="forecast">'+settings.translation[4]+'<span class="icon small"></span></li>'+
                        '<li id="detail">'+settings.translation[5]+'<span class="icon small"></span></li>'+
                    '</ul>'+
                    '<div class="clear"></div>';

                    item.html(template);

                    change_city_action(item);

                    forecast_action (item, weather);

                    detail_action (item, weather);
                },
                error : function(){
                    item.html(settings.translation[21]);
                }
            });
        };


        var detail_action = function (item, weather) {
            var detail = item.find('#detail');
            detail.click(function () {
                var forecast_content = item.find('#forecast-content');
                forecast_content.hide();
                var detail_content = item.find('#detail-content');
                if (detail_content.length > 0) {detail_content.show();}
                else {
                    var template = 
                    '<div id="detail-content">';

                    template += 
                    '<table>'+
                        '<tr class="even">'+
                            '<td>'+settings.translation[6]+'</td>'+
                            '<td>'+weather.list[0].pressure+ ' '+settings.translation[7]+'</td>'+
                        '</tr>'+
                        '<tr class="odd">'+
                            '<td>'+settings.translation[8]+'</td>'+
                            '<td>'+weather.list[0].humidity+ ' %'+'</td>'+
                        '</tr>'+
                        '<tr class="even">'+
                            '<td>'+settings.translation[9]+'</td>'+
                            '<td>'+Math.round(weather.list[0].speed) + ' '+settings.translation[10]+'</td>'+
                        '</tr>'+
                        '<tr class="odd">'+
                            '<td>'+settings.translation[11]+'</td>'+
                            '<td>'+weather.list[0].deg +'</td>'+
                        '</tr>'+
                        '<tr class="even">'+
                            '<td>'+settings.translation[12]+'</td>'+
                            '<td>'+weather.list[0].clouds+ ' %'+'</td>'+
                        '</tr>'+
                    '</table>';

                    template += 
                        '<div class="clear"></div>'+
                    '</div>';

                    item.append(template);
                }
            });
        };

        var format_day_name = function (unixtimestamp) {
            var weekdays = new Array(7);
            weekdays[0] = settings.translation[13];
            weekdays[1] = settings.translation[14];
            weekdays[2] = settings.translation[15];
            weekdays[3] = settings.translation[16];
            weekdays[4] = settings.translation[17];
            weekdays[5] = settings.translation[18];
            weekdays[6] = settings.translation[19];

            var xx = new Date();
            xx.setTime(unixtimestamp*1000); // javascript timestamps are in milliseconds
            return weekdays[xx.getDay()];
        };

        var forecast_action = function (item, weather) {
            var forecast = item.find('#forecast');
            forecast.click(function () {
                var detail_content = item.find('#detail-content');
                detail_content.hide();
                var forecast_content = item.find('#forecast-content');
                if (forecast_content.length > 0) {forecast_content.show();}
                else {
                    var template = 
                    '<div id="forecast-content">';
                        
                        for (var i = 2; i <= 5; i++) {
                            var last = '';
                            if (i == 5) last = ' last';
                            
                            var day_name = format_day_name(weather.list[i].dt);

                            template += 
                            '<div class="forecast-day'+last+'">'+
                                '<div class="day-name">'+
                                    day_name +
                                '</div>'+
                                '<div class="temp-max">'+
                                    weather.list[i].temp.max.toFixed(1) + 'ÂºC' +
                                '</div>'+
                                '<div class="w-'+weather.list[i].weather[0].icon+' w-icon"></div>'+ 
                                '<div class="temp-min">'+
                                    weather.list[i].temp.min.toFixed(1) + 'ÂºC' +
                                '</div>'+
                            '</div>';
//                            if (i < 5)  template += '<span class="separator"></span>';
                        }

                    template += 
                    '<div class="clear"></div>'+
                    '</div>';

                    item.append(template);
                }
            });
        };

        var change_city_action = function (item) {
            var change_city = item.find('.change-city');
            var trigger = change_city.find('.trigger');
            var city_lists = change_city.find('.city-lists');
            var city_lists_li = city_lists.find('li');
            trigger.click(function () {
                city_lists.show();
            });

            city_lists.find('li').each(function () {
                $(this).click(function () {
                    item.html('');
                    item.attr('data-dlh-weather-city', $(this).html());
                    logic(item);
                });
            });
            
        };

        return this.each(function() {
            logic($(this));
        });
    }
})( jQuery );


/*
 ### jQuery Star Rating Plugin v3.14 - 2012-01-26 ###
 * Home: http://www.fyneworks.com/jquery/star-rating/
 * Code: http://code.google.com/p/jquery-star-rating-plugin/
 *
    * Dual licensed under the MIT and GPL licenses:
 *   http://www.opensource.org/licenses/mit-license.php
 *   http://www.gnu.org/licenses/gpl.html
 ###
*/

/*# AVOID COLLISIONS #*/
;if(window.jQuery) (function($){
/*# AVOID COLLISIONS #*/
    
    // IE6 Background Image Fix
    if ($.browser.msie) try { document.execCommand("BackgroundImageCache", false, true)} catch(e) { };
    // Thanks to http://www.visualjquery.com/rating/rating_redux.html
    
    // plugin initialization
    $.fn.rating = function(options){
        if(this.length==0) return this; // quick fail
        
        // Handle API methods
        if(typeof arguments[0]=='string'){
            // Perform API methods on individual elements
            if(this.length>1){
                var args = arguments;
                return this.each(function(){
                    $.fn.rating.apply($(this), args);
    });
            };
            // Invoke API method handler
            $.fn.rating[arguments[0]].apply(this, $.makeArray(arguments).slice(1) || []);
            // Quick exit...
            return this;
        };
        
        // Initialize options for this call
        var options = $.extend(
            {}/* new object */,
            $.fn.rating.options/* default options */,
            options || {} /* just-in-time options */
        );
        
        // Allow multiple controls with the same name by making each call unique
        $.fn.rating.calls++;
        
        // loop through each matched element
        this
         .not('.star-rating-applied')
            .addClass('star-rating-applied')
        .each(function(){
            
            // Load control parameters / find context / etc
            var control, input = $(this);
            var eid = (this.name || 'unnamed-rating').replace(/\[|\]/g, '_').replace(/^\_+|\_+$/g,'');
            var context = $(this.form || document.body);
            
            // FIX: http://code.google.com/p/jquery-star-rating-plugin/issues/detail?id=23
            var raters = context.data('rating');
            if(!raters || raters.call!=$.fn.rating.calls) raters = { count:0, call:$.fn.rating.calls };
            var rater = raters[eid];
            
            // if rater is available, verify that the control still exists
            if(rater) control = rater.data('rating');
            
            if(rater && control)//{// save a byte!
                // add star to control if rater is available and the same control still exists
                control.count++;
                
            //}// save a byte!
            else{
                // create new control if first star or control element was removed/replaced
                
                // Initialize options for this rater
                control = $.extend(
                    {}/* new object */,
                    options || {} /* current call options */,
                    ($.metadata? input.metadata(): ($.meta?input.data():null)) || {}, /* metadata options */
                    { count:0, stars: [], inputs: [] }
                );
                
                // increment number of rating controls
                control.serial = raters.count++;
                
                // create rating element
                rater = $('<span class="star-rating-control"/>');
                input.before(rater);
                
                // Mark element for initialization (once all stars are ready)
                rater.addClass('rating-to-be-drawn');
                
                // Accept readOnly setting from 'disabled' property
                if(input.attr('disabled') || input.hasClass('disabled')) control.readOnly = true;
                
                // Accept required setting from class property (class='required')
                if(input.hasClass('required')) control.required = true;
                
                // Create 'cancel' button
                /*rater.append(
                    control.cancel = $('<div class="rating-cancel"><a title="' + control.cancel + '">' + control.cancelValue + '</a></div>')
                    .mouseover(function(){
                        $(this).rating('drain');
                        $(this).addClass('star-rating-hover');
                        //$(this).rating('focus');
                    })
                    .mouseout(function(){
                        $(this).rating('draw');
                        $(this).removeClass('star-rating-hover');
                        //$(this).rating('blur');
                    })
                    .click(function(){
                     $(this).rating('select');
                    })
                    .data('rating', control)
                );*/
                
            }; // first element of group
            
            // insert rating star
            var star = $('<div class="star-rating rater-'+ control.serial +'"><a title="' + (this.title || this.value) + '">' + this.value + '</a></div>');
            rater.append(star);
            
            // inherit attributes from input element
            if(this.id) star.attr('id', this.id);
            if(this.className) star.addClass(this.className);
            
            // Half-stars?
            if(control.half) control.split = 2;
            
            // Prepare division control
            if(typeof control.split=='number' && control.split>0){
                var stw = ($.fn.width ? star.width() : 0) || control.starWidth;
                var spi = (control.count % control.split), spw = Math.floor(stw/control.split);
                star
                // restrict star's width and hide overflow (already in CSS)
                .width(spw)
                // move the star left by using a negative margin
                // this is work-around to IE's stupid box model (position:relative doesn't work)
                .find('a').css({ 'margin-left':'-'+ (spi*spw) +'px' })
            };
            
            // readOnly?
            if(control.readOnly)//{ //save a byte!
                // Mark star as readOnly so user can customize display
                star.addClass('star-rating-readonly');
            //}  //save a byte!
            else//{ //save a byte!
             // Enable hover css effects
                star.addClass('star-rating-live')
                 // Attach mouse events
                    .mouseover(function(){
                        $(this).rating('fill');
                        $(this).rating('focus');
                    })
                    .mouseout(function(){
                        $(this).rating('draw');
                        $(this).rating('blur');
                    })
                    .click(function(){
                        $(this).rating('select');
                    })
                ;
            //}; //save a byte!
            
            // set current selection
            if(this.checked)    control.current = star;
            
            // set current select for links
            if(this.nodeName=="A"){
    if($(this).hasClass('selected'))
     control.current = star;
   };
            
            // hide input element
            input.hide();
            
            // backward compatibility, form element to plugin
            input.change(function(){
    $(this).rating('select');
   });
            
            // attach reference to star to input element and vice-versa
            star.data('rating.input', input.data('rating.star', star));
            
            // store control information in form (or body when form not available)
            control.stars[control.stars.length] = star[0];
            control.inputs[control.inputs.length] = input[0];
            control.rater = raters[eid] = rater;
            control.context = context;
            
            input.data('rating', control);
            rater.data('rating', control);
            star.data('rating', control);
            context.data('rating', raters);
  }); // each element
        
        // Initialize ratings (first draw)
        $('.rating-to-be-drawn').rating('draw').removeClass('rating-to-be-drawn');
        
        return this; // don't break the chain...
    };
    
    /*--------------------------------------------------------*/
    
    /*
        ### Core functionality and API ###
    */
    $.extend($.fn.rating, {
        // Used to append a unique serial number to internal control ID
        // each time the plugin is invoked so same name controls can co-exist
        calls: 0,
        
        focus: function(){
            var control = this.data('rating'); if(!control) return this;
            if(!control.focus) return this; // quick fail if not required
            // find data for event
            var input = $(this).data('rating.input') || $( this.tagName=='INPUT' ? this : null );
   // focus handler, as requested by focusdigital.co.uk
            if(control.focus) control.focus.apply(input[0], [input.val(), $('a', input.data('rating.star'))[0]]);
        }, // $.fn.rating.focus
        
        blur: function(){
            var control = this.data('rating'); if(!control) return this;
            if(!control.blur) return this; // quick fail if not required
            // find data for event
            var input = $(this).data('rating.input') || $( this.tagName=='INPUT' ? this : null );
   // blur handler, as requested by focusdigital.co.uk
            if(control.blur) control.blur.apply(input[0], [input.val(), $('a', input.data('rating.star'))[0]]);
        }, // $.fn.rating.blur
        
        fill: function(){ // fill to the current mouse position.
            var control = this.data('rating'); if(!control) return this;
            // do not execute when control is in read-only mode
            if(control.readOnly) return;
            // Reset all stars and highlight them up to this element
            this.rating('drain');
            this.prevAll().andSelf().filter('.rater-'+ control.serial).addClass('star-rating-hover');
        },// $.fn.rating.fill
        
        drain: function() { // drain all the stars.
            var control = this.data('rating'); if(!control) return this;
            // do not execute when control is in read-only mode
            if(control.readOnly) return;
            // Reset all stars
            control.rater.children().filter('.rater-'+ control.serial).removeClass('star-rating-on').removeClass('star-rating-hover');
        },// $.fn.rating.drain
        
        draw: function(){ // set value and stars to reflect current selection
            var control = this.data('rating'); if(!control) return this;
            // Clear all stars
            this.rating('drain');
            // Set control value
            if(control.current){
                control.current.data('rating.input').attr('checked','checked');
                control.current.prevAll().andSelf().filter('.rater-'+ control.serial).addClass('star-rating-on');
            }
            else
             $(control.inputs).removeAttr('checked');
            // Show/hide 'cancel' button
            //control.cancel[control.readOnly || control.required?'hide':'show']();
            // Add/remove read-only classes to remove hand pointer
            this.siblings()[control.readOnly?'addClass':'removeClass']('star-rating-readonly');
        },// $.fn.rating.draw
        
        
        
        
        
        select: function(value,wantCallBack){ // select a value
                    
                    // ***** MODIFICATION *****
                    // Thanks to faivre.thomas - http://code.google.com/p/jquery-star-rating-plugin/issues/detail?id=27
                    //
                    // ***** LIST OF MODIFICATION *****
                    // ***** added Parameter wantCallBack : false if you don't want a callback. true or undefined if you want postback to be performed at the end of this method'
                    // ***** recursive calls to this method were like : ... .rating('select') it's now like .rating('select',undefined,wantCallBack); (parameters are set.)
                    // ***** line which is calling callback
                    // ***** /LIST OF MODIFICATION *****
            
            var control = this.data('rating'); if(!control) return this;
            // do not execute when control is in read-only mode
            if(control.readOnly) return;
            // clear selection
            control.current = null;
            // programmatically (based on user input)
            if(typeof value!='undefined'){
             // select by index (0 based)
                if(typeof value=='number')
             return $(control.stars[value]).rating('select',undefined,wantCallBack);
                // select by literal value (must be passed as a string
                if(typeof value=='string')
                    //return
                    $.each(control.stars, function(){
                        if($(this).data('rating.input').val()==value) $(this).rating('select',undefined,wantCallBack);
                    });
            }
            else
                control.current = this[0].tagName=='INPUT' ?
                 this.data('rating.star') :
                    (this.is('.rater-'+ control.serial) ? this : null);

            // Update rating control state
            this.data('rating', control);
            // Update display
            this.rating('draw');
            // find data for event
            var input = $( control.current ? control.current.data('rating.input') : null );
            // click callback, as requested here: http://plugins.jquery.com/node/1655
                    
                    // **** MODIFICATION *****
                    // Thanks to faivre.thomas - http://code.google.com/p/jquery-star-rating-plugin/issues/detail?id=27
                    //
                    //old line doing the callback :
                    //if(control.callback) control.callback.apply(input[0], [input.val(), $('a', control.current)[0]]);// callback event
                    //
                    //new line doing the callback (if i want :)
                    if((wantCallBack ||wantCallBack == undefined) && control.callback) control.callback.apply(input[0], [input.val(), $('a', control.current)[0]]);// callback event
                    //to ensure retro-compatibility, wantCallBack must be considered as true by default
                    // **** /MODIFICATION *****
                    
  },// $.fn.rating.select
        
        
        
        
        
        readOnly: function(toggle, disable){ // make the control read-only (still submits value)
            var control = this.data('rating'); if(!control) return this;
            // setread-only status
            control.readOnly = toggle || toggle==undefined ? true : false;
            // enable/disable control value submission
            if(disable) $(control.inputs).attr("disabled", "disabled");
            else                $(control.inputs).removeAttr("disabled");
            // Update rating control state
            this.data('rating', control);
            // Update display
            this.rating('draw');
        },// $.fn.rating.readOnly
        
        disable: function(){ // make read-only and never submit value
            this.rating('readOnly', true, true);
        },// $.fn.rating.disable
        
        enable: function(){ // make read/write and submit value
            this.rating('readOnly', false, false);
        }// $.fn.rating.select
        
 });
    
    /*--------------------------------------------------------*/
    
    /*
        ### Default Settings ###
        eg.: You can override default control like this:
        $.fn.rating.options.cancel = 'Clear';
    */
    $.fn.rating.options = { //$.extend($.fn.rating, { options: {
            //cancel: 'Cancel Rating',   // advisory title for the 'cancel' link
            //cancelValue: '',           // value to submit when user click the 'cancel' link
            split: 0,                  // split the star into how many parts?
            
            // Width of star image in case the plugin can't work it out. This can happen if
            // the jQuery.dimensions plugin is not available OR the image is hidden at installation
            starWidth: 16//,
            
            //NB.: These don't need to be pre-defined (can be undefined/null) so let's save some code!
            //half:     false,         // just a shortcut to control.split = 2
            //required: false,         // disables the 'cancel' button so user can only select one of the specified values
            //readOnly: false,         // disable rating plugin interaction/ values cannot be changed
            //focus:    function(){},  // executed when stars are focused
            //blur:     function(){},  // executed when stars are focused
            //callback: function(){},  // executed when a star is clicked
 }; //} });
    
    /*--------------------------------------------------------*/
    
    /*
        ### Default implementation ###
        The plugin will attach itself to file inputs
        with the class 'multi' when the page loads
    */
    $(function(){
     $('input[type=radio].star').rating();
    });
    
    
    
/*# AVOID COLLISIONS #*/
})(jQuery);
/*# AVOID COLLISIONS #*/


function dlh_fixed_widget(options) {
    
    if ( !options.widgets) return false;
    
    if ( options.widgets.length < 1) return false;
    
    if ( !options.sidebar) return false;
        
    function widget() {} // widget class
    
    var widgets = new Array();
    
    var window_height = jQuery(window).height();
    var document_height = jQuery(document).height();
    var fixed_margin_top = options.margin_top;
    
    jQuery('.dlh-widget-clone-' + options.sidebar).remove(); // clear fixed mode p1
    
    for ( var i = 0; i < options.widgets.length; i++ ) {
        widget_obj = jQuery('#' + options.widgets[i]);
        widget_obj.css('position',''); // clear fixed mode p2
        if ( widget_obj.attr('id') ) { 
            widgets[i] = new widget();
            widgets[i].obj = widget_obj;
            widgets[i].clone = widget_obj.clone();
            widgets[i].clone.children().remove();
            widgets[i].clone_id = widget_obj.attr('id') + '_clone';
            widgets[i].clone.addClass('dlh-widget-clone-' + options.sidebar);
            widgets[i].clone.attr('id', widgets[i].clone_id);
            widgets[i].clone.css('height', widget_obj.height());
            widgets[i].clone.css('visibility', 'hidden');
            widgets[i].offset_top = widget_obj.offset().top;
            widgets[i].fixed_margin_top = fixed_margin_top;
            widgets[i].height = widget_obj.outerHeight(true);
            widgets[i].fixed_margin_bottom = fixed_margin_top + widgets[i].height;
            fixed_margin_top += widgets[i].height;
        } else {
            widgets[i] = false;         
        }
    }
    
    var next_widgets_height = 0;
    
    var widget_parent_container;
        
    for ( var i = widgets.length - 1; i >= 0; i-- ) {
        if (widgets[i]) {
            widgets[i].next_widgets_height = next_widgets_height;
            widgets[i].fixed_margin_bottom += next_widgets_height;
            next_widgets_height += widgets[i].height;
            if ( !widget_parent_container ) {
                widget_parent_container = widget_obj.parent();
                widget_parent_container.css('height','');
                widget_parent_container.height(widget_parent_container.height());
            }
        }
    }
    
    jQuery(window).off('load scroll.' + options.sidebar);
    
    for ( var i = 0; i < widgets.length; i++ ) {
        if (widgets[i]) fixed_widget(widgets[i]);
    }
    
    function fixed_widget(widget) {
        
        var trigger_top = widget.offset_top - widget.fixed_margin_top;
        var trigger_bottom = document_height - options.margin_bottom;
    
        var widget_width; if ( options.width_inherit ) widget_width = 'inherit'; else widget_width = widget.obj.css('width');
        
        var style_applied_top = false;
        var style_applied_bottom = false;
        var style_applied_normal = false;
        
        jQuery(window).on('scroll.' + options.sidebar, function (event) {
            var scroll = jQuery(this).scrollTop();

            if ( scroll + widget.fixed_margin_bottom >= trigger_bottom ) { // fixed bottom
                if ( !style_applied_bottom ) {
                    widget.obj.css('position', 'fixed');
                    widget.obj.css('top', '');
                    widget.obj.css('width', widget_width);
                    if(jQuery('#'+widget.clone_id).length <= 0) widget.obj.before(widget.clone);
                    style_applied_bottom = true;
                    style_applied_top = false;
                    style_applied_normal = false;
                }

                widget.obj.css({
                    'bottom': scroll + window_height + widget.next_widgets_height - trigger_bottom
                });
            
            } else if ( scroll >= trigger_top ) { // fixed top
                if ( !style_applied_top ) {
                    widget.obj.css('position', 'fixed');
                    widget.obj.css('top', widget.fixed_margin_top);
                    widget.obj.css('bottom', '');
                    widget.obj.css('width', widget_width);
                    if(jQuery('#'+widget.clone_id).length <= 0) widget.obj.before(widget.clone);
                    style_applied_top = true;
                    style_applied_bottom = false;
                    style_applied_normal = false;

                }
            } else { // normal
                if ( !style_applied_normal ) {
                    widget.obj.css('position', '');
                    widget.obj.css('top', '');
                    widget.obj.css('width', '');
                    if(jQuery('#'+widget.clone_id).length > 0) jQuery('#'+widget.clone_id).remove();
                    style_applied_normal = true;
                    style_applied_top = false;
                    style_applied_bottom = false;
                }
            }
        }).trigger('scroll.' + options.sidebar);
        
        jQuery(window).on('resize', function() {
            if ( jQuery(window).width() <= options.screen_max_width ) {
                jQuery(window).off('load scroll.' + options.sidebar);
                widget.obj.css('position', '');
                widget.obj.css('top', '');
                widget.obj.css('width', '');
                widget.obj.css('margin', '');
                widget.obj.css('padding', '');
                if(jQuery('#'+widget.clone_id).length > 0) jQuery('#'+widget.clone_id).remove();
                style_applied_normal = true;
                style_applied_top = false;
                style_applied_bottom = false;               
            }
        }).trigger('resize');
        
    }   
    
}

// Push Menu
/**
 * jquery.multilevelpushmenu.js v2.0.9
 *
 * Licensed under the MIT license.
 * http://www.opensource.org/licenses/mit-license.php
 * 
 * Copyright 2013, Make IT d.o.o.
 * http://multi-level-push-menu.make.rs
 * https://github.com/adgsm/multi-level-push-menu
 */
(function ( $ ) {
        $.fn.multilevelpushmenu = function( options ) {
                var args = arguments;
                        returnValue = null;
                
                this.each(function(){
                        var instance = this,
                                $this = $( this ),
                                $container = ( $this.context != undefined ) ? $this : $( 'body' ),
                                menu = ( options.menu != undefined ) ? options.menu : $this.find( 'nav' ),
                                clickEventType, dragEventType;

                        // Settings
                        var settings = $.extend({
                                container: $container,
                                containersToPush: null,
                                menuID: ( ( $container.prop( 'id' ) != undefined && $container.prop( 'id' ) != '' ) ? $container.prop( 'id' ) : this.nodeName.toLowerCase() ) + "_multilevelpushmenu",
                                wrapperClass: 'multilevelpushmenu_wrapper',
                                menuInactiveClass: 'multilevelpushmenu_inactive',
                                menu: menu,
                                menuWidth: 0,
                                menuHeight: 0,
                                collapsed: false,
                                fullCollapse: false,
                                direction: 'ltr',
                                backText: 'Back',
                                backItemClass: 'backItemClass',
                                backItemIcon: 'fa fa-angle-right',
                                groupIcon: 'fa fa-angle-left',
                                mode: 'overlap',
                                overlapWidth: 40,
                                preventItemClick: true,
                                preventGroupItemClick: true,
                                onCollapseMenuStart: function() {},
                                onCollapseMenuEnd: function() {},
                                onExpandMenuStart: function() {},
                                onExpandMenuEnd: function() {},
                                onGroupItemClick: function() {},
                                onItemClick: function() {},
                                onTitleItemClick: function() {},
                                onBackItemClick: function() {}
                        }, options );

                        // Store a settings reference withint the element's data
                        if (!$.data(instance, 'plugin_multilevelpushmenu')) {
                                $.data(instance, 'plugin_multilevelpushmenu', settings);
                                instance.settings = $.data(instance, 'plugin_multilevelpushmenu');
                        }

                        // Exposed methods
                        var methods = {
                                // Initialize menu
                                init: function () {
                                        return initialize.apply(this, Array.prototype.slice.call(arguments));
                                },
                                // Collapse menu
                                collapse: function () {
                                        return collapseMenu.apply(this, Array.prototype.slice.call(arguments));
                                },
                                // Expand menu
                                expand: function () {
                                        return expandMenu.apply(this, Array.prototype.slice.call(arguments));
                                },
                                // Menu expanded
                                menuexpanded: function () {
                                        return menuExpanded.apply(this, Array.prototype.slice.call(arguments));
                                },
                                // Active menu
                                activemenu: function () {
                                        return activeMenu.apply(this, Array.prototype.slice.call(arguments));
                                },
                                // Find menu(s) by title
                                findmenusbytitle: function () {
                                        return findMenusByTitle.apply(this, Array.prototype.slice.call(arguments));
                                },
                                // Find item(s) by name
                                finditemsbyname: function () {
                                        return findItemsByName.apply(this, Array.prototype.slice.call(arguments));
                                },
                                // Find path to root menu collection
                                pathtoroot: function () {
                                        return pathToRoot.apply(this, Array.prototype.slice.call(arguments));
                                },
                                // Find shared path to root of two menus
                                comparepaths: function () {
                                        return comparePaths.apply(this, Array.prototype.slice.call(arguments));
                                },
                                // Get/Set settings options
                                option: function () {
                                        return manageOptions.apply(this, Array.prototype.slice.call(arguments));
                                },
                                // Add item(s)
                                additems: function () {
                                        return addItems.apply(this, Array.prototype.slice.call(arguments));
                                },
                                // Remove item(s)
                                removeitems: function () {
                                        return removeItems.apply(this, Array.prototype.slice.call(arguments));
                                }
                        };

                        // IE 8 and modern browsers, prevent event propagation
                        function stopEventPropagation( e ){
                                if ( e.stopPropagation && e.preventDefault ) {
                                        e.stopPropagation();
                                        e.preventDefault();
                                }
                                else {
                                        e.cancelBubble = true;
                                        e.returnValue = false;
                                }
                        }

                        // Create DOM structure if it does not already exist within the container (input: array)
                        function createDOMStructure() {
                                var $mainWrapper = $( "<nav />" )
                                    .prop( { "id" : instance.settings.menuID, "className" : instance.settings.wrapperClass } )
                                    .appendTo( instance.settings.container );
                                createNestedDOMStructure( instance.settings.menu, $mainWrapper );
                        }
                        function createNestedDOMStructure( menus, $wrapper ){
                                if( menus.level == undefined ) menus.level = 0;
                                $.each( menus, function(){
                                        var $levelHolder = $( "<div />" )
                                            .attr( { "class" : "levelHolderClass" + ( ( instance.settings.direction == 'rtl' ) ? " rtl" : " ltr" ), "data-level" : menus.level, "style" : ( ( instance.settings.direction == 'rtl' ) ? "margin-right: " : "margin-left: " ) + ( ( menus.level == 0 && !instance.settings.collapsed ) ? 0 : "-200%" ) } )
                                            .appendTo( $wrapper ),
                                            extWidth = ( isValidDim( instance.settings.menuWidth ) || ( isInt( instance.settings.menuWidth ) && instance.settings.menuWidth > 0 ) );
                                        $levelHolder.bind( dragEventType ,  function(e){
                                                holderSwipe( e, $levelHolder );
                                        });
                                        if( extWidth ) $levelHolder.width(instance.settings.menuWidth);
                                        var $title = $( "<h2 />" )
                                                .attr( { "style" : "text-align: " + ( ( instance.settings.direction == 'rtl' ) ? "right" : "left" ) } )
                                            .text( this.title )
                                            .appendTo( $levelHolder ),
                                            $titleIcon = $( "<i />" )
                                            .prop( { "class" : ( ( instance.settings.direction == 'rtl' ) ? "floatLeft" : "floatRight" ) + " cursorPointer " + this.icon } )
                                            .prependTo( $title );
                                        $titleIcon.bind( clickEventType ,  function(e){
                                                titleIconClick(e, $levelHolder, menus);
                                        });
                                        if( menus.level > 0 ) createBackItem( $levelHolder );
                                        var $itemGroup = $( "<ul />" )
                                            .appendTo( $levelHolder );
                                        $.each(this.items, function(){
                                                createItem( this, $levelHolder , -1 );
                                        })
                                });
                        }

                        // Update DOM structure if it already exists in container (input: HTML markup)
                        function updateDOMStructure() {
                                var $mainWrapper = ( instance.settings.container.find( 'nav' ).length > 0 ) ? instance.settings.container.find( 'nav' ) : instance.settings.menu;
                                if( $mainWrapper.length == 0 ) return false;
                                $mainWrapper.prop( { "id" : instance.settings.menuID, "className" : instance.settings.wrapperClass } );
                                updateNestedDOMStructure( $mainWrapper );
                        }
                        function updateNestedDOMStructure( $wrapper ){
                                if( $wrapper.level == undefined ) $wrapper.level = 0;
                                $.each( $wrapper, function(){
                                        var $levelHolder = $( "<div />" )
                                            .attr( { "class" : "levelHolderClass" + ( ( instance.settings.direction == 'rtl' ) ? " rtl" : " ltr" ), "data-level" : $wrapper.level, "style" : ( ( instance.settings.direction == 'rtl' ) ? "margin-right: " : "margin-left: " ) + ( ( $wrapper.level == 0 && !instance.settings.collapsed ) ? 0 : "-200%" ) } )
                                            .appendTo( $wrapper ),
                                            extWidth = ( isValidDim( instance.settings.menuWidth ) || ( isInt( instance.settings.menuWidth ) && instance.settings.menuWidth > 0 ) );
                                        $levelHolder.bind( dragEventType ,  function(e){
                                                holderSwipe( e, $levelHolder );
                                        });
                                        if( extWidth ) $levelHolder.width(instance.settings.menuWidth);
                                        var $title = $wrapper.children( 'h2' );
                                        $title.attr( { "style" : "text-align: " + ( ( instance.settings.direction == 'rtl' ) ? "right" : "left" ) } );
                                        $title.appendTo( $levelHolder );
                                        var $titleIcon = $title.children( 'i' );
                                        $titleIcon.addClass( ( ( instance.settings.direction == 'rtl' ) ? "floatLeft" : "floatRight" ) + " cursorPointer" );
                                        $titleIcon.bind( clickEventType , function(e){
                                                titleIconClick(e, $levelHolder, $wrapper);
                                        });
                                        if( $wrapper.level > 0 ) createBackItem( $levelHolder );
                                        var $itemGroup = $wrapper.children( 'ul' );
                                        $itemGroup.appendTo( $levelHolder );
                                        $.each($itemGroup.children( 'li' ), function(){
                                                var $item = $( this );
                                                $item.attr( { "style" : "text-align: " + ( ( instance.settings.direction == 'rtl' ) ? "right" : "left" ) } );
                                                var $itemAnchor = $item.children( 'a' );
                                                var $itemIcon = $itemAnchor.children( 'i' );
                                                $itemIcon.addClass( ( ( instance.settings.direction == 'rtl' ) ? "floatLeft" : "floatRight" ) );
                                                if($item.children( 'ul' ).length > 0) {
                                                        $itemAnchor.bind( clickEventType , function(e){
                                                                itemGroupAnchorClick( e, $levelHolder, $item );
                                                        });
                                                        createItemGroupIcon( $itemAnchor );
                                                        $item.level = $wrapper.level + 1;
                                                        updateNestedDOMStructure($item);
                                                } else {
                                                        $itemAnchor.bind( clickEventType , function(e){
                                                                itemAnchorClick( e, $levelHolder, $item );
                                                        });
                                                }
                                        })
                                });
                        }

                        // Click event for title icon
                        function titleIconClick( e, $levelHolder, menus ) {
                                if( $(instance).find( 'div.levelHolderClass' ).is(':animated') ) return false;
                                instance.settings.onTitleItemClick.apply(this, Array.prototype.slice.call([e, $levelHolder, instance.settings]));
                                stopEventPropagation(e);
                                var instanceFC = ( instance.settings.direction == 'rtl' ) ?
                                        parseInt( $levelHolder.css( 'margin-right' ) ) < 0
                                        :
                                        parseInt( $levelHolder.css( 'margin-left' ) ) < 0;
                                if( menus.level == 0 && instanceFC ) {
                                        expandMenu();
                                }
                                else {
                                        var $nextLevelHolders = instance.settings.container
                                                .find( '#' + instance.settings.menuID + ' div.levelHolderClass' )
                                                .filter(function(){
                                                        var retObjs = ( instance.settings.direction == 'rtl' ) ?
                                                        (($( this ).attr( 'data-level' ) > $levelHolder.attr( 'data-level' )) && ( parseInt( $( this ).css( 'margin-right' ) ) >= 0 ) )
                                                        :
                                                        (($( this ).attr( 'data-level' ) > $levelHolder.attr( 'data-level' )) && ( parseInt( $( this ).css( 'margin-left' ) ) >= 0 ) );
                                                        return retObjs;
                                                }),
                                                $prevLevelHolders = instance.settings.container
                                                .find( '#' + instance.settings.menuID + ' div.levelHolderClass' )
                                                .filter(function(){
                                                        var retObjs = ( instance.settings.direction == 'rtl' ) ?
                                                        (($( this ).attr( 'data-level' ) <= $levelHolder.attr( 'data-level' )) && ( parseInt( $( this ).css( 'margin-right' ) ) >= 0 ) )
                                                        :
                                                        (($( this ).attr( 'data-level' ) <= $levelHolder.attr( 'data-level' )) && ( parseInt( $( this ).css( 'margin-left' ) ) >= 0 ) );
                                                        return retObjs;
                                                }),
                                                collapseAll = ( $nextLevelHolders.length == 0 && $prevLevelHolders.length == 1 ) ? collapseMenu() : collapseMenu( parseInt( $levelHolder.attr( 'data-level' ) ) );
                                }
                                $levelHolder.css( 'visibility' , 'visible' );
                                $levelHolder.find( '.' + instance.settings.backItemClass ).css( 'visibility' , 'visible' );
                                $levelHolder.find( 'ul' ).css( 'visibility' , 'visible' );
                                $levelHolder.removeClass( instance.settings.menuInactiveClass );
                        }

                        // Create Back item DOM elements
                        function createBackItem( $levelHolder ) {
                                var $backItem = $( "<div />" )
                                    .attr( { "class" : instance.settings.backItemClass } )
                                    .appendTo( $levelHolder ),
                                    $backItemAnchor = $( "<a />" )
                                    .prop( { "href" : "#" } )
                                    .text( instance.settings.backText )
                                    .appendTo( $backItem ),
                                    $backItemIcon = $( "<i />" )
                                    .prop( { "class" : ( ( instance.settings.direction == 'rtl' ) ? "floatLeft " : "floatRight " ) + instance.settings.backItemIcon } )
                                    .prependTo( $backItemAnchor );
                                $backItemAnchor.bind( clickEventType , function(e){
                                        backItemAnchorClick(e, $levelHolder);
                                });
                        }

                        // Click event for back item
                        function backItemAnchorClick( e, $levelHolder ) {
                                if( $(instance).find( 'div.levelHolderClass' ).is(':animated') ) return false;
                                instance.settings.onBackItemClick.apply(this, Array.prototype.slice.call([e, $levelHolder, instance.settings]));
                                stopEventPropagation(e);
                                collapseMenu( parseInt( $levelHolder.attr( 'data-level' ) - 1 ) );
                        }

                        // Click event for group items
                        function itemGroupAnchorClick( e, $levelHolder, $item ) {
                                if( $(instance).find( 'div.levelHolderClass' ).is(':animated') ) return false;
                                instance.settings.onGroupItemClick.apply(this, Array.prototype.slice.call([e, $levelHolder, $item, instance.settings]));
                                expandMenu( $item.find( 'div:first' ) );
                                if( instance.settings.preventGroupItemClick ) stopEventPropagation(e);
                        }

                        // Create item group DOM element
                        function createItemGroupIcon( $itemAnchor ) {
                                var $itemGroupIcon = $( "<i />" )
                                        .attr( { "class" : ( ( instance.settings.direction == 'rtl' ) ? " floatRight iconSpacing_rtl " : " floatLeft iconSpacing_ltr " ) + instance.settings.groupIcon } )
                                        .prependTo( $itemAnchor );
                        }

                        // Create item DOM element
                        function createItem() {
                                var item = arguments[0],
                                        $levelHolder = arguments[1],
                                        position = arguments[2],
                                        $itemGroup = $levelHolder.find( 'ul:first' ),
                                        $item = $( "<li />" );
                                        ( position < ( $itemGroup.find( 'li' ).length ) && position >= 0 ) ? 
                                                $item.insertBefore( $itemGroup.find( 'li' ).eq( position ) ) : $item.appendTo( $itemGroup );
                                        $item.attr( { "style" : "text-align: " + ( ( instance.settings.direction == 'rtl' ) ? "right" : "left" ) } );
                                    var $itemAnchor = $( "<a />" )
                                    .prop( { "href" : item.link } )
                                    .text( item.name )
                                    .appendTo( $item ),
                                    $itemIcon = $( "<i />" )
                                        .prop( { "class" : ( ( instance.settings.direction == 'rtl' ) ? "floatLeft " : "floatRight " ) + item.icon } )
                                    .prependTo( $itemAnchor );
                                if(item.items) {
                                        $itemAnchor.bind( clickEventType , function(e){
                                                itemGroupAnchorClick( e, $levelHolder, $item );
                                        });
                                        createItemGroupIcon( $itemAnchor );
                                        item.items.level = parseInt( $levelHolder.attr( 'data-level' ), 10 ) + 1;
                                        createNestedDOMStructure(item.items, $item);
                                } else {
                                        $itemAnchor.bind( clickEventType , function(e){
                                                itemAnchorClick( e, $levelHolder, $item );
                                        });
                                }
                        }

                        // Click event for items
                        function itemAnchorClick( e, $levelHolder, $item ) {
                                instance.settings.onItemClick.apply(this, Array.prototype.slice.call([e, $levelHolder, $item, instance.settings]));
                                if( instance.settings.preventItemClick ) stopEventPropagation(e);
                        }

                        // Swipe/Drag event for holders
                        function holderSwipe( emd, $levelHolder ) {
                                if( $(instance).find( 'div.levelHolderClass' ).is(':animated') ) return false;
                                stopEventPropagation( emd );
                                var level = ( $levelHolder.attr( 'data-level' ) > 0 ) ? $levelHolder.attr( 'data-level' ) - 1 : undefined;
                                if( emd.type == 'touchmove' ) {
                                        emd = ( emd.touches ) ? emd : emd.originalEvent;
                                        if( !emd.touches || emd.touches.length <= 0 ) return false;
                                        var touch = emd.touches[0];
                                        instance.settings.container.unbind( 'touchend' );
                                        instance.settings.container.bind( 'touchend' , function( emm ) {
                                                stopEventPropagation( emm );
                                                $levelHolder.significance = 0;
                                                $levelHolder.swipeStart = 0;
                                                instance.settings.container.unbind( 'touchend' );
                                        });
                                        if ( $levelHolder.swipeStart != undefined && $levelHolder.swipeStart != 0 ) {
                                                $levelHolder.significance = touch.pageX - $levelHolder.swipeStart;
                                        }
                                        else {
                                                $levelHolder.significance = 0;
                                                $levelHolder.swipeStart = touch.pageX;
                                                return true;
                                        }
                                        if( Math.abs( $levelHolder.significance ) > instance.settings.overlapWidth*.3 ) {
                                                if( instance.settings.direction == 'rtl' ) $levelHolder.significance *= ( -1 );
                                                ( $levelHolder.significance > 0 ) ? expandMenu( ( level == undefined ) ? level : $levelHolder ) : collapseMenu( level );
                                                $levelHolder.significance = 0;
                                                $levelHolder.swipeStart = 0;
                                        }
                                }
                                else {
                                        var significance = 0;
                                        $levelHolder.unbind( 'mousemove' );
                                        $levelHolder.bind( 'mousemove' ,  function( emm ){
                                                significance = emm.clientX - emd.clientX;
                                                if( Math.abs( significance ) > instance.settings.overlapWidth*.3 ) {
                                                        $levelHolder.unbind( 'mousemove' );
                                                        if( instance.settings.direction == 'rtl' ) significance *= ( -1 );
                                                        ( significance > 0 ) ? expandMenu( ( level == undefined ) ? level : $levelHolder ) : collapseMenu( level );
                                                        return true;
                                                }
                                        });
                                        instance.settings.container.unbind( 'mouseup' );
                                        instance.settings.container.bind( 'mouseup' ,  function(e){
                                                stopEventPropagation( e );
                                                $levelHolder.unbind( 'mousemove' );
                                                instance.settings.container.unbind( 'mouseup' );
                                        });
                                }
                        }

                        // Sizing DOM elements per creation/update
                        function sizeDOMelements() {
                                var forceWidth = arguments[0],
                                        forceHeight = arguments[1],
                                        filter = arguments[2],
                                        ieShadowFilterDistortion = ($('#' + instance.settings.menuID + ' div.levelHolderClass').first().css('filter').match(/DXImageTransform\.Microsoft\.Shadow/)) ? $('#' + instance.settings.menuID + ' div.levelHolderClass').first().get(0).filters.item("DXImageTransform.Microsoft.Shadow").strength : 0,
                                        maxWidth = ( forceWidth == undefined ) ? Math.max.apply( null,
                                        $('#' + instance.settings.menuID + ' div.levelHolderClass').map(function(){ return $(this).width(); }).get() ) * 1.1 : forceWidth,
                                        maxLevel = Math.max.apply( null,
                                        $('#' + instance.settings.menuID + ' div.levelHolderClass').map(function(){ return $(this).attr( 'data-level' ); }).get() ),
                                        extWidth = ( isValidDim( instance.settings.menuWidth ) || ( isInt( instance.settings.menuWidth ) && instance.settings.menuWidth > 0 ) ),
                                        extHeight = ( isValidDim( instance.settings.menuHeight ) || ( isInt( instance.settings.menuHeight ) && instance.settings.menuHeight > 0 ) ),
                                        maxExtWidth = ( extWidth ) ? ( instance.settings.menuWidth + maxLevel * ( instance.settings.overlapWidth + ieShadowFilterDistortion ) ) : ( maxWidth + maxLevel * ( instance.settings.overlapWidth + ieShadowFilterDistortion ) ),
                                        maxHeight = ( forceHeight == undefined ) ? Math.max.apply( null,
                                        $('#' + instance.settings.menuID + ' div.levelHolderClass').map(function(){ return $(this).height(); }).get() ) : forceHeight,
                                        $objects = ( filter == undefined ) ? $('#' + instance.settings.menuID + ' div.levelHolderClass' ) : filter; 
                                ( extWidth ) ? $objects.width(instance.settings.menuWidth) : $objects.width( maxWidth );
                                ( extHeight ) ? $('#' + instance.settings.menuID).height(instance.settings.menuHeight) : $('#' + instance.settings.menuID).height( maxHeight );
                                instance.settings.container.css( 'min-width' , maxExtWidth + 'px' );
                                instance.settings.container.css( 'min-height' , maxHeight + 'px' );
                                instance.settings.container.children( 'nav:first' ).css( 'min-width' , maxExtWidth + 'px' );
                                instance.settings.container.children( 'nav:first' ).css( 'min-height' , maxHeight + 'px' );
                                instance.settings.container.width( maxExtWidth );
                                instance.settings.container.height( maxHeight );
                                instance.settings.menuWidth = maxWidth;
                                instance.settings.menuHeight = maxHeight;
                                var fix = ( filter != undefined ) ? null : fixLazyBrowsers();
                        }

                        // Hide wrappers in browsers that
                        // does not understand negative margin in %
                        // before DOM element got its dimensions
                        function fixLazyBrowsers() {
                                var $baseLevelHolder = $('#' + instance.settings.menuID + ' div.levelHolderClass:first'),
                                $hiddenLevelHolders = instance.settings.container
                                        .find( '#' + instance.settings.menuID + ' div.levelHolderClass' )
                                        .filter(function(){
                                                var retObjs = ( instance.settings.direction == 'rtl' ) ?
                                                        ( ( $( this ).position().left > instance.settings.container.width() || parseInt( $( this ).css( 'margin-right' ) ) < 0 ) && $( this ).attr( 'data-level' ) > $baseLevelHolder.attr( 'data-level' ) )
                                                        :
                                                        ( ( $( this ).position().left < 0 || parseInt( $( this ).css( 'margin-left' ) ) < 0 ) && $( this ).attr( 'data-level' ) > $baseLevelHolder.attr( 'data-level' ) );
                                                return retObjs;
                                        });
                                $hiddenLevelHolders.each(function(){
                                        if( instance.settings.direction == 'rtl' ){
                                                $( this ).css( 'margin-right' , ( ( $( this ).attr( 'data-level' ) == $baseLevelHolder.attr( 'data-level' ) && !instance.settings.collapsed ) ? 0 : (-2) * $( this ).width() ) )
                                        }
                                        else {
                                                $( this ).css( 'margin-left' , ( ( $( this ).attr( 'data-level' ) == $baseLevelHolder.attr( 'data-level' ) && !instance.settings.collapsed ) ? 0 : (-2) * $( this ).width() ) );
                                        }
                                });
                                if( instance.settings.direction == 'rtl' ){
                                        $baseLevelHolder.css( 'margin-right' , ( !instance.settings.collapsed ) ? 0 : (-2) * $baseLevelHolder.width() )
                                }
                                else {
                                        $baseLevelHolder.css( 'margin-left' , ( !instance.settings.collapsed ) ? 0 : (-2) * $baseLevelHolder.width() );
                                }
                        }

                        // Is integer
                        function isInt( n ) {
                                return typeof n === 'number' && parseFloat( n ) == parseInt( n, 10 ) && !isNaN( n );
                        }

                        // Is Valid CSS dimension
                        function isValidDim( s ) {
                                return typeof s === 'string' && ( s.indexOf( '%' ) != -1 || s.indexOf( 'px' ) != -1 || s.indexOf( 'em' ) != -1 );
                        }

                        // Initialize menu level push menu
                        function initialize(){
                                var execute = ( options.menu != undefined ) ? createDOMStructure() : updateDOMStructure();
                                sizeDOMelements();
                                startMode( instance.settings.collapsed );
                                return $this;
                        }

                        // Initialize menu in collapsed/expanded mode 
                        function startMode( mode ) {
                                if( mode ) {
                                        $baseLevelHolder = $('#' + instance.settings.menuID + ' div.levelHolderClass:first');
                                        $baseLevelHolder.find( 'ul' ).hide();
                                        $baseLevelHolder.addClass( instance.settings.menuInactiveClass );
                                        if( instance.settings.direction == 'rtl' ) {
                                                $baseLevelHolder.stop().animate({
                                                        marginRight: ( ( -1 ) * $baseLevelHolder.width() + ( ( instance.settings.fullCollapse ) ? 0 : instance.settings.overlapWidth ) )
                                                })
                                        }
                                        else {
                                                $baseLevelHolder.stop().animate({
                                                        marginLeft:  ( ( -1 ) * $baseLevelHolder.width() + ( ( instance.settings.fullCollapse ) ? 0 : instance.settings.overlapWidth ) )
                                                });
                                        }
                                }
                        }

                        // Push container(s) of choice
                        function pushContainers( absMove ) {
                                if( instance.settings.containersToPush == null ) return false;
                                $.each( instance.settings.containersToPush, function() {
                                        var lMr = parseInt( $( this ).css( 'margin-left' ) ),
                                                lM = isInt( lMr ) ? lMr : 0,
                                                rMr = parseInt( $( this ).css( 'margin-right' ) ),
                                                rM = isInt( rMr ) ? rMr : 0;
                                        $( this ).stop().animate({
                                                marginLeft:  lM + ( ( instance.settings.direction == 'rtl' ) ? (-1) : 1 ) * absMove,
                                                marginRight: rM + ( ( instance.settings.direction == 'rtl' ) ? 1 : (-1) ) * absMove
                                        });
                                });
                        }

                        // Collapse menu
                        function collapseMenu() {
                                if( $(instance).find( 'div.levelHolderClass' ).is(':animated') ) return false;
                                instance.settings.onCollapseMenuStart.apply(this, Array.prototype.slice.call([instance.settings]));
                                var level = arguments[0],
                                        callbacks = arguments[1],
                                        collapingObjects = {},
                                        ieShadowFilterDistortion,lwidth, lpush, lMarginLeft, lMarginLeftFC,
                                        $baseLevelHolder = $('#' + instance.settings.menuID + ' div.levelHolderClass:first'),
                                        collapseAll = ( level == undefined ) ? true : false;
                                collapingObjects[ 'collapsingEnded' ] = false;
                                if( typeof level == 'object' ) {
                                        level = level.attr( 'data-level' );
                                }
                                else if( typeof level == 'string' ){
                                        var $selectedLevelHolder = findMenusByTitle( level );
                                        if( $selectedLevelHolder && $selectedLevelHolder.length == 1 ) {
                                                level = $selectedLevelHolder.attr( 'data-level' );
                                        }
                                        else {
                                                level = $baseLevelHolder.attr( 'data-level' );
                                        }
                                }
                                else if( level == undefined || !isInt( level ) || level < 0 ) {
                                        level = $baseLevelHolder.attr( 'data-level' );
                                }
                                if( callbacks == undefined && typeof callbacks != 'object' ) {
                                        callbacks = [ { 'method' : instance.settings.onCollapseMenuEnd, 'args' : [instance.settings] } ];
                                } else {
                                        $.merge(callbacks, [ { 'method' : instance.settings.onCollapseMenuEnd, 'args' : [instance.settings] } ]);
                                }
                                var $nextLevelHolders = instance.settings.container
                                        .find( '#' + instance.settings.menuID + ' div.levelHolderClass' )
                                        .filter(function(){
                                                var retObjs = ( instance.settings.direction == 'rtl' ) ? 
                                                ($( this ).attr( 'data-level' ) > level) && (parseInt( $( this ).css( 'margin-right' ) ) >= 0 && $( this ).position().left < instance.settings.container.width() - instance.settings.overlapWidth )
                                                :
                                                ($( this ).attr( 'data-level' ) > level) && (parseInt( $( this ).css( 'margin-left' ) ) >= 0 && $( this ).position().left >= 0 );
                                                return retObjs;
                                        }),
                                        $prevLevelHolders = instance.settings.container
                                        .find( '#' + instance.settings.menuID + ' div.levelHolderClass' )
                                        .filter(function(){
                                                var retObjs = ( instance.settings.direction == 'rtl' ) ? 
                                                ($( this ).attr( 'data-level' ) <= level) && (parseInt( $( this ).css( 'margin-right' ) ) >= 0 && $( this ).position().left < instance.settings.container.width() - instance.settings.overlapWidth )
                                                :
                                                ($( this ).attr( 'data-level' ) <= level) && (parseInt( $( this ).css( 'margin-left' ) ) >= 0 && $( this ).position().left >= 0 );
                                                return retObjs;
                                        });
                                if( $prevLevelHolders.length > 0 ) {
                                        collapingObjects[ 'prevAnimEnded' ] = false;
                                        $nextLevelHolders.each(function( key, val ){
                                                ieShadowFilterDistortion = ($( val ).css('filter').match(/DXImageTransform\.Microsoft\.Shadow/)) ? $( val ).get(0).filters.item("DXImageTransform.Microsoft.Shadow").strength : 0;
                                                lwidth = ( instance.settings.mode == 'overlap' ) ? $( val ).width() - ( $nextLevelHolders.length + $prevLevelHolders.length - $( val ).attr( 'data-level' ) - 1) * ( instance.settings.overlapWidth + ieShadowFilterDistortion ) - ieShadowFilterDistortion : $( val ).width() - ieShadowFilterDistortion
                                                if( instance.settings.direction == 'rtl' ) {
                                                        $( val ).stop().animate({
                                                                marginRight : ( (-1) * lwidth ),
                                                                width: lwidth
                                                        });
                                                }
                                                else {
                                                        $( val ).stop().animate({
                                                                marginLeft : ( (-1) * lwidth ),
                                                                width: lwidth
                                                        });
                                                }
                                        });
                                        collapingObjects[ 'nextAnimEnded' ] = ( $nextLevelHolders.length > 0 ) ? false : true ;
                                        $nextLevelHolders.last().queue(function(){
                                                collapingObjects[ 'nextAnimEnded' ] = true;
                                                animatedEventCallback( collapingObjects , callbacks );
                                        });
                                        $prevLevelHolders.each(function( key, val ){
                                                ieShadowFilterDistortion = ($( val ).css('filter').match(/DXImageTransform\.Microsoft\.Shadow/)) ? $( val ).get(0).filters.item("DXImageTransform.Microsoft.Shadow").strength : 0;
                                                var $makeLevelHolderVisible = $prevLevelHolders.filter(function(){
                                                        return $( this ).attr( 'data-level' ) == level;
                                                });
                                                $makeLevelHolderVisible.css( 'visibility' , 'visible' );
                                                $makeLevelHolderVisible.find( '.' + instance.settings.backItemClass ).css( 'visibility' , 'visible' );
                                                $makeLevelHolderVisible.find( 'ul' ).css( 'visibility' , 'visible' );
                                                $makeLevelHolderVisible.removeClass( instance.settings.menuInactiveClass );
                                                lwidth = ( instance.settings.mode == 'overlap' ) ? $( val ).width() - $nextLevelHolders.length * ( instance.settings.overlapWidth + ieShadowFilterDistortion ) - ieShadowFilterDistortion : $( val ).width() - ieShadowFilterDistortion;
                                                if( instance.settings.direction == 'rtl' ) {
                                                        $( val ).stop().animate({
                                                                width: lwidth,
                                                                marginRight : ( $( val ).attr( 'data-level' ) == $baseLevelHolder.attr( 'data-level' ) && collapseAll ) ?
                                                                        ( instance.settings.fullCollapse ) ?
                                                                                ( -1 ) * $( val ).width()
                                                                                :
                                                                                ( ( -1 ) * $( val ).width() + ( $nextLevelHolders.length + 1 ) * instance.settings.overlapWidth )
                                                                        :
                                                                        0
                                                        }, function(){
                                                                if( $( val ).attr( 'data-level' ) == $baseLevelHolder.attr( 'data-level' ) && collapseAll ){
                                                                        $baseLevelHolder.find( 'ul' ).hide(500, function(){
                                                                                $baseLevelHolder.addClass( instance.settings.menuInactiveClass );
                                                                        });
                                                                }
                                                        });
                                                }
                                                else {
                                                        $( val ).stop().animate({
                                                                width: lwidth,
                                                                marginLeft : ( $( val ).attr( 'data-level' ) == $baseLevelHolder.attr( 'data-level' ) && collapseAll ) ?
                                                                        ( instance.settings.fullCollapse ) ?
                                                                                ( -1 ) * $( val ).width()
                                                                                :
                                                                                ( ( -1 ) * $( val ).width() + ( $nextLevelHolders.length + 1 ) * instance.settings.overlapWidth )
                                                                        :
                                                                        0
                                                        }, function(){
                                                                if( $( val ).attr( 'data-level' ) == $baseLevelHolder.attr( 'data-level' ) && collapseAll ){
                                                                        $baseLevelHolder.find( 'ul' ).hide(500, function(){
                                                                                $baseLevelHolder.addClass( instance.settings.menuInactiveClass );
                                                                        });
                                                                }
                                                        });
                                                }
                                                lpush = ( instance.settings.mode == 'overlap' ) ? ( (-1) * ( $nextLevelHolders.length * ( instance.settings.overlapWidth + ieShadowFilterDistortion ) ) ) : 0 ;
                                                if( $( val ).attr( 'data-level' ) == $baseLevelHolder.attr( 'data-level' ) && collapseAll ){
                                                        var blpush = ( instance.settings.fullCollapse ) ? ( -1 ) * ( $baseLevelHolder.width() - ieShadowFilterDistortion ) : ( -1 ) * ( $baseLevelHolder.width() - ieShadowFilterDistortion ) + instance.settings.overlapWidth;
                                                        pushContainers( blpush );
                                                }
                                                else {
                                                        pushContainers( lpush );
                                                }
                                        });
                                        $prevLevelHolders.last().queue(function(){
                                                collapingObjects[ 'prevAnimEnded' ] = true;
                                                animatedEventCallback( collapingObjects , callbacks );
                                        });
                                }
                                collapingObjects[ 'collapsingEnded' ] = true;
                                animatedEventCallback( collapingObjects , callbacks );
                                return $this;
                        }

                        // Expand Menu helper
                        function expandMenuActions() {
                                if( $(instance).find( 'div.levelHolderClass' ).is(':animated') ) return false;
                                instance.settings.onExpandMenuStart.apply(this, Array.prototype.slice.call([instance.settings]));
                                var menuTitle = arguments[0],
                                        callbacks = arguments[1],
                                        ieShadowFilterDistortion, lwidth, lpush, blpush,
                                        expandingObjects = {},
                                        $baseLevelHolder = $('#' + instance.settings.menuID + ' div.levelHolderClass:first'),
                                        baseExpand = ( menuTitle == undefined ) ? true : false,
                                        baseLevelHolderCollapsed = ( instance.settings.direction == 'rtl' ) ?
                                                parseInt( $baseLevelHolder.css( 'margin-right' ), 10 ) < 0 || $baseLevelHolder.position().left >= instance.settings.container.width() - instance.settings.overlapWidth
                                                :
                                                parseInt( $baseLevelHolder.css( 'margin-left' ), 10 ) < 0 || $baseLevelHolder.position().left < 0;
                                expandingObjects[ 'expandingEnded' ] = false;
                                if( callbacks == undefined && typeof callbacks != 'object' ) {
                                        callbacks = [ { 'method' : instance.settings.onExpandMenuEnd, 'args' : [instance.settings] } ];
                                } else {
                                        $.merge(callbacks, [ { 'method' : instance.settings.onExpandMenuEnd, 'args' : [instance.settings] } ]);
                                }
                                if( baseExpand ) {
                                        expandingObjects[ 'baseAnimEnded' ] = false;
                                        $baseLevelHolder.removeClass( instance.settings.menuInactiveClass );
                                        if( instance.settings.direction == 'rtl' ) {
                                                $baseLevelHolder.stop().animate({
                                                        marginRight: 0
                                                },function(){
                                                        $baseLevelHolder.find( 'ul' ).show(500).last().queue(function(){
                                                                expandingObjects[ 'baseAnimEnded' ] = true;
                                                                animatedEventCallback( expandingObjects , callbacks );
                                                        });
                                                });
                                        }
                                        else {
                                                $baseLevelHolder.stop().animate({
                                                        marginLeft: 0
                                                },function(){
                                                        $baseLevelHolder.find( 'ul' ).show(500).last().queue(function(){
                                                                expandingObjects[ 'baseAnimEnded' ] = true;
                                                                animatedEventCallback( expandingObjects , callbacks );
                                                        });
                                                });
                                        }
                                        blpush = ( instance.settings.fullCollapse ) ? $baseLevelHolder.width() : $baseLevelHolder.width() - instance.settings.overlapWidth;
                                        var pushbm = ( !menuExpanded( $baseLevelHolder ) ) ? pushContainers( blpush ) : null;
                                } else {
                                        if( typeof menuTitle == 'object' ) {
                                                $selectedLevelHolder = menuTitle;
                                        }
                                        else if( typeof menuTitle == 'string' ){
                                                $selectedLevelHolder = findMenusByTitle( menuTitle );
                                        }
                                        else {
                                                $selectedLevelHolder = null;
                                                $.error( 'Provided menu selector is not valid' );
                                        }
                                        if( $selectedLevelHolder && $selectedLevelHolder.length == 1 ) {
                                                var $activeLevelHolder = activeMenu(),
                                                        activeLevel = ( $activeLevelHolder.length == 1 ) ? $activeLevelHolder.attr( 'data-level' ) : 0,
                                                        baseWidth = $selectedLevelHolder.width();
                                                expandingObjects[ 'setToOpenAnimEnded' ] = false;
                                                if( setToOpenHolders = pathToRoot( $selectedLevelHolder ) ) {
                                                        var parentLevelHoldersLen = $( setToOpenHolders ).length - 1;
                                                        $baseLevelHolder.find( 'ul' ).show(0);
                                                        $( setToOpenHolders ).find( 'ul' ).css( 'visibility' , 'hidden' );
                                                        $( setToOpenHolders ).find( 'div' ).css( 'visibility' , 'visible' );
                                                        $( setToOpenHolders ).find( '.' + instance.settings.backItemClass ).css( 'visibility' , 'hidden' );
                                                        $( setToOpenHolders ).each( function( key, val ) {
                                                                ieShadowFilterDistortion = ($( val ).css('filter').match(/DXImageTransform\.Microsoft\.Shadow/)) ? $( val ).get(0).filters.item("DXImageTransform.Microsoft.Shadow").strength : 0;
                                                                lwidth = baseWidth - ieShadowFilterDistortion + ( parentLevelHoldersLen - $( val ).attr( 'data-level' ) ) * ( instance.settings.overlapWidth + ieShadowFilterDistortion );
                                                                if( instance.settings.direction == 'rtl' ) {
                                                                        $( val ).stop().animate({
                                                                                marginRight: 0,
                                                                                width: ( instance.settings.mode == 'overlap' ) ? lwidth : baseWidth - ieShadowFilterDistortion
                                                                        }, function(){
                                                                                $( val ).addClass( instance.settings.menuInactiveClass );
                                                                        });
                                                                }
                                                                else {
                                                                        $( val ).stop().animate({
                                                                                marginLeft: 0,
                                                                                width: ( instance.settings.mode == 'overlap' ) ? lwidth : baseWidth - ieShadowFilterDistortion
                                                                        }, function(){
                                                                                $( val ).addClass( instance.settings.menuInactiveClass );
                                                                        });
                                                                }
                                                        });
                                                        $( setToOpenHolders ).last().queue(function(){
                                                                $( this ).removeClass( instance.settings.menuInactiveClass );
                                                                expandingObjects[ 'setToOpenAnimEnded' ] = true;
                                                                animatedEventCallback( expandingObjects , callbacks );
                                                        });
                                                        if( baseLevelHolderCollapsed ) {
                                                                blpush = ( instance.settings.fullCollapse ) ? $baseLevelHolder.width() : ( $baseLevelHolder.width() - instance.settings.overlapWidth );
                                                                pushContainers( blpush );
                                                        }
                                                        if( instance.settings.mode == 'overlap' ){
                                                                lpush = ( ( baseLevelHolderCollapsed ) ? ( baseWidth + ( parentLevelHoldersLen -  ( ( instance.settings.fullCollapse ) ? 0 : 1 ) ) * ( instance.settings.overlapWidth + ieShadowFilterDistortion ) ) : ( ( parentLevelHoldersLen - activeLevel ) * ( instance.settings.overlapWidth + ieShadowFilterDistortion ) ) );
                                                                pushContainers( lpush );
                                                        }
                                                        $selectedLevelHolder.css( 'visibility' , 'visible' );
                                                        $selectedLevelHolder.find( '.' + instance.settings.backItemClass ).css( 'visibility' , 'visible' );
                                                        $selectedLevelHolder.find( 'ul' ).css( 'visibility' , 'visible' );
                                                        $selectedLevelHolder.removeClass( instance.settings.menuInactiveClass );
                                                }
                                                else {
                                                        $.error( 'Invalid menu object provided' );
                                                }
                                        }
                                        else {
                                                $.error( 'No or too many menus named ' + menuTitle );
                                        }
                                }
                                expandingObjects[ 'expandingEnded' ] = true;
                                animatedEventCallback( expandingObjects , callbacks );
                        }

                        // Expand menu
                        function expandMenu() {
                                var menu = arguments[0],
                                        $expandLevelHolder,
                                        $activeLevelHolder = activeMenu(),
                                        $sharedLevelHolders, collapseLevel, $searchRes;
                                if( typeof menu == 'object' ) {
                                        $expandLevelHolder = menu;
                                }
                                else if( typeof menu == 'string' ){
                                        $searchRes = findMenusByTitle( menu );
                                        if($searchRes) {
                                                $expandLevelHolder = $searchRes.eq( 0 );
                                        }
                                        else {
                                                $.error( menu + ' menu level does not exist!' );
                                        }
                                }
                                else {
                                        $expandLevelHolder = $('#' + instance.settings.menuID + ' div.levelHolderClass:first');
                                }
                                $sharedLevelHolders = comparePaths( $expandLevelHolder , $activeLevelHolder, true );
                                collapseLevel = ( $sharedLevelHolders.length > 0 ) ? Math.max.apply( null,
                                        $sharedLevelHolders.map(function(){ return $(this).attr( 'data-level' ); }).get() ) : 0;
                                if( collapseLevel < $activeLevelHolder.attr( 'data-level' ) ) {
                                        collapseMenu( collapseLevel , [ { 'method' : expandMenuActions, 'args' : arguments } ] );
                                }
                                else {
                                        expandMenuActions.apply( this, Array.prototype.slice.call( arguments ) );
                                }
                                return $this;
                        }

                        // Find menu(s) by Title text
                        function findMenusByTitle() {
                                var menuTitle = arguments[0],
                                        response,
                                        $selectedLevelHolders = instance.settings.container
                                        .find( '#' + instance.settings.menuID + ' div.levelHolderClass' )
                                        .filter(function(){
                                                return ( ($( this ).children( 'h2' ).text() == menuTitle ) );
                                        });
                                if( $selectedLevelHolders.length > 0 ) {
                                        returnValue = $selectedLevelHolders;
                                        response = returnValue;
                                }
                                else {
                                        returnValue = false;
                                        response = returnValue;
                                }
                                return response;
                        }

                        // Find item(s) by Name
                        function findItemsByName() {
                                var itemName = arguments[0],
                                        response,
                                        $selectedItems = instance.settings.container
                                        .find( '#' + instance.settings.menuID + ' div.levelHolderClass li' )
                                        .filter(function(){
                                                return ( ($( this ).children( 'a' ).text() == itemName ) );
                                        });
                                if( $selectedItems.length > 0 ) {
                                        returnValue = $selectedItems;
                                        response = returnValue;
                                }
                                else {
                                        returnValue = false;
                                        response = returnValue;
                                }
                                return response;
                        }

                        // Find pathToRoot for provided menu
                        function pathToRoot() {
                                var $selectedLevelHolder = arguments[0],
                                        $parentLevelHolders, setToOpenHolders, response;
                                if( $selectedLevelHolder == undefined || $selectedLevelHolder.length != 1 ) {
                                        returnValue = false;
                                        return returnValue;
                                };
                                $parentLevelHolders = $selectedLevelHolder.parents( 'div.levelHolderClass' );
                                setToOpenHolders = $.merge( $parentLevelHolders.get().reverse(), $selectedLevelHolder.get() );
                                returnValue = setToOpenHolders;
                                return returnValue;
                        }

                        // Finds the same part of the path to root of two provided menus 
                        function comparePaths() {
                                var $levelHolder0 = arguments[0],
                                        $levelHolder1 = arguments[1],
                                        mode = ( arguments[2] != undefined ) ? arguments[2] : false,
                                        $parentLevelHolders0, $parentLevelHolders1, setParents0, setParents1, lPath, sPath, comparePath, response;
                                if( $levelHolder0 == undefined || $levelHolder1 == undefined ) {
                                        returnValue = false;
                                        return returnValue;
                                };
                                $parentLevelHolders0 = ( $levelHolder0.length == 1 ) ? $levelHolder0.parents( 'div.levelHolderClass' ) : null;
                                $parentLevelHolders1 = ( $levelHolder1.length == 1 ) ? $levelHolder1.parents( 'div.levelHolderClass' ) : null;
                                setParents0 = ( $parentLevelHolders0 != null ) ? $.merge( $parentLevelHolders0.get().reverse(), $levelHolder0.get() ) : [];
                                setParents1 = ( $parentLevelHolders1 != null ) ? $.merge( $parentLevelHolders1.get().reverse(), $levelHolder1.get() ) : [];
                                lPath = ( setParents0.length >= setParents1.length  ) ? setParents0 : setParents1;
                                sPath = ( lPath === setParents0  ) ? setParents1 : setParents0;
                                comparePath = $( lPath ).filter(function() {
                                        return ( mode ) ? ( $.inArray( this, sPath ) != -1 ) : ( $.inArray( this, sPath ) == -1 );
                                });
                                returnValue = comparePath;
                                return returnValue;
                        }

                        // Active menu
                        function activeMenu() {
                                var $activeLevelHolders = instance.settings.container
                                        .find( '#' + instance.settings.menuID + ' div.levelHolderClass' )
                                        .filter(function(){
                                                var retObjs = ( instance.settings.direction == 'rtl' ) ?
                                                        ((parseInt( $( this ).css( 'margin-right' ) ) >= 0 && $( this ).position().left < instance.settings.container.width() - instance.settings.overlapWidth ) )
                                                        :
                                                        ((parseInt( $( this ).css( 'margin-left' ) ) >= 0 && $( this ).position().left >= 0 ) );
                                                return retObjs;
                                        }),
                                        maxLevel = Math.max.apply( null,
                                        $activeLevelHolders.map(function(){ return $(this).attr( 'data-level' ); }).get() );
                                $activeLevelHolder = $activeLevelHolders.filter(function(){
                                        return $( this ).attr( 'data-level' ) == maxLevel;
                                });
                                returnValue = $activeLevelHolder;
                                return returnValue;
                        }

                        // Menu expanded
                        function menuExpanded() {
                                var $levelHolder = arguments[0],
                                        returnValue = false;
                                if( $levelHolder == undefined ) return returnValue;

                                var check = ( instance.settings.direction == 'rtl' ) ?
                                        ( parseInt( $levelHolder.css( 'margin-right' ) ) >= 0 && $levelHolder.position().left < instance.settings.container.width() - instance.settings.overlapWidth )
                                        :
                                        ( parseInt( $levelHolder.css( 'margin-left' ) ) >= 0 && $levelHolder.position().left >= 0 );
                                return check;
                        }

                        // Add item(s)
                        function addItems() {
                                var items = arguments[0],
                                        $levelHolder = arguments[1],
                                        position = arguments[2];
                                if( $levelHolder == undefined || typeof items != 'object' ) return false;
                                if( items.level == undefined ) items.level = parseInt( $levelHolder.attr( 'data-level' ) , 10 );
                                if( position == undefined ) position = 0;
                                var $itemGroup = $levelHolder.find( 'ul:first' );
                                $.each(items, function() {
                                        if( this.name != undefined )
                                                createItem( this, $levelHolder, position );
                                });
                                sizeDOMelements( instance.settings.menuWidth , undefined , $levelHolder.find( 'div.levelHolderClass' ) );
                                return $this;
                        }

                        // Remove item(s)
                        function removeItems() {
                                var $items = arguments[0];
                                if( $items == undefined || typeof $items != 'object' || $items.length == 0 ) return false;
                                $items.remove();
                                var $activeMenu = activeMenu();
                                if( $activeMenu.length == 1 ) {
                                        $activeMenu.css( 'visibility' , 'visible' );
                                        $activeMenu.find( '.' + instance.settings.backItemClass ).css( 'visibility' , 'visible' );
                                        $activeMenu.find( 'ul' ).css( 'visibility' , 'visible' );
                                        $activeMenu.removeClass( instance.settings.menuInactiveClass );
                                        var widthDiff = $activeMenu.width() - instance.settings.menuWidth;
                                        if( widthDiff != 0 ) {
                                                var $visibleLevelHolders = instance.settings.container
                                                        .find( '#' + instance.settings.menuID + ' div.levelHolderClass' )
                                                        .filter(function(){
                                                                var retObjs = ( instance.settings.direction == 'rtl' ) ?
                                                                        ( parseInt( $( this ).css( 'margin-right' ) ) >= 0 && $( this ).position().left < instance.settings.container.width() - instance.settings.overlapWidth )
                                                                        :
                                                                        ( parseInt( $( this ).css( 'margin-left' ) ) >= 0 && $( this ).position().left >= 0 );
                                                                return retObjs;
                                                        });
                                                $visibleLevelHolders.each(function(){
                                                        $( this ).width( $( this ).width() - widthDiff );
                                                });
                                        }
                                }
                                return $this;
                        }

                        // Manage multiple animated events and associated callbacks
                        function animatedEventCallback( animatedObjects, callbacks ) {
                                var doCallBack = true;
                                $.each( animatedObjects, function( key, val ){
                                        doCallBack = doCallBack && val;
                                });
                                if( doCallBack )
                                        window.setTimeout(function(){
                                                $.each( callbacks, function( key, val ){
                                                        val['method'].apply( this, Array.prototype.slice.call( val['args'] ) );
                                                });
                                        }, 1);
                        }

                        // Get/set settings options
                        function manageOptions() {
                                var response = false;
                                if( instance.settings[arguments[0]] != undefined ) {
                                        if( arguments[1] != undefined )
                                                instance.settings[arguments[0]] = arguments[1];
                                        response = instance.settings[arguments[0]];
                                } else {
                                        $.error('No option ' + arguments[0] + ' found in jQuery.multilevelpushmenu');
                                }
                                return response;
                        }
                        
                        // Mobile check
                        // http://coveroverflow.com/a/11381730/989439
                        function mobileCheck() {
                                var check = false;
                                (function(a){if(/(android|ipad|playbook|silk|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)))check = true})(navigator.userAgent||navigator.vendor||window.opera);
                                return check;
                        }

                        if( mobileCheck() ) {
                                clickEventType = 'touchend';
                                dragEventType = 'touchmove';
                        }
                        else {
                                clickEventType = 'click';
                                dragEventType = 'mousedown';
                        }

                        // Invoke called method or init
                        if ( methods[options] ) {
                                returnValue = methods[options].apply(this, Array.prototype.slice.call(args, 1));
                                return returnValue;
                        } else if (typeof options === 'object' || !options) {
                                returnValue = methods.init.apply(this, arguments);
                                return returnValue;
                        } else {
                                $.error('No ' + options + ' method found in jQuery.multilevelpushmenu');
                        }

                        // Return object instance or option value
                        if (!returnValue) {
                                returnValue = this;
                        }
                });
                return returnValue;
        }
}( jQuery ));

/*
 * Lazy Load - jQuery plugin for lazy loading images
 *
 * Copyright (c) 2007-2013 Mika Tuupola
 *
 * Licensed under the MIT license:
 *   http://www.opensource.org/licenses/mit-license.php
 *
 * Project home:
 *   http://www.appelsiini.net/projects/lazyload
 *
 * Version:  1.9.1
 *
 */

(function($, window, document, undefined) {
    var $window = $(window);

    $.fn.lazyload = function(options) {
        var elements = this;
        var $container;
        var settings = {
            threshold       : 0,
            failure_limit   : 0,
            event           : "scroll",
            effect          : "show",
            container       : window,
            data_attribute  : "original",
            skip_invisible  : true,
            appear          : null,
            load            : null,
            placeholder     : "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC"
        };

        function update() {
            var counter = 0;

            elements.each(function() {
                var $this = $(this);
                if (settings.skip_invisible && !$this.is(":visible")) {
                    return;
                }
                if ($.abovethetop(this, settings) ||
                    $.leftofbegin(this, settings)) {
                        /* Nothing. */
                } else if (!$.belowthefold(this, settings) &&
                    !$.rightoffold(this, settings)) {
                        $this.trigger("appear");
                        /* if we found an image we'll load, reset the counter */
                        counter = 0;
                } else {
                    if (++counter > settings.failure_limit) {
                        return false;
                    }
                }
            });

        }

        if(options) {
            /* Maintain BC for a couple of versions. */
            if (undefined !== options.failurelimit) {
                options.failure_limit = options.failurelimit;
                delete options.failurelimit;
            }
            if (undefined !== options.effectspeed) {
                options.effect_speed = options.effectspeed;
                delete options.effectspeed;
            }

            $.extend(settings, options);
        }

        /* Cache container as jQuery as object. */
        $container = (settings.container === undefined ||
                      settings.container === window) ? $window : $(settings.container);

        /* Fire one scroll event per scroll. Not one scroll event per image. */
        if (0 === settings.event.indexOf("scroll")) {
            $container.bind(settings.event, function() {
                return update();
            });
        }

        this.each(function() {
            var self = this;
            var $self = $(self);

            self.loaded = false;

            /* If no src attribute given use data:uri. */
            if ($self.attr("src") === undefined || $self.attr("src") === false) {
                if ($self.is("img")) {
                    $self.attr("src", settings.placeholder);
                }
            }

            /* When appear is triggered load original image. */
            $self.one("appear", function() {
                if (!this.loaded) {
                    if (settings.appear) {
                        var elements_left = elements.length;
                        settings.appear.call(self, elements_left, settings);
                    }
                    $("<img />")
                        .bind("load", function() {

                            var original = $self.attr("data-" + settings.data_attribute);
                            $self.hide();
                            if ($self.is("img")) {
                                $self.attr("src", original);
                            } else {
                                $self.css("background-image", "url('" + original + "')");
                            }
                            $self[settings.effect](settings.effect_speed);

                            self.loaded = true;

                            /* Remove image from array so it is not looped next time. */
                            var temp = $.grep(elements, function(element) {
                                return !element.loaded;
                            });
                            elements = $(temp);

                            if (settings.load) {
                                var elements_left = elements.length;
                                settings.load.call(self, elements_left, settings);
                            }
                        })
                        .attr("src", $self.attr("data-" + settings.data_attribute));
                }
            });

            /* When wanted event is triggered load original image */
            /* by triggering appear.                              */
            if (0 !== settings.event.indexOf("scroll")) {
                $self.bind(settings.event, function() {
                    if (!self.loaded) {
                        $self.trigger("appear");
                    }
                });
            }
        });

        /* Check if something appears when window is resized. */
        $window.bind("resize", function() {
            update();
        });

        /* With IOS5 force loading images when navigating with back button. */
        /* Non optimal workaround. */
        if ((/(?:iphone|ipod|ipad).*os 5/gi).test(navigator.appVersion)) {
            $window.bind("pageshow", function(event) {
                if (event.originalEvent && event.originalEvent.persisted) {
                    elements.each(function() {
                        $(this).trigger("appear");
                    });
                }
            });
        }

        /* Force initial check if images should appear. */
        $(document).ready(function() {
            update();
        });

        return this;
    };

    /* Convenience methods in jQuery namespace.           */
    /* Use as  $.belowthefold(element, {threshold : 100, container : window}) */

    $.belowthefold = function(element, settings) {
        var fold;

        if (settings.container === undefined || settings.container === window) {
            fold = (window.innerHeight ? window.innerHeight : $window.height()) + $window.scrollTop();
        } else {
            fold = $(settings.container).offset().top + $(settings.container).height();
        }

        return fold <= $(element).offset().top - settings.threshold;
    };

    $.rightoffold = function(element, settings) {
        var fold;

        if (settings.container === undefined || settings.container === window) {
            fold = $window.width() + $window.scrollLeft();
        } else {
            fold = $(settings.container).offset().left + $(settings.container).width();
        }

        return fold <= $(element).offset().left - settings.threshold;
    };

    $.abovethetop = function(element, settings) {
        var fold;

        if (settings.container === undefined || settings.container === window) {
            fold = $window.scrollTop();
        } else {
            fold = $(settings.container).offset().top;
        }

        return fold >= $(element).offset().top + settings.threshold  + $(element).height();
    };

    $.leftofbegin = function(element, settings) {
        var fold;

        if (settings.container === undefined || settings.container === window) {
            fold = $window.scrollLeft();
        } else {
            fold = $(settings.container).offset().left;
        }

        return fold >= $(element).offset().left + settings.threshold + $(element).width();
    };

    $.inviewport = function(element, settings) {
         return !$.rightoffold(element, settings) && !$.leftofbegin(element, settings) &&
                !$.belowthefold(element, settings) && !$.abovethetop(element, settings);
     };

    /* Custom selectors for your convenience.   */
    /* Use as $("img:below-the-fold").something() or */
    /* $("img").filter(":below-the-fold").something() which is faster */

    $.extend($.expr[":"], {
        "below-the-fold" : function(a) { return $.belowthefold(a, {threshold : 0}); },
        "above-the-top"  : function(a) { return !$.belowthefold(a, {threshold : 0}); },
        "right-of-screen": function(a) { return $.rightoffold(a, {threshold : 0}); },
        "left-of-screen" : function(a) { return !$.rightoffold(a, {threshold : 0}); },
        "in-viewport"    : function(a) { return $.inviewport(a, {threshold : 0}); },
        /* Maintain BC for couple of versions. */
        "above-the-fold" : function(a) { return !$.belowthefold(a, {threshold : 0}); },
        "right-of-fold"  : function(a) { return $.rightoffold(a, {threshold : 0}); },
        "left-of-fold"   : function(a) { return !$.rightoffold(a, {threshold : 0}); }
    });

})(jQuery, window, document);


/**
 * hoverIntent is similar to jQuery's built-in "hover" method except that
 * instead of firing the handlerIn function immediately, hoverIntent checks
 * to see if the user's mouse has slowed down (beneath the sensitivity
 * threshold) before firing the event. The handlerOut function is only
 * called after a matching handlerIn.
 *
 * hoverIntent r7 // 2013.03.11 // jQuery 1.9.1+
 * http://cherne.net/brian/resources/jquery.hoverIntent.html
 *
 **/
(function($) {
    $.fn.hoverIntent = function(handlerIn,handlerOut,selector) {

        // default configuration values
        var cfg = {
            interval: 100,
            sensitivity: 7,
            timeout: 0
        };

        if ( typeof handlerIn === "object" ) {
            cfg = $.extend(cfg, handlerIn );
        } else if ($.isFunction(handlerOut)) {
            cfg = $.extend(cfg, { over: handlerIn, out: handlerOut, selector: selector } );
        } else {
            cfg = $.extend(cfg, { over: handlerIn, out: handlerIn, selector: handlerOut } );
        }

        // instantiate variables
        // cX, cY = current X and Y position of mouse, updated by mousemove event
        // pX, pY = previous X and Y position of mouse, set by mouseover and polling interval
        var cX, cY, pX, pY;

        // A private function for getting mouse position
        var track = function(ev) {
            cX = ev.pageX;
            cY = ev.pageY;
        };

        // A private function for comparing current and previous mouse position
        var compare = function(ev,ob) {
            ob.hoverIntent_t = clearTimeout(ob.hoverIntent_t);
            // compare mouse positions to see if they've crossed the threshold
            if ( ( Math.abs(pX-cX) + Math.abs(pY-cY) ) < cfg.sensitivity ) {
                $(ob).off("mousemove.hoverIntent",track);
                // set hoverIntent state to true (so mouseOut can be called)
                ob.hoverIntent_s = 1;
                return cfg.over.apply(ob,[ev]);
            } else {
                // set previous coordinates for next time
                pX = cX; pY = cY;
                // use self-calling timeout, guarantees intervals are spaced out properly (avoids JavaScript timer bugs)
                ob.hoverIntent_t = setTimeout( function(){compare(ev, ob);} , cfg.interval );
            }
        };

        // A private function for delaying the mouseOut function
        var delay = function(ev,ob) {
            ob.hoverIntent_t = clearTimeout(ob.hoverIntent_t);
            ob.hoverIntent_s = 0;
            return cfg.out.apply(ob,[ev]);
        };

        // A private function for handling mouse 'hovering'
        var handleHover = function(e) {
            // copy objects to be passed into t (required for event object to be passed in IE)
            var ev = jQuery.extend({},e);
            var ob = this;

            // cancel hoverIntent timer if it exists
            if (ob.hoverIntent_t) { ob.hoverIntent_t = clearTimeout(ob.hoverIntent_t); }

            // if e.type == "mouseenter"
            if (e.type == "mouseenter") {
                // set "previous" X and Y position based on initial entry point
                pX = ev.pageX; pY = ev.pageY;
                // update "current" X and Y position based on mousemove
                $(ob).on("mousemove.hoverIntent",track);
                // start polling interval (self-calling timeout) to compare mouse coordinates over time
                if (ob.hoverIntent_s != 1) { ob.hoverIntent_t = setTimeout( function(){compare(ev,ob);} , cfg.interval );}

                // else e.type == "mouseleave"
            } else {
                // unbind expensive mousemove event
                $(ob).off("mousemove.hoverIntent",track);
                // if hoverIntent state is true, then call the mouseOut function after the specified delay
                if (ob.hoverIntent_s == 1) { ob.hoverIntent_t = setTimeout( function(){delay(ev,ob);} , cfg.timeout );}
            }
        };

        // listen for mouseenter and mouseleave
        return this.on({'mouseenter.hoverIntent':handleHover,'mouseleave.hoverIntent':handleHover}, cfg.selector);
    };
})(jQuery);

/*
 * jQuery Superfish Menu Plugin - v1.7.4
 * Copyright (c) 2013 Joel Birch
 *
 * Dual licensed under the MIT and GPL licenses:
 *	http://www.opensource.org/licenses/mit-license.php
 *	http://www.gnu.org/licenses/gpl.html
 */

;(function ($) {
	"use strict";

	var methods = (function () {
		// private properties and methods go here
		var c = {
				bcClass: 'sf-breadcrumb',
				menuClass: 'sf-js-enabled',
				anchorClass: 'sf-with-ul',
				menuArrowClass: 'sf-arrows'
			},
			ios = (function () {
				var ios = /iPhone|iPad|iPod/i.test(navigator.userAgent);
				if (ios) {
					// iOS clicks only bubble as far as body children
					$(window).load(function () {
						$('body').children().on('click', $.noop);
					});
				}
				return ios;
			})(),
			wp7 = (function () {
				var style = document.documentElement.style;
				return ('behavior' in style && 'fill' in style && /iemobile/i.test(navigator.userAgent));
			})(),
			toggleMenuClasses = function ($menu, o) {
				var classes = c.menuClass;
				if (o.cssArrows) {
					classes += ' ' + c.menuArrowClass;
				}
				$menu.toggleClass(classes);
			},
			setPathToCurrent = function ($menu, o) {
				return $menu.find('li.' + o.pathClass).slice(0, o.pathLevels)
					.addClass(o.hoverClass + ' ' + c.bcClass)
						.filter(function () {
							return ($(this).children(o.popUpSelector).hide().show().length);
						}).removeClass(o.pathClass);
			},
			toggleAnchorClass = function ($li) {
				$li.children('a').toggleClass(c.anchorClass);
			},
			toggleTouchAction = function ($menu) {
				var touchAction = $menu.css('ms-touch-action');
				touchAction = (touchAction === 'pan-y') ? 'auto' : 'pan-y';
				$menu.css('ms-touch-action', touchAction);
			},
			applyHandlers = function ($menu, o) {
				var targets = 'li:has(' + o.popUpSelector + ')';
				if ($.fn.hoverIntent && !o.disableHI) {
					$menu.hoverIntent(over, out, targets);
				}
				else {
					$menu
						.on('mouseenter.superfish', targets, over)
						.on('mouseleave.superfish', targets, out);
				}
				var touchevent = 'MSPointerDown.superfish';
				if (!ios) {
					touchevent += ' touchend.superfish';
				}
				if (wp7) {
					touchevent += ' mousedown.superfish';
				}
				$menu
					.on('focusin.superfish', 'li', over)
					.on('focusout.superfish', 'li', out)
					.on(touchevent, 'a', o, touchHandler);
			},
			touchHandler = function (e) {
				var $this = $(this),
					$ul = $this.siblings(e.data.popUpSelector);

				if ($ul.length > 0 && $ul.is(':hidden')) {
					$this.one('click.superfish', false);
					if (e.type === 'MSPointerDown') {
						$this.trigger('focus');
					} else {
						$.proxy(over, $this.parent('li'))();
					}
				}
			},
			over = function () {
				var $this = $(this),
					o = getOptions($this);
				clearTimeout(o.sfTimer);
				$this.siblings().superfish('hide').end().superfish('show');
			},
			out = function () {
				var $this = $(this),
					o = getOptions($this);
				if (ios) {
					$.proxy(close, $this, o)();
				}
				else {
					clearTimeout(o.sfTimer);
					o.sfTimer = setTimeout($.proxy(close, $this, o), o.delay);
				}
			},
			close = function (o) {
				o.retainPath = ($.inArray(this[0], o.$path) > -1);
				this.superfish('hide');

				if (!this.parents('.' + o.hoverClass).length) {
					o.onIdle.call(getMenu(this));
					if (o.$path.length) {
						$.proxy(over, o.$path)();
					}
				}
			},
			getMenu = function ($el) {
				return $el.closest('.' + c.menuClass);
			},
			getOptions = function ($el) {
				return getMenu($el).data('sf-options');
			};

		return {
			// public methods
			hide: function (instant) {
				if (this.length) {
					var $this = this,
						o = getOptions($this);
					if (!o) {
						return this;
					}
					var not = (o.retainPath === true) ? o.$path : '',
						$ul = $this.find('li.' + o.hoverClass).add(this).not(not).removeClass(o.hoverClass).children(o.popUpSelector),
						speed = o.speedOut;

					if (instant) {
						$ul.show();
						speed = 0;
					}
					o.retainPath = false;
					o.onBeforeHide.call($ul);
					$ul.stop(true, true).animate(o.animationOut, speed, function () {
						var $this = $(this);
						o.onHide.call($this);
					});
				}
				return this;
			},
			show: function () {
				var o = getOptions(this);
				if (!o) {
					return this;
				}
				var $this = this.addClass(o.hoverClass),
					$ul = $this.children(o.popUpSelector);

				o.onBeforeShow.call($ul);
				$ul.stop(true, true).animate(o.animation, o.speed, function () {
					o.onShow.call($ul);
				});
				return this;
			},
			destroy: function () {
				return this.each(function () {
					var $this = $(this),
						o = $this.data('sf-options'),
						$hasPopUp;
					if (!o) {
						return false;
					}
					$hasPopUp = $this.find(o.popUpSelector).parent('li');
					clearTimeout(o.sfTimer);
					toggleMenuClasses($this, o);
					toggleAnchorClass($hasPopUp);
					toggleTouchAction($this);
					// remove event handlers
					$this.off('.superfish').off('.hoverIntent');
					// clear animation's inline display style
					$hasPopUp.children(o.popUpSelector).attr('style', function (i, style) {
						return style.replace(/display[^;]+;?/g, '');
					});
					// reset 'current' path classes
					o.$path.removeClass(o.hoverClass + ' ' + c.bcClass).addClass(o.pathClass);
					$this.find('.' + o.hoverClass).removeClass(o.hoverClass);
					o.onDestroy.call($this);
					$this.removeData('sf-options');
				});
			},
			init: function (op) {
				return this.each(function () {
					var $this = $(this);
					if ($this.data('sf-options')) {
						return false;
					}
					var o = $.extend({}, $.fn.superfish.defaults, op),
						$hasPopUp = $this.find(o.popUpSelector).parent('li');
					o.$path = setPathToCurrent($this, o);

					$this.data('sf-options', o);

					toggleMenuClasses($this, o);
					toggleAnchorClass($hasPopUp);
					toggleTouchAction($this);
					applyHandlers($this, o);

					$hasPopUp.not('.' + c.bcClass).superfish('hide', true);

					o.onInit.call(this);
				});
			}
		};
	})();

	$.fn.superfish = function (method, args) {
		if (methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
		}
		else if (typeof method === 'object' || ! method) {
			return methods.init.apply(this, arguments);
		}
		else {
			return $.error('Method ' +  method + ' does not exist on jQuery.fn.superfish');
		}
	};

	$.fn.superfish.defaults = {
		popUpSelector: 'ul,.sf-mega', // within menu context
		hoverClass: 'sfHover',
		pathClass: 'overrideThisToUse',
		pathLevels: 1,
		delay: 800,
		animation: {opacity: 'show'},
		animationOut: {opacity: 'hide'},
		speed: 'normal',
		speedOut: 'fast',
		cssArrows: true,
		disableHI: false,
		onInit: $.noop,
		onBeforeShow: $.noop,
		onShow: $.noop,
		onBeforeHide: $.noop,
		onHide: $.noop,
		onIdle: $.noop,
		onDestroy: $.noop
	};

	// soon to be deprecated
	$.fn.extend({
		hideSuperfishUl: methods.hide,
		showSuperfishUl: methods.show
	});

})(jQuery);



/*
 * Superfish v1.4.8 - jQuery menu widget
 */

// ;(function($){$.fn.superfish=function(op){var sf=$.fn.superfish,c=sf.c,$arrow=$(['<span class="',c.arrowClass,'"> &#187;</span>'].join('')),over=function(){var $$=$(this),menu=getMenu($$);clearTimeout(menu.sfTimer);$$.showSuperfishUl().siblings().hideSuperfishUl()},out=function(){var $$=$(this),menu=getMenu($$),o=sf.op;clearTimeout(menu.sfTimer);menu.sfTimer=setTimeout(function(){o.retainPath=($.inArray($$[0],o.$path)>-1);$$.hideSuperfishUl();if(o.$path.length&&$$.parents(['li.',o.hoverClass].join('')).length<1){over.call(o.$path)}},o.delay)},getMenu=function($menu){var menu=$menu.parents(['ul.',c.menuClass,':first'].join(''))[0];sf.op=sf.o[menu.serial];return menu},addArrow=function($a){$a.addClass(c.anchorClass).append($arrow.clone())};return this.each(function(){var s=this.serial=sf.o.length;var o=$.extend({},sf.defaults,op);o.$path=$('li.'+o.pathClass,this).slice(0,o.pathLevels).each(function(){$(this).addClass([o.hoverClass,c.bcClass].join(' ')).filter('li:has(ul)').removeClass(o.pathClass)});sf.o[s]=sf.op=o;$('li:has(ul)',this)[($.fn.hoverIntent&&!o.disableHI)?'hoverIntent':'hover'](over,out).each(function(){if(o.autoArrows)addArrow($('>a:first-child',this))}).not('.'+c.bcClass).hideSuperfishUl();var $a=$('a',this);$a.each(function(i){var $li=$a.eq(i).parents('li');$a.eq(i).focus(function(){over.call($li)}).blur(function(){out.call($li)})});o.onInit.call(this)}).each(function(){var menuClasses=[c.menuClass];if(sf.op.dropShadows&&!($.browser.msie&&$.browser.version<7))menuClasses.push(c.shadowClass);$(this).addClass(menuClasses.join(' '))})};var sf=$.fn.superfish;sf.o=[];sf.op={};sf.IE7fix=function(){var o=sf.op;if($.browser.msie&&$.browser.version>6&&o.dropShadows&&o.animation.opacity!=undefined)this.toggleClass(sf.c.shadowClass+'-off')};sf.c={bcClass:'sf-breadcrumb',menuClass:'sf-js-enabled',anchorClass:'sf-with-ul',arrowClass:'sf-sub-indicator',shadowClass:'sf-shadow'};sf.defaults={hoverClass:'sfHover',pathClass:'overideThisToUse',pathLevels:1,delay:800,animation:{opacity:'show'},speed:'normal',autoArrows:true,dropShadows:true,disableHI:false,onInit:function(){},onBeforeShow:function(){},onShow:function(){},onHide:function(){}};$.fn.extend({hideSuperfishUl:function(){var o=sf.op,not=(o.retainPath===true)?o.$path:'';o.retainPath=false;var $ul=$(['li.',o.hoverClass].join(''),this).add(this).not(not).removeClass(o.hoverClass).find('>ul').hide().css('visibility','hidden');o.onHide.call($ul);return this},showSuperfishUl:function(){var o=sf.op,sh=sf.c.shadowClass+'-off',$ul=this.addClass(o.hoverClass).find('>ul:hidden').css('visibility','visible');sf.IE7fix.call($ul);o.onBeforeShow.call($ul);$ul.animate(o.animation,o.speed,function(){sf.IE7fix.call($ul);o.onShow.call($ul)});return this}})})(jQuery);

/* 
 * jQuery - Easy Ticker plugin - v1.0
 * http://www.aakashweb.com/
 * Copyright 2012, Aakash Chakravarthy
 * Released under the MIT License.
 */

(function($){
	$.fn.easyTicker = function(options) {
	
	var defaults = {
		direction: 'up',
   		easing: 'swing',
		speed: 'slow',
		interval: 2000,
		height: 'auto',
		visible: 0,
		mousePause: 1,
		controls:{
			up: '',
			down: '',
			toggle: ''
		},
        onLoaded: $.noop,
        afterAjax: $.noop,
        ajax: false    
	};
	
	// Initialize the variables
	var options = $.extend(defaults, options), 
		timer = 0,
		tClass = 'et-run',
		winFocus = 0,
		vBody = $('body'),
		cUp = $(options.controls.up),
		cDown = $(options.controls.down),
		cToggle = $(options.controls.toggle),
        isAjaxRunning = false;

	
	// The initializing function
	var init = function(obj, target){
		
		// target.children().css('margin', 0);
		
		obj.css({
			position : 'relative',
			height : (options.height == 'auto') ? objHeight(obj, target) : options.height,
			overflow : 'hidden'
		});
		
		target.css({
			'position' : 'absolute'
		});
		
		if(options.visible != 0 && options.height == 'auto'){
			adjHeight(obj, target);
		}

		// Set the class to the "toggle" control and set the timer.
		cToggle.addClass(tClass);
		setTimer(obj, target);
        options.onLoaded.apply(obj);
	}
	var animate_move = function (obj, target, type, done) {       

        if(!obj.is(':visible')) return;
        
        if(type == 'up'){
            var sel = ':first-child',
                eq = '-=',
                appType = 'appendTo';
        } else{
            var sel = ':last-child',
                eq = '+=',
                appType = 'prependTo';
        }
        
        
        var selChild = $(target).children(sel);
        var height = selChild.outerHeight();
    
        $(target).stop(true, true).animate({
            'top': eq + height + "px"
        }, options.speed, options.easing, function(){
            
            if (!options.ajax) {
                 selChild.hide()[appType](target);
                 selChild.fadeIn();
            }

            $(target).css('top', 0);

            if(options.visible != 0 && options.height == 'auto'){
                adjHeight(obj, target);
            }

            if (done) done.apply($(this));
        });
    }

	// Core function to move the element up and down.
	var move = function(obj, target, type){
        if (options.ajax) {
            if(!obj.is(':visible')) return;
            // if(type == 'up'){
                if (!isAjaxRunning) {
                    var $obj = obj;
                    //var $last_child = $obj.find('li:last-child');
                    
                    $.ajax({
                        url: dlh_ajax.ajax_url,
                        type : 'post',
                        dataType : 'json',
                        data : {
                            'action': 'dlh_easy_ticker',
                            'nonce': dlh_ajax.ajax_nonce,
                            //'data_id': $last_child.data('id'),
                            'data_type' : (type == 'up')?'older':'newer',
                            'data_tag':  $obj.data('tag'),
                            'data_page_top': $obj.data('page-top'),
                            'data_page_bottom': $obj.data('page-bottom'),
                            'data_title_limit': $obj.data('title-limit'),
                            'data_excerpt_limit': $obj.data('excerpt-limit'),
                              
                        },
                        beforeSend: function () {
                            isAjaxRunning = true;
                        },
                        success: function(response) {
                            
                            if(response.type == "success") {
                                if(type == 'up'){
                                    animate_move ($obj, target, type, function () {
                                        var $first_child = $obj.find('li:first-child');
                                        $first_child.remove();
                                        $obj.find('ul').append(response.output);
                                        //make animated after append
                                        $obj.find('li:last-child').hide().fadeIn();
                                        options.afterAjax.apply($obj);
                                        $obj.data('page-top', $obj.data('page-top') + 1);
                                        $obj.data('page-bottom', $obj.data('page-bottom') + 1);
                                        isAjaxRunning = false;
                                    });
                                } else if (type == 'down') {
                                    animate_move ($obj, target, type, function () {

                                        var $last_child = $obj.find('li:last-child');
                                        $last_child.remove();
                                        $obj.find('ul').prepend(response.output);
                                        //make animated after prepend
                                        $obj.find('li:first-child').hide().fadeIn();
                                        options.afterAjax.apply($obj);
                                        $obj.data('page-top', $obj.data('page-top') -1 );
                                        $obj.data('page-bottom', $obj.data('page-bottom') - 1);
                                        isAjaxRunning = false;
                                   });
                                }  
                               
                            } else if(response.type == "empty") {
                                if (options.direction == 'up') options.direction = 'down';
                                else if (options.direction == 'down') options.direction = 'up';
                                isAjaxRunning = false;                                   
                            }


                        }
                    });
                }
            // }
        } else {
            animate_move(obj, target, type);
        }	
        
	}
	
	// Activates the timer.
	var setTimer = function(obj, target){
		if(cToggle.length == 0 || cToggle.hasClass(tClass)){
			timer = setInterval(function(){
				if (vBody.attr('data-focus') != 1){ return; }
				move(obj, target, options.direction);
			}, options.interval);
		}
	}
	
	// Stops the timer
	var stopTimer = function(obj){
		clearInterval(timer);
	}
	
	// Adjust the wrapper height and show the visible elements only.
	var adjHeight = function(obj, target){
		var wrapHeight = 0;
		$(target).children(':lt(' + options.visible + ')').each(function(){
			wrapHeight += $(this).outerHeight();
		});
		
		obj.stop(true, true).animate({height: wrapHeight}, options.speed);
	}
	
	// Get the maximum height of the children.
	var objHeight = function(obj, target){
		var height = 0;
		
		var tempDisp = obj.css('display');
		obj.css('display', 'block');
				
		$(target).children().each(function(){
			height += $(this).outerHeight();
		});
		
		obj.css('display', tempDisp);
		return height;
	}
	
	// Hack to check window status
	function onBlur(){ vBody.attr('data-focus', 0); };
	function onFocus(){ vBody.attr('data-focus', 1); };
	
	if (/*@cc_on!@*/false) { // check for Internet Explorer
		document.onfocusin = onFocus;
		document.onfocusout = onBlur;
	}else{
		$(window).bind('focus mouseover', onFocus);
		$(window).bind('blur', onBlur);
	}

	return this.each(function(){
		var obj = $(this);
		var tar = obj.children(':first-child');
		
		// Initialize the content
		init(obj, tar);
		
		// Bind the mousePause action
		if(options.mousePause == 1){
			obj.mouseover(function(){
				stopTimer(obj);
			}).mouseleave(function(){
				setTimer(obj, tar);
			});
		}
		
		// Controls action
		cToggle.live('click', function(){
			if($(this).hasClass(tClass)){
				stopTimer(obj);
				$(this).removeClass(tClass);
			}else{
				$(this).addClass(tClass);
				setTimer(obj, tar);
			}
		});
		
		cUp.live('click', function(){
			move(obj, tar, 'up');
		});
		
		cDown.live('click', function(){
			move(obj, tar, 'down');
		});
		
	});
};
})(jQuery);

// Royal slider
// jquery.royalslider v9.4.92
(function(l){function t(b,f){var c,g,a=this,e=navigator.userAgent.toLowerCase();a.uid=l.rsModules.uid++;a.ns=".rs"+a.uid;var d=document.createElement("div").style,j=["webkit","Moz","ms","O"],h="",k=0;for(c=0;c<j.length;c++)g=j[c],!h&&g+"Transform"in d&&(h=g),g=g.toLowerCase(),window.requestAnimationFrame||(window.requestAnimationFrame=window[g+"RequestAnimationFrame"],window.cancelAnimationFrame=window[g+"CancelAnimationFrame"]||window[g+"CancelRequestAnimationFrame"]);window.requestAnimationFrame||
(window.requestAnimationFrame=function(a){var b=(new Date).getTime(),c=Math.max(0,16-(b-k)),d=window.setTimeout(function(){a(b+c)},c);k=b+c;return d});window.cancelAnimationFrame||(window.cancelAnimationFrame=function(a){clearTimeout(a)});a.isIPAD=e.match(/(ipad)/);j=/(chrome)[ \/]([\w.]+)/.exec(e)||/(webkit)[ \/]([\w.]+)/.exec(e)||/(opera)(?:.*version|)[ \/]([\w.]+)/.exec(e)||/(msie) ([\w.]+)/.exec(e)||0>e.indexOf("compatible")&&/(mozilla)(?:.*? rv:([\w.]+)|)/.exec(e)||[];c=j[1]||"";g=j[2]||"0";
j={};c&&(j[c]=!0,j.version=g);j.chrome&&(j.webkit=!0);a._a=j;a.isAndroid=-1<e.indexOf("android");a.slider=l(b);a.ev=l(a);a._b=l(document);a.st=l.extend({},l.fn.royalSlider.defaults,f);a._c=a.st.transitionSpeed;a._d=0;if(a.st.allowCSS3&&(!j.webkit||a.st.allowCSS3OnWebkit))e=h+(h?"T":"t"),a._e=e+"ransform"in d&&e+"ransition"in d,a._e&&(a._f=h+(h?"P":"p")+"erspective"in d);h=h.toLowerCase();a._g="-"+h+"-";a._h="vertical"===a.st.slidesOrientation?!1:!0;a._i=a._h?"left":"top";a._j=a._h?"width":"height";
a._k=-1;a._l="fade"===a.st.transitionType?!1:!0;a._l||(a.st.sliderDrag=!1,a._m=10);a._n="z-index:0; display:none; opacity:0;";a._o=0;a._p=0;a._q=0;l.each(l.rsModules,function(b,c){"uid"!==b&&c.call(a)});a.slides=[];a._r=0;(a.st.slides?l(a.st.slides):a.slider.children().detach()).each(function(){a._s(this,!0)});a.st.randomizeSlides&&a.slides.sort(function(){return 0.5-Math.random()});a.numSlides=a.slides.length;a._t();a.st.startSlideId?a.st.startSlideId>a.numSlides-1&&(a.st.startSlideId=a.numSlides-
1):a.st.startSlideId=0;a._o=a.staticSlideId=a.currSlideId=a._u=a.st.startSlideId;a.currSlide=a.slides[a.currSlideId];a._v=0;a.msTouch=!1;a.slider.addClass((a._h?"rsHor":"rsVer")+(a._l?"":" rsFade"));d='<div class="rsOverflow"><div class="rsContainer">';a.slidesSpacing=a.st.slidesSpacing;a._w=(a._h?a.slider.width():a.slider.height())+a.st.slidesSpacing;a._x=Boolean(0<a._y);1>=a.numSlides&&(a._z=!1);a._a1=a._z&&a._l?2===a.numSlides?1:2:0;a._b1=6>a.numSlides?a.numSlides:6;a._c1=0;a._d1=0;a.slidesJQ=
[];for(c=0;c<a.numSlides;c++)a.slidesJQ.push(l('<div style="'+(a._l?"":c!==a.currSlideId?a._n:"z-index:0;")+'" class="rsSlide "></div>'));a._e1=d=l(d+"</div></div>");h=a.ns;a.msEnabled=window.navigator.msPointerEnabled;a.msEnabled?(a.msTouch=Boolean(1<window.navigator.msMaxTouchPoints),a.hasTouch=!1,a._n1=0.2,a._j1="MSPointerDown"+h,a._k1="MSPointerMove"+h,a._l1="MSPointerUp"+h,a._m1="MSPointerCancel"+h):(a._j1="mousedown"+h,a._k1="mousemove"+h,a._l1="mouseup"+h,a._m1="mouseup"+h,"ontouchstart"in
window||"createTouch"in document?(a.hasTouch=!0,a._j1+=" touchstart"+h,a._k1+=" touchmove"+h,a._l1+=" touchend"+h,a._m1+=" touchcancel"+h,a._n1=0.5,a.st.sliderTouch&&(a._f1=!0)):(a.hasTouch=!1,a._n1=0.2));a.st.sliderDrag&&(a._f1=!0,j.msie||j.opera?a._g1=a._h1="move":j.mozilla?(a._g1="-moz-grab",a._h1="-moz-grabbing"):j.webkit&&-1!=navigator.platform.indexOf("Mac")&&(a._g1="-webkit-grab",a._h1="-webkit-grabbing"),a._i1());a.slider.html(d);a._o1=a.st.controlsInside?a._e1:a.slider;a._p1=a._e1.children(".rsContainer");
a.msEnabled&&a._p1.css("-ms-touch-action",a._h?"pan-y":"pan-x");a._q1=l('<div class="rsPreloader"></div>');d=a._p1.children(".rsSlide");a._r1=a.slidesJQ[a.currSlideId];a._s1=0;a._e?(a._t1="transition-property",a._u1="transition-duration",a._v1="transition-timing-function",a._w1=a._x1=a._g+"transform",a._f?(j.webkit&&!j.chrome&&a.slider.addClass("rsWebkit3d"),/iphone|ipad|ipod/gi.test(navigator.appVersion),a._y1="translate3d(",a._z1="px, ",a._a2="px, 0px)"):(a._y1="translate(",a._z1="px, ",a._a2="px)"),
a._l?a._p1[a._g+a._t1]=a._g+"transform":(h={},h[a._g+a._t1]="opacity",h[a._g+a._u1]=a.st.transitionSpeed+"ms",h[a._g+a._v1]=a.st.css3easeInOut,d.css(h))):(a._x1="left",a._w1="top");var n;l(window).on("resize"+a.ns,function(){n&&clearTimeout(n);n=setTimeout(function(){a.updateSliderSize()},50)});a.ev.trigger("rsAfterPropsSetup");a.updateSliderSize();a.st.keyboardNavEnabled&&a._b2();if(a.st.arrowsNavHideOnTouch&&(a.hasTouch||a.msTouch))a.st.arrowsNav=!1;a.st.arrowsNav&&(d=a._o1,l('<div class="rsArrow rsArrowLeft"><div class="rsArrowIcn"></div></div><div class="rsArrow rsArrowRight"><div class="rsArrowIcn"></div></div>').appendTo(d),
a._c2=d.children(".rsArrowLeft").click(function(b){b.preventDefault();a.prev()}),a._d2=d.children(".rsArrowRight").click(function(b){b.preventDefault();a.next()}),a.st.arrowsNavAutoHide&&!a.hasTouch&&(a._c2.addClass("rsHidden"),a._d2.addClass("rsHidden"),d.one("mousemove.arrowshover",function(){a._c2.removeClass("rsHidden");a._d2.removeClass("rsHidden")}),d.hover(function(){a._e2||(a._c2.removeClass("rsHidden"),a._d2.removeClass("rsHidden"))},function(){a._e2||(a._c2.addClass("rsHidden"),a._d2.addClass("rsHidden"))})),
a.ev.on("rsOnUpdateNav",function(){a._f2()}),a._f2());if(a._f1)a._p1.on(a._j1,function(b){a._g2(b)});else a.dragSuccess=!1;var m=["rsPlayBtnIcon","rsPlayBtn","rsCloseVideoBtn","rsCloseVideoIcn"];a._p1.click(function(b){if(!a.dragSuccess){var c=l(b.target).attr("class");if(-1!==l.inArray(c,m)&&a.toggleVideo())return!1;if(a.st.navigateByClick&&!a._h2){if(l(b.target).closest(".rsNoDrag",a._r1).length)return!0;a._i2(b)}a.ev.trigger("rsSlideClick")}}).on("click.rs","a",function(){if(a.dragSuccess)return!1;
a._h2=!0;setTimeout(function(){a._h2=!1},3)});a.ev.trigger("rsAfterInit")}l.rsModules||(l.rsModules={uid:0});t.prototype={constructor:t,_i2:function(b){b=b[this._h?"pageX":"pageY"]-this._j2;b>=this._q?this.next():0>b&&this.prev()},_t:function(){var b;b=this.st.numImagesToPreload;if(this._z=this.st.loop)2===this.numSlides?(this._z=!1,this.st.loopRewind=!0):2>this.numSlides&&(this.st.loopRewind=this._z=!1);this._z&&0<b&&(4>=this.numSlides?b=1:this.st.numImagesToPreload>(this.numSlides-1)/2&&(b=Math.floor((this.numSlides-
1)/2)));this._y=b},_s:function(b,f){function c(a,b){b?e.images.push(a.attr(b)):e.images.push(a.text());if(j){j=!1;e.caption="src"===b?a.attr("alt"):a.contents();e.image=e.images[0];e.videoURL=a.attr("data-rsVideo");var c=a.attr("data-rsw"),d=a.attr("data-rsh");"undefined"!==typeof c&&!1!==c&&"undefined"!==typeof d&&!1!==d?(e.iW=parseInt(c,10),e.iH=parseInt(d,10)):g.st.imgWidth&&g.st.imgHeight&&(e.iW=g.st.imgWidth,e.iH=g.st.imgHeight)}}var g=this,a,e={},d,j=!0;b=l(b);g._k2=b;g.ev.trigger("rsBeforeParseNode",
[b,e]);if(!e.stopParsing)return b=g._k2,e.id=g._r,e.contentAdded=!1,g._r++,e.images=[],e.isBig=!1,e.hasCover||(b.hasClass("rsImg")?(d=b,a=!0):(d=b.find(".rsImg"),d.length&&(a=!0)),a?(e.bigImage=d.eq(0).attr("data-rsBigImg"),d.each(function(){var a=l(this);a.is("a")?c(a,"href"):a.is("img")?c(a,"src"):c(a)})):b.is("img")&&(b.addClass("rsImg rsMainSlideImage"),c(b,"src"))),d=b.find(".rsCaption"),d.length&&(e.caption=d.remove()),e.content=b,g.ev.trigger("rsAfterParseNode",[b,e]),f&&g.slides.push(e),0===
e.images.length&&(e.isLoaded=!0,e.isRendered=!1,e.isLoading=!1,e.images=null),e},_b2:function(){var b=this,f,c,g=function(a){37===a?b.prev():39===a&&b.next()};b._b.on("keydown"+b.ns,function(a){if(!b._l2&&(c=a.keyCode,(37===c||39===c)&&!f))g(c),f=setInterval(function(){g(c)},700)}).on("keyup"+b.ns,function(){f&&(clearInterval(f),f=null)})},goTo:function(b,f){b!==this.currSlideId&&this._m2(b,this.st.transitionSpeed,!0,!f)},destroy:function(b){this.ev.trigger("rsBeforeDestroy");this._b.off("keydown"+
this.ns+" keyup"+this.ns+" "+this._k1+" "+this._l1);this._p1.off(this._j1+" click");this.slider.data("royalSlider",null);l.removeData(this.slider,"royalSlider");l(window).off("resize"+this.ns);b&&this.slider.remove();this.ev=this.slider=this.slides=null},_n2:function(b,f){function c(c,e,f){c.isAdded?(g(e,c),a(e,c)):(f||(f=d.slidesJQ[e]),c.holder?f=c.holder:(f=d.slidesJQ[e]=l(f),c.holder=f),c.appendOnLoaded=!1,a(e,c,f),g(e,c),d._p2(c,f,b),c.isAdded=!0)}function g(a,c){c.contentAdded||(d.setItemHtml(c,
b),b||(c.contentAdded=!0))}function a(a,b,c){d._l&&(c||(c=d.slidesJQ[a]),c.css(d._i,(a+d._d1+p)*d._w))}function e(a){if(k){if(a>n-1)return e(a-n);if(0>a)return e(n+a)}return a}var d=this,j,h,k=d._z,n=d.numSlides;if(!isNaN(f))return e(f);var m=d.currSlideId,p,q=b?Math.abs(d._o2-d.currSlideId)>=d.numSlides-1?0:1:d._y,r=Math.min(2,q),u=!1,t=!1,s;for(h=m;h<m+1+r;h++)if(s=e(h),(j=d.slides[s])&&(!j.isAdded||!j.positionSet)){u=!0;break}for(h=m-1;h>m-1-r;h--)if(s=e(h),(j=d.slides[s])&&(!j.isAdded||!j.positionSet)){t=
!0;break}if(u)for(h=m;h<m+q+1;h++)s=e(h),p=Math.floor((d._u-(m-h))/d.numSlides)*d.numSlides,(j=d.slides[s])&&c(j,s);if(t)for(h=m-1;h>m-1-q;h--)s=e(h),p=Math.floor((d._u-(m-h))/n)*n,(j=d.slides[s])&&c(j,s);if(!b){r=e(m-q);m=e(m+q);q=r>m?0:r;for(h=0;h<n;h++)if(!(r>m&&h>r-1)&&(h<q||h>m))if((j=d.slides[h])&&j.holder)j.holder.detach(),j.isAdded=!1}},setItemHtml:function(b,f){var c=this,g=function(){if(b.images){if(!b.isLoading){var e,f;b.content.hasClass("rsImg")?(e=b.content,f=!0):e=b.content.find(".rsImg:not(img)");
e&&!e.is("img")&&e.each(function(){var a=l(this),c='<img class="rsImg" src="'+(a.is("a")?a.attr("href"):a.text())+'" />';f?b.content=l(c):a.replaceWith(c)});e=f?b.content:b.content.find("img.rsImg");h();e.eq(0).addClass("rsMainSlideImage");b.iW&&b.iH&&(b.isLoaded||c._q2(b),d());b.isLoading=!0;if(b.isBig)l("<img />").on("load.rs error.rs",function(){l(this).off("load.rs error.rs");a([this],!0)}).attr("src",b.image);else{b.loaded=[];b.numStartedLoad=0;e=function(){l(this).off("load.rs error.rs");b.loaded.push(this);
b.loaded.length===b.numStartedLoad&&a(b.loaded,!1)};for(var g=0;g<b.images.length;g++){var j=l("<img />");b.numStartedLoad++;j.on("load.rs error.rs",e).attr("src",b.images[g])}}}}else b.isRendered=!0,b.isLoaded=!0,b.isLoading=!1,d(!0)},a=function(a,c){if(a.length){var d=a[0];if(c!==b.isBig)(d=b.holder.children())&&1<d.length&&k();else if(b.iW&&b.iH)e();else if(b.iW=d.width,b.iH=d.height,b.iW&&b.iH)e();else{var f=new Image;f.onload=function(){f.width?(b.iW=f.width,b.iH=f.height,e()):setTimeout(function(){f.width&&
(b.iW=f.width,b.iH=f.height);e()},1E3)};f.src=d.src}}else e()},e=function(){b.isLoaded=!0;b.isLoading=!1;d();k();j()},d=function(){if(!b.isAppended&&c.ev){var a=c.st.visibleNearby,d=b.id-c._o;if(!f&&!b.appendOnLoaded&&c.st.fadeinLoadedSlide&&(0===d||(a||c._r2||c._l2)&&(-1===d||1===d)))a={visibility:"visible",opacity:0},a[c._g+"transition"]="opacity 400ms ease-in-out",b.content.css(a),setTimeout(function(){b.content.css("opacity",1)},16);b.holder.find(".rsPreloader").length?b.holder.append(b.content):
b.holder.html(b.content);b.isAppended=!0;b.isLoaded&&(c._q2(b),j());b.sizeReady||(b.sizeReady=!0,setTimeout(function(){c.ev.trigger("rsMaybeSizeReady",b)},100))}},j=function(){!b.loadedTriggered&&c.ev&&(b.isLoaded=b.loadedTriggered=!0,b.holder.trigger("rsAfterContentSet"),c.ev.trigger("rsAfterContentSet",b))},h=function(){c.st.usePreloader&&b.holder.html(c._q1.clone())},k=function(){if(c.st.usePreloader){var a=b.holder.find(".rsPreloader");a.length&&a.remove()}};b.isLoaded?d():f?!c._l&&b.images&&
b.iW&&b.iH?g():(b.holder.isWaiting=!0,h(),b.holder.slideId=-99):g()},_p2:function(b){this._p1.append(b.holder);b.appendOnLoaded=!1},_g2:function(b,f){var c=this,g,a="touchstart"===b.type;c._s2=a;c.ev.trigger("rsDragStart");if(l(b.target).closest(".rsNoDrag",c._r1).length)return c.dragSuccess=!1,!0;!f&&c._r2&&(c._t2=!0,c._u2());c.dragSuccess=!1;if(c._l2)a&&(c._v2=!0);else{a&&(c._v2=!1);c._w2();if(a){var e=b.originalEvent.touches;if(e&&0<e.length)g=e[0],1<e.length&&(c._v2=!0);else return}else b.preventDefault(),
g=b,c.msEnabled&&(g=g.originalEvent);c._l2=!0;c._b.on(c._k1,function(a){c._x2(a,f)}).on(c._l1,function(a){c._y2(a,f)});c._z2="";c._a3=!1;c._b3=g.pageX;c._c3=g.pageY;c._d3=c._v=(!f?c._h:c._e3)?g.pageX:g.pageY;c._f3=0;c._g3=0;c._h3=!f?c._p:c._i3;c._j3=(new Date).getTime();if(a)c._e1.on(c._m1,function(a){c._y2(a,f)})}},_k3:function(b,f){if(this._l3){var c=this._m3,g=b.pageX-this._b3,a=b.pageY-this._c3,e=this._h3+g,d=this._h3+a,j=!f?this._h:this._e3,e=j?e:d,d=this._z2;this._a3=!0;this._b3=b.pageX;this._c3=
b.pageY;"x"===d&&0!==g?this._f3=0<g?1:-1:"y"===d&&0!==a&&(this._g3=0<a?1:-1);d=j?this._b3:this._c3;g=j?g:a;f?e>this._n3?e=this._h3+g*this._n1:e<this._o3&&(e=this._h3+g*this._n1):this._z||(0>=this.currSlideId&&0<d-this._d3&&(e=this._h3+g*this._n1),this.currSlideId>=this.numSlides-1&&0>d-this._d3&&(e=this._h3+g*this._n1));this._h3=e;200<c-this._j3&&(this._j3=c,this._v=d);f?this._q3(this._h3):this._l&&this._p3(this._h3)}},_x2:function(b,f){var c=this,g,a="touchmove"===b.type;if(!c._s2||a){if(a){if(c._r3)return;
var e=b.originalEvent.touches;if(e){if(1<e.length)return;g=e[0]}else return}else g=b,c.msEnabled&&(g=g.originalEvent);c._a3||(c._e&&(!f?c._p1:c._s3).css(c._g+c._u1,"0s"),function j(){c._l2&&(c._t3=requestAnimationFrame(j),c._u3&&c._k3(c._u3,f))}());if(c._l3)b.preventDefault(),c._m3=(new Date).getTime(),c._u3=g;else if(e=!f?c._h:c._e3,g=Math.abs(g.pageX-c._b3)-Math.abs(g.pageY-c._c3)-(e?-7:7),7<g){if(e)b.preventDefault(),c._z2="x";else if(a){c._v3();return}c._l3=!0}else if(-7>g){if(e){if(a){c._v3();
return}}else b.preventDefault(),c._z2="y";c._l3=!0}}},_v3:function(){this._r3=!0;this._a3=this._l2=!1;this._y2()},_y2:function(b,f){function c(a){return 100>a?100:500<a?500:a}function g(b,d){if(a._l||f)j=(-a._u-a._d1)*a._w,h=Math.abs(a._p-j),a._c=h/d,b&&(a._c+=250),a._c=c(a._c),a._x3(j,!1)}var a=this,e,d,j,h;d="touchend"===b.type||"touchcancel"===b.type;if(!a._s2||d)if(a._s2=!1,a.ev.trigger("rsDragRelease"),a._u3=null,a._l2=!1,a._r3=!1,a._l3=!1,a._m3=0,cancelAnimationFrame(a._t3),a._a3&&(f?a._q3(a._h3):
a._l&&a._p3(a._h3)),a._b.off(a._k1).off(a._l1),d&&a._e1.off(a._m1),a._i1(),!a._a3&&!a._v2&&f&&a._w3){var k=l(b.target).closest(".rsNavItem");k.length&&a.goTo(k.index())}else{e=!f?a._h:a._e3;if(!a._a3||"y"===a._z2&&e||"x"===a._z2&&!e)if(!f&&a._t2){a._t2=!1;if(a.st.navigateByClick){a._i2(a.msEnabled?b.originalEvent:b);a.dragSuccess=!0;return}a.dragSuccess=!0}else{a._t2=!1;a.dragSuccess=!1;return}else a.dragSuccess=!0;a._t2=!1;a._z2="";var n=a.st.minSlideOffset;d=d?b.originalEvent.changedTouches[0]:
a.msEnabled?b.originalEvent:b;var m=e?d.pageX:d.pageY,p=a._d3;d=a._v;var q=a.currSlideId,r=a.numSlides,u=e?a._f3:a._g3,t=a._z;Math.abs(m-p);e=m-d;d=(new Date).getTime()-a._j3;d=Math.abs(e)/d;if(0===u||1>=r)g(!0,d);else{if(!t&&!f)if(0>=q){if(0<u){g(!0,d);return}}else if(q>=r-1&&0>u){g(!0,d);return}if(f){j=a._i3;if(j>a._n3)j=a._n3;else if(j<a._o3)j=a._o3;else{n=d*d/0.006;k=-a._i3;m=a._y3-a._z3+a._i3;0<e&&n>k?(k+=a._z3/(15/(0.003*(n/d))),d=d*k/n,n=k):0>e&&n>m&&(m+=a._z3/(15/(0.003*(n/d))),d=d*m/n,n=
m);k=Math.max(Math.round(d/0.003),50);j+=n*(0>e?-1:1);if(j>a._n3){a._a4(j,k,!0,a._n3,200);return}if(j<a._o3){a._a4(j,k,!0,a._o3,200);return}}a._a4(j,k,!0)}else p+n<m?0>u?g(!1,d):a._m2("prev",c(Math.abs(a._p-(-a._u-a._d1+1)*a._w)/d),!1,!0,!0):p-n>m?0<u?g(!1,d):a._m2("next",c(Math.abs(a._p-(-a._u-a._d1-1)*a._w)/d),!1,!0,!0):g(!1,d)}}},_p3:function(b){b=this._p=b;this._e?this._p1.css(this._x1,this._y1+(this._h?b+this._z1+0:0+this._z1+b)+this._a2):this._p1.css(this._h?this._x1:this._w1,b)},updateSliderSize:function(b){var f,
c;if(this.st.autoScaleSlider){var g=this.st.autoScaleSliderWidth,a=this.st.autoScaleSliderHeight;this.st.autoScaleHeight?(f=this.slider.width(),f!=this.width&&(this.slider.css("height",f*(a/g)),f=this.slider.width()),c=this.slider.height()):(c=this.slider.height(),c!=this.height&&(this.slider.css("width",c*(g/a)),c=this.slider.height()),f=this.slider.width())}else f=this.slider.width(),c=this.slider.height();if(b||f!=this.width||c!=this.height){this.width=f;this.height=c;this._b4=f;this._c4=c;this.ev.trigger("rsBeforeSizeSet");
this.ev.trigger("rsAfterSizePropSet");this._e1.css({width:this._b4,height:this._c4});this._w=(this._h?this._b4:this._c4)+this.st.slidesSpacing;this._d4=this.st.imageScalePadding;for(f=0;f<this.slides.length;f++)b=this.slides[f],b.positionSet=!1,b&&(b.images&&b.isLoaded)&&(b.isRendered=!1,this._q2(b));if(this._e4)for(f=0;f<this._e4.length;f++)b=this._e4[f],b.holder.css(this._i,(b.id+this._d1)*this._w);this._n2();this._l&&(this._e&&this._p1.css(this._g+"transition-duration","0s"),this._p3((-this._u-
this._d1)*this._w));this.ev.trigger("rsOnUpdateNav")}this._j2=this._e1.offset();this._j2=this._j2[this._i]},appendSlide:function(b,f){var c=this._s(b);if(isNaN(f)||f>this.numSlides)f=this.numSlides;this.slides.splice(f,0,c);this.slidesJQ.splice(f,0,'<div style="'+(this._l?"position:absolute;":this._n)+'" class="rsSlide"></div>');f<this.currSlideId&&this.currSlideId++;this.ev.trigger("rsOnAppendSlide",[c,f]);this._f4(f);f===this.currSlideId&&this.ev.trigger("rsAfterSlideChange")},removeSlide:function(b){var f=
this.slides[b];f&&(f.holder&&f.holder.remove(),b<this.currSlideId&&this.currSlideId--,this.slides.splice(b,1),this.slidesJQ.splice(b,1),this.ev.trigger("rsOnRemoveSlide",[b]),this._f4(b),b===this.currSlideId&&this.ev.trigger("rsAfterSlideChange"))},_f4:function(){var b=this,f=b.numSlides,f=0>=b._u?0:Math.floor(b._u/f);b.numSlides=b.slides.length;0===b.numSlides?(b.currSlideId=b._d1=b._u=0,b.currSlide=b._g4=null):b._u=f*b.numSlides+b.currSlideId;for(f=0;f<b.numSlides;f++)b.slides[f].id=f;b.currSlide=
b.slides[b.currSlideId];b._r1=b.slidesJQ[b.currSlideId];b.currSlideId>=b.numSlides?b.goTo(b.numSlides-1):0>b.currSlideId&&b.goTo(0);b._t();b._l&&b._z&&b._p1.css(b._g+b._u1,"0ms");b._h4&&clearTimeout(b._h4);b._h4=setTimeout(function(){b._l&&b._p3((-b._u-b._d1)*b._w);b._n2();b._l||b._r1.css({display:"block",opacity:1})},14);b.ev.trigger("rsOnUpdateNav")},_i1:function(){this._f1&&this._l&&(this._g1?this._e1.css("cursor",this._g1):(this._e1.removeClass("grabbing-cursor"),this._e1.addClass("grab-cursor")))},
_w2:function(){this._f1&&this._l&&(this._h1?this._e1.css("cursor",this._h1):(this._e1.removeClass("grab-cursor"),this._e1.addClass("grabbing-cursor")))},next:function(b){this._m2("next",this.st.transitionSpeed,!0,!b)},prev:function(b){this._m2("prev",this.st.transitionSpeed,!0,!b)},_m2:function(b,f,c,g,a){var e=this,d,j,h;e.ev.trigger("rsBeforeMove",[b,g]);h="next"===b?e.currSlideId+1:"prev"===b?e.currSlideId-1:b=parseInt(b,10);if(!e._z){if(0>h){e._i4("left",!g);return}if(h>=e.numSlides){e._i4("right",
!g);return}}e._r2&&(e._u2(!0),c=!1);j=h-e.currSlideId;h=e._o2=e.currSlideId;var k=e.currSlideId+j;g=e._u;var l;e._z?(k=e._n2(!1,k),g+=j):g=k;e._o=k;e._g4=e.slidesJQ[e.currSlideId];e._u=g;e.currSlideId=e._o;e.currSlide=e.slides[e.currSlideId];e._r1=e.slidesJQ[e.currSlideId];var k=e.st.slidesDiff,m=Boolean(0<j);j=Math.abs(j);var p=Math.floor(h/e._y),q=Math.floor((h+(m?k:-k))/e._y),p=(m?Math.max(p,q):Math.min(p,q))*e._y+(m?e._y-1:0);p>e.numSlides-1?p=e.numSlides-1:0>p&&(p=0);h=m?p-h:h-p;h>e._y&&(h=e._y);
if(j>h+k){e._d1+=(j-(h+k))*(m?-1:1);f*=1.4;for(h=0;h<e.numSlides;h++)e.slides[h].positionSet=!1}e._c=f;e._n2(!0);a||(l=!0);d=(-g-e._d1)*e._w;l?setTimeout(function(){e._j4=!1;e._x3(d,b,!1,c);e.ev.trigger("rsOnUpdateNav")},0):(e._x3(d,b,!1,c),e.ev.trigger("rsOnUpdateNav"))},_f2:function(){this.st.arrowsNav&&(1>=this.numSlides?(this._c2.css("display","none"),this._d2.css("display","none")):(this._c2.css("display","block"),this._d2.css("display","block"),!this._z&&!this.st.loopRewind&&(0===this.currSlideId?
this._c2.addClass("rsArrowDisabled"):this._c2.removeClass("rsArrowDisabled"),this.currSlideId===this.numSlides-1?this._d2.addClass("rsArrowDisabled"):this._d2.removeClass("rsArrowDisabled"))))},_x3:function(b,f,c,g,a){function e(){var a;if(j&&(a=j.data("rsTimeout")))j!==h&&j.css({opacity:0,display:"none",zIndex:0}),clearTimeout(a),j.data("rsTimeout","");if(a=h.data("rsTimeout"))clearTimeout(a),h.data("rsTimeout","")}var d=this,j,h,k={};isNaN(d._c)&&(d._c=400);d._p=d._h3=b;d.ev.trigger("rsBeforeAnimStart");
d._e?d._l?(d._c=parseInt(d._c,10),c=d._g+d._v1,k[d._g+d._u1]=d._c+"ms",k[c]=g?l.rsCSS3Easing[d.st.easeInOut]:l.rsCSS3Easing[d.st.easeOut],d._p1.css(k),g||!d.hasTouch?setTimeout(function(){d._p3(b)},5):d._p3(b)):(d._c=d.st.transitionSpeed,j=d._g4,h=d._r1,h.data("rsTimeout")&&h.css("opacity",0),e(),j&&j.data("rsTimeout",setTimeout(function(){k[d._g+d._u1]="0ms";k.zIndex=0;k.display="none";j.data("rsTimeout","");j.css(k);setTimeout(function(){j.css("opacity",0)},16)},d._c+60)),k.display="block",k.zIndex=
d._m,k.opacity=0,k[d._g+d._u1]="0ms",k[d._g+d._v1]=l.rsCSS3Easing[d.st.easeInOut],h.css(k),h.data("rsTimeout",setTimeout(function(){h.css(d._g+d._u1,d._c+"ms");h.data("rsTimeout",setTimeout(function(){h.css("opacity",1);h.data("rsTimeout","")},20))},20))):d._l?(k[d._h?d._x1:d._w1]=b+"px",d._p1.animate(k,d._c,g?d.st.easeInOut:d.st.easeOut)):(j=d._g4,h=d._r1,h.stop(!0,!0).css({opacity:0,display:"block",zIndex:d._m}),d._c=d.st.transitionSpeed,h.animate({opacity:1},d._c,d.st.easeInOut),e(),j&&j.data("rsTimeout",
setTimeout(function(){j.stop(!0,!0).css({opacity:0,display:"none",zIndex:0})},d._c+60)));d._r2=!0;d.loadingTimeout&&clearTimeout(d.loadingTimeout);d.loadingTimeout=a?setTimeout(function(){d.loadingTimeout=null;a.call()},d._c+60):setTimeout(function(){d.loadingTimeout=null;d._k4(f)},d._c+60)},_u2:function(b){this._r2=!1;clearTimeout(this.loadingTimeout);if(this._l)if(this._e){if(!b){b=this._p;var f=this._h3=this._l4();this._p1.css(this._g+this._u1,"0ms");b!==f&&this._p3(f)}}else this._p1.stop(!0),
this._p=parseInt(this._p1.css(this._x1),10);else 20<this._m?this._m=10:this._m++},_l4:function(){var b=window.getComputedStyle(this._p1.get(0),null).getPropertyValue(this._g+"transform").replace(/^matrix\(/i,"").split(/, |\)$/g),f=0===b[0].indexOf("matrix3d");return parseInt(b[this._h?f?12:4:f?13:5],10)},_m4:function(b,f){return this._e?this._y1+(f?b+this._z1+0:0+this._z1+b)+this._a2:b},_k4:function(){this._l||(this._r1.css("z-index",0),this._m=10);this._r2=!1;this.staticSlideId=this.currSlideId;
this._n2();this._n4=!1;this.ev.trigger("rsAfterSlideChange")},_i4:function(b,f){var c=this,g=(-c._u-c._d1)*c._w;if(!(0===c.numSlides||c._r2))if(c.st.loopRewind)c.goTo("left"===b?c.numSlides-1:0,f);else if(c._l){c._c=200;var a=function(){c._r2=!1};c._x3(g+("left"===b?30:-30),"",!1,!0,function(){c._r2=!1;c._x3(g,"",!1,!0,a)})}},_q2:function(b){if(!b.isRendered){var f=b.content,c="rsMainSlideImage",g,a=this.st.imageAlignCenter,e=this.st.imageScaleMode,d;b.videoURL&&(c="rsVideoContainer","fill"!==e?g=
!0:(d=f,d.hasClass(c)||(d=d.find("."+c)),d.css({width:"100%",height:"100%"}),c="rsMainSlideImage"));f.hasClass(c)||(f=f.find("."+c));if(f){var j=b.iW,c=b.iH;b.isRendered=!0;if("none"!==e||a){b="fill"!==e?this._d4:0;d=this._b4-2*b;var h=this._c4-2*b,k,l,m={};if("fit-if-smaller"===e&&(j>d||c>h))e="fit";if("fill"===e||"fit"===e)k=d/j,l=h/c,k="fill"==e?k>l?k:l:"fit"==e?k<l?k:l:1,j=Math.ceil(j*k,10),c=Math.ceil(c*k,10);"none"!==e&&(m.width=j,m.height=c,g&&f.find(".rsImg").css({width:"100%",height:"100%"}));
a&&(m.marginLeft=Math.floor((d-j)/2)+b,m.marginTop=Math.floor((h-c)/2)+b);f.css(m)}}}}};l.rsProto=t.prototype;l.fn.royalSlider=function(b){var f=arguments;return this.each(function(){var c=l(this);if("object"===typeof b||!b)c.data("royalSlider")||c.data("royalSlider",new t(c,b));else if((c=c.data("royalSlider"))&&c[b])return c[b].apply(c,Array.prototype.slice.call(f,1))})};l.fn.royalSlider.defaults={slidesSpacing:8,startSlideId:0,loop:!1,loopRewind:!1,numImagesToPreload:4,fadeinLoadedSlide:!0,slidesOrientation:"horizontal",
transitionType:"move",transitionSpeed:600,controlNavigation:"bullets",controlsInside:!0,arrowsNav:!0,arrowsNavAutoHide:!0,navigateByClick:!0,randomizeSlides:!1,sliderDrag:!0,sliderTouch:!0,keyboardNavEnabled:!1,fadeInAfterLoaded:!0,allowCSS3:!0,allowCSS3OnWebkit:!0,addActiveClass:!1,autoHeight:!1,easeOut:"easeOutSine",easeInOut:"easeInOutSine",minSlideOffset:10,imageScaleMode:"fit-if-smaller",imageAlignCenter:!0,imageScalePadding:4,usePreloader:!0,autoScaleSlider:!1,autoScaleSliderWidth:800,autoScaleSliderHeight:400,
autoScaleHeight:!0,arrowsNavHideOnTouch:!1,globalCaption:!1,slidesDiff:2};l.rsCSS3Easing={easeOutSine:"cubic-bezier(0.390, 0.575, 0.565, 1.000)",easeInOutSine:"cubic-bezier(0.445, 0.050, 0.550, 0.950)"};l.extend(jQuery.easing,{easeInOutSine:function(b,f,c,g,a){return-g/2*(Math.cos(Math.PI*f/a)-1)+c},easeOutSine:function(b,f,c,g,a){return g*Math.sin(f/a*(Math.PI/2))+c},easeOutCubic:function(b,f,c,g,a){return g*((f=f/a-1)*f*f+1)+c}})})(jQuery,window);
// jquery.rs.active-class v1.0.1
(function(c){c.rsProto._o4=function(){var b,a=this;if(a.st.addActiveClass)a.ev.on("rsOnUpdateNav",function(){b&&clearTimeout(b);b=setTimeout(function(){a._g4&&a._g4.removeClass("rsActiveSlide");a._r1&&a._r1.addClass("rsActiveSlide");b=null},50)})};c.rsModules.activeClass=c.rsProto._o4})(jQuery);
// jquery.rs.animated-blocks v1.0.7
(function(j){j.extend(j.rsProto,{_p4:function(){function l(){var g=a.currSlide;if(a.currSlide&&a.currSlide.isLoaded&&a._t4!==g){if(0<a._s4.length){for(b=0;b<a._s4.length;b++)clearTimeout(a._s4[b]);a._s4=[]}if(0<a._r4.length){var f;for(b=0;b<a._r4.length;b++)if(f=a._r4[b])a._e?(f.block.css(a._g+a._u1,"0s"),f.block.css(f.css)):f.block.stop(!0).css(f.css),a._t4=null,g.animBlocksDisplayed=!1;a._r4=[]}g.animBlocks&&(g.animBlocksDisplayed=!0,a._t4=g,a._u4(g.animBlocks))}}var a=this,b;a._q4={fadeEffect:!0,
moveEffect:"top",moveOffset:20,speed:400,easing:"easeOutSine",delay:200};a.st.block=j.extend({},a._q4,a.st.block);a._r4=[];a._s4=[];a.ev.on("rsAfterInit",function(){l()});a.ev.on("rsBeforeParseNode",function(a,b,d){b=j(b);d.animBlocks=b.find(".rsABlock").css("display","none");d.animBlocks.length||(d.animBlocks=b.hasClass("rsABlock")?b.css("display","none"):!1)});a.ev.on("rsAfterContentSet",function(b,f){f.id===a.slides[a.currSlideId].id&&setTimeout(function(){l()},a.st.fadeinLoadedSlide?300:0)});
a.ev.on("rsAfterSlideChange",function(){l()})},_v4:function(j,a){setTimeout(function(){j.css(a)},6)},_u4:function(l){var a=this,b,g,f,d,h,e,m;a._s4=[];l.each(function(l){b=j(this);g={};f={};d=null;var c=b.attr("data-move-offset"),c=c?parseInt(c,10):a.st.block.moveOffset;if(0<c&&((e=b.data("move-effect"))?(e=e.toLowerCase(),"none"===e?e=!1:"left"!==e&&("top"!==e&&"bottom"!==e&&"right"!==e)&&(e=a.st.block.moveEffect,"none"===e&&(e=!1))):e=a.st.block.moveEffect,e&&"none"!==e)){var n;n="right"===e||"left"===
e?!0:!1;var k;m=!1;a._e?(k=0,h=a._x1):(n?isNaN(parseInt(b.css("right"),10))?h="left":(h="right",m=!0):isNaN(parseInt(b.css("bottom"),10))?h="top":(h="bottom",m=!0),h="margin-"+h,m&&(c=-c),a._e?k=parseInt(b.css(h),10):(k=b.data("rs-start-move-prop"),void 0===k&&(k=parseInt(b.css(h),10),b.data("rs-start-move-prop",k))));f[h]=a._m4("top"===e||"left"===e?k-c:k+c,n);g[h]=a._m4(k,n)}if(c=b.attr("data-fade-effect")){if("none"===c.toLowerCase()||"false"===c.toLowerCase())c=!1}else c=a.st.block.fadeEffect;
c&&(f.opacity=0,g.opacity=1);if(c||e)d={},d.hasFade=Boolean(c),Boolean(e)&&(d.moveProp=h,d.hasMove=!0),d.speed=b.data("speed"),isNaN(d.speed)&&(d.speed=a.st.block.speed),d.easing=b.data("easing"),d.easing||(d.easing=a.st.block.easing),d.css3Easing=j.rsCSS3Easing[d.easing],d.delay=b.data("delay"),isNaN(d.delay)&&(d.delay=a.st.block.delay*l);c={};a._e&&(c[a._g+a._u1]="0ms");c.moveProp=g.moveProp;c.opacity=g.opacity;c.display="none";a._r4.push({block:b,css:c});a._v4(b,f);a._s4.push(setTimeout(function(b,
d,c,e){return function(){b.css("display","block");if(c){var g={};if(a._e){var f="";c.hasMove&&(f+=c.moveProp);c.hasFade&&(c.hasMove&&(f+=", "),f+="opacity");g[a._g+a._t1]=f;g[a._g+a._u1]=c.speed+"ms";g[a._g+a._v1]=c.css3Easing;b.css(g);setTimeout(function(){b.css(d)},24)}else setTimeout(function(){b.animate(d,c.speed,c.easing)},16)}delete a._s4[e]}}(b,g,d,l),6>=d.delay?12:d.delay))})}});j.rsModules.animatedBlocks=j.rsProto._p4})(jQuery);
// jquery.rs.auto-height v1.0.2
(function(b){b.extend(b.rsProto,{_w4:function(){var a=this;if(a.st.autoHeight){var b,d,e,c=function(c){e=a.slides[a.currSlideId];if(b=e.holder)if((d=b.height())&&void 0!==d)a._c4=d,a._e||!c?a._e1.css("height",d):a._e1.stop(!0,!0).animate({height:d},a.st.transitionSpeed)};a.ev.on("rsMaybeSizeReady.rsAutoHeight",function(a,b){e===b&&c()});a.ev.on("rsAfterContentSet.rsAutoHeight",function(a,b){e===b&&c()});a.slider.addClass("rsAutoHeight");a.ev.one("rsAfterInit",function(){setTimeout(function(){c(!1);
setTimeout(function(){a.slider.append('<div style="clear:both; float: none;"></div>');a._e&&a._e1.css(a._g+"transition","height "+a.st.transitionSpeed+"ms ease-in-out")},16)},16)});a.ev.on("rsBeforeAnimStart",function(){c(!0)});a.ev.on("rsBeforeSizeSet",function(){setTimeout(function(){c(!1)},16)})}}});b.rsModules.autoHeight=b.rsProto._w4})(jQuery);
// jquery.rs.autoplay v1.0.5
(function(b){b.extend(b.rsProto,{_x4:function(){var a=this,d;a._y4={enabled:!1,stopAtAction:!0,pauseOnHover:!0,delay:2E3};!a.st.autoPlay&&a.st.autoplay&&(a.st.autoPlay=a.st.autoplay);a.st.autoPlay=b.extend({},a._y4,a.st.autoPlay);a.st.autoPlay.enabled&&(a.ev.on("rsBeforeParseNode",function(a,c,f){c=b(c);if(d=c.attr("data-rsDelay"))f.customDelay=parseInt(d,10)}),a.ev.one("rsAfterInit",function(){a._z4()}),a.ev.on("rsBeforeDestroy",function(){a.stopAutoPlay();a.slider.off("mouseenter mouseleave");b(window).off("blur"+
a.ns+" focus"+a.ns)}))},_z4:function(){var a=this;a.startAutoPlay();a.ev.on("rsAfterContentSet",function(b,e){!a._l2&&(!a._r2&&a._a5&&e===a.currSlide)&&a._b5()});a.ev.on("rsDragRelease",function(){a._a5&&a._c5&&(a._c5=!1,a._b5())});a.ev.on("rsAfterSlideChange",function(){a._a5&&a._c5&&(a._c5=!1,a.currSlide.isLoaded&&a._b5())});a.ev.on("rsDragStart",function(){a._a5&&(a.st.autoPlay.stopAtAction?a.stopAutoPlay():(a._c5=!0,a._d5()))});a.ev.on("rsBeforeMove",function(b,e,c){a._a5&&(c&&a.st.autoPlay.stopAtAction?
a.stopAutoPlay():(a._c5=!0,a._d5()))});a._e5=!1;a.ev.on("rsVideoStop",function(){a._a5&&(a._e5=!1,a._b5())});a.ev.on("rsVideoPlay",function(){a._a5&&(a._c5=!1,a._d5(),a._e5=!0)});b(window).on("blur"+a.ns,function(){a._a5&&(a._c5=!0,a._d5())}).on("focus"+a.ns,function(){a._a5&&a._c5&&(a._c5=!1,a._b5())});a.st.autoPlay.pauseOnHover&&(a._f5=!1,a.slider.hover(function(){a._a5&&(a._c5=!1,a._d5(),a._f5=!0)},function(){a._a5&&(a._f5=!1,a._b5())}))},toggleAutoPlay:function(){this._a5?this.stopAutoPlay():
this.startAutoPlay()},startAutoPlay:function(){this._a5=!0;this.currSlide.isLoaded&&this._b5()},stopAutoPlay:function(){this._e5=this._f5=this._c5=this._a5=!1;this._d5()},_b5:function(){var a=this;!a._f5&&!a._e5&&(a._g5=!0,a._h5&&clearTimeout(a._h5),a._h5=setTimeout(function(){var b;!a._z&&!a.st.loopRewind&&(b=!0,a.st.loopRewind=!0);a.next(!0);b&&(a.st.loopRewind=!1)},!a.currSlide.customDelay?a.st.autoPlay.delay:a.currSlide.customDelay))},_d5:function(){!this._f5&&!this._e5&&(this._g5=!1,this._h5&&
(clearTimeout(this._h5),this._h5=null))}});b.rsModules.autoplay=b.rsProto._x4})(jQuery);
// jquery.rs.bullets v1.0.1
(function(c){c.extend(c.rsProto,{_i5:function(){var a=this;"bullets"===a.st.controlNavigation&&(a.ev.one("rsAfterPropsSetup",function(){a._j5=!0;a.slider.addClass("rsWithBullets");for(var b='<div class="rsNav rsBullets">',e=0;e<a.numSlides;e++)b+='<div class="rsNavItem rsBullet"><span></span></div>';a._k5=b=c(b+"</div>");a._l5=b.appendTo(a.slider).children();a._k5.on("click.rs",".rsNavItem",function(){a._m5||a.goTo(c(this).index())})}),a.ev.on("rsOnAppendSlide",function(b,c,d){d>=a.numSlides?a._k5.append('<div class="rsNavItem rsBullet"><span></span></div>'):
a._l5.eq(d).before('<div class="rsNavItem rsBullet"><span></span></div>');a._l5=a._k5.children()}),a.ev.on("rsOnRemoveSlide",function(b,c){var d=a._l5.eq(c);d&&d.length&&(d.remove(),a._l5=a._k5.children())}),a.ev.on("rsOnUpdateNav",function(){var b=a.currSlideId;a._n5&&a._n5.removeClass("rsNavSelected");b=a._l5.eq(b);b.addClass("rsNavSelected");a._n5=b}))}});c.rsModules.bullets=c.rsProto._i5})(jQuery);
// jquery.rs.deeplinking v1.0.6 + jQuery hashchange plugin v1.3 Copyright (c) 2010 Ben Alman
(function(b){b.extend(b.rsProto,{_o5:function(){var a=this,g,c,e;a._p5={enabled:!1,change:!1,prefix:""};a.st.deeplinking=b.extend({},a._p5,a.st.deeplinking);if(a.st.deeplinking.enabled){var h=a.st.deeplinking.change,d="#"+a.st.deeplinking.prefix,f=function(){var a=window.location.hash;return a&&(a=parseInt(a.substring(d.length),10),0<=a)?a-1:-1},j=f();-1!==j&&(a.st.startSlideId=j);h&&(b(window).on("hashchange"+a.ns,function(){if(!g){var b=f();0>b||(b>a.numSlides-1&&(b=a.numSlides-1),a.goTo(b))}}),
a.ev.on("rsBeforeAnimStart",function(){c&&clearTimeout(c);e&&clearTimeout(e)}),a.ev.on("rsAfterSlideChange",function(){c&&clearTimeout(c);e&&clearTimeout(e);e=setTimeout(function(){g=!0;window.location.replace((""+window.location).split("#")[0]+d+(a.currSlideId+1));c=setTimeout(function(){g=!1;c=null},60)},400)}));a.ev.on("rsBeforeDestroy",function(){c=e=null;h&&b(window).off("hashchange"+a.ns)})}}});b.rsModules.deeplinking=b.rsProto._o5})(jQuery);
(function(b,a,g){function c(a){a=a||location.href;return"#"+a.replace(/^[^#]*#?(.*)$/,"$1")}"$:nomunge";var e=document,h,d=b.event.special,f=e.documentMode,j="onhashchange"in a&&(f===g||7<f);b.fn.hashchange=function(a){return a?this.bind("hashchange",a):this.trigger("hashchange")};b.fn.hashchange.delay=50;d.hashchange=b.extend(d.hashchange,{setup:function(){if(j)return!1;b(h.start)},teardown:function(){if(j)return!1;b(h.stop)}});var p=function(){var e=c(),d=r(n);e!==n?(q(n=e,d),b(a).trigger("hashchange")):
d!==n&&(location.href=location.href.replace(/#.*/,"")+d);l=setTimeout(p,b.fn.hashchange.delay)},d={},l,n=c(),q=f=function(a){return a},r=f;d.start=function(){l||p()};d.stop=function(){l&&clearTimeout(l);l=g};if(a.attachEvent&&!a.addEventListener&&!j){var k,m;d.start=function(){k||(m=(m=b.fn.hashchange.src)&&m+c(),k=b('<iframe tabindex="-1" title="empty"/>').hide().one("load",function(){m||q(c());p()}).attr("src",m||"javascript:0").insertAfter("body")[0].contentWindow,e.onpropertychange=function(){try{"title"===
event.propertyName&&(k.document.title=e.title)}catch(a){}})};d.stop=f;r=function(){return c(k.location.href)};q=function(a,d){var c=k.document,f=b.fn.hashchange.domain;a!==d&&(c.title=e.title,c.open(),f&&c.write('<script>document.domain="'+f+'"\x3c/script>'),c.close(),k.location.hash=a)}}h=d})(jQuery,this);
// jquery.rs.fullscreen v1.0.5
(function(c){c.extend(c.rsProto,{_q5:function(){var a=this;a._r5={enabled:!1,keyboardNav:!0,buttonFS:!0,nativeFS:!1,doubleTap:!0};a.st.fullscreen=c.extend({},a._r5,a.st.fullscreen);if(a.st.fullscreen.enabled)a.ev.one("rsBeforeSizeSet",function(){a._s5()})},_s5:function(){var a=this;a._t5=!a.st.keyboardNavEnabled&&a.st.fullscreen.keyboardNav;if(a.st.fullscreen.nativeFS){a._u5={supportsFullScreen:!1,isFullScreen:function(){return!1},requestFullScreen:function(){},cancelFullScreen:function(){},fullScreenEventName:"",
prefix:""};var b=["webkit","moz","o","ms","khtml"];if(!a.isAndroid)if("undefined"!=typeof document.cancelFullScreen)a._u5.supportsFullScreen=!0;else for(var d=0;d<b.length;d++)if(a._u5.prefix=b[d],"undefined"!=typeof document[a._u5.prefix+"CancelFullScreen"]){a._u5.supportsFullScreen=!0;break}a._u5.supportsFullScreen?(a.nativeFS=!0,a._u5.fullScreenEventName=a._u5.prefix+"fullscreenchange"+a.ns,a._u5.isFullScreen=function(){switch(this.prefix){case "":return document.fullScreen;case "webkit":return document.webkitIsFullScreen;
default:return document[this.prefix+"FullScreen"]}},a._u5.requestFullScreen=function(a){return""===this.prefix?a.requestFullScreen():a[this.prefix+"RequestFullScreen"]()},a._u5.cancelFullScreen=function(){return""===this.prefix?document.cancelFullScreen():document[this.prefix+"CancelFullScreen"]()}):a._u5=!1}a.st.fullscreen.buttonFS&&(a._v5=c('<div class="rsFullscreenBtn"><div class="rsFullscreenIcn"></div></div>').appendTo(a._o1).on("click.rs",function(){a.isFullscreen?a.exitFullscreen():a.enterFullscreen()}))},
enterFullscreen:function(a){var b=this;if(b._u5)if(a)b._u5.requestFullScreen(c("html")[0]);else{b._b.on(b._u5.fullScreenEventName,function(){b._u5.isFullScreen()?b.enterFullscreen(!0):b.exitFullscreen(!0)});b._u5.requestFullScreen(c("html")[0]);return}if(!b._w5){b._w5=!0;b._b.on("keyup"+b.ns+"fullscreen",function(a){27===a.keyCode&&b.exitFullscreen()});b._t5&&b._b2();a=c(window);b._x5=a.scrollTop();b._y5=a.scrollLeft();b._z5=c("html").attr("style");b._a6=c("body").attr("style");b._b6=b.slider.attr("style");
c("body, html").css({overflow:"hidden",height:"100%",width:"100%",margin:"0",padding:"0"});b.slider.addClass("rsFullscreen");var d;for(d=0;d<b.numSlides;d++)a=b.slides[d],a.isRendered=!1,a.bigImage&&(a.isBig=!0,a.isMedLoaded=a.isLoaded,a.isMedLoading=a.isLoading,a.medImage=a.image,a.medIW=a.iW,a.medIH=a.iH,a.slideId=-99,a.bigImage!==a.medImage&&(a.sizeType="big"),a.isLoaded=a.isBigLoaded,a.isLoading=!1,a.image=a.bigImage,a.images[0]=a.bigImage,a.iW=a.bigIW,a.iH=a.bigIH,a.isAppended=a.contentAdded=
!1,b._c6(a));b.isFullscreen=!0;b._w5=!1;b.updateSliderSize();b.ev.trigger("rsEnterFullscreen")}},exitFullscreen:function(a){var b=this;if(b._u5){if(!a){b._u5.cancelFullScreen(c("html")[0]);return}b._b.off(b._u5.fullScreenEventName)}if(!b._w5){b._w5=!0;b._b.off("keyup"+b.ns+"fullscreen");b._t5&&b._b.off("keydown"+b.ns);c("html").attr("style",b._z5||"");c("body").attr("style",b._a6||"");var d;for(d=0;d<b.numSlides;d++)a=b.slides[d],a.isRendered=!1,a.bigImage&&(a.isBig=!1,a.slideId=-99,a.isBigLoaded=
a.isLoaded,a.isBigLoading=a.isLoading,a.bigImage=a.image,a.bigIW=a.iW,a.bigIH=a.iH,a.isLoaded=a.isMedLoaded,a.isLoading=!1,a.image=a.medImage,a.images[0]=a.medImage,a.iW=a.medIW,a.iH=a.medIH,a.isAppended=a.contentAdded=!1,b._c6(a,!0),a.bigImage!==a.medImage&&(a.sizeType="med"));b.isFullscreen=!1;a=c(window);a.scrollTop(b._x5);a.scrollLeft(b._y5);b._w5=!1;b.slider.removeClass("rsFullscreen");b.updateSliderSize();setTimeout(function(){b.updateSliderSize()},1);b.ev.trigger("rsExitFullscreen")}},_c6:function(a){var b=
!a.isLoaded&&!a.isLoading?'<a class="rsImg rsMainSlideImage" href="'+a.image+'"></a>':'<img class="rsImg rsMainSlideImage" src="'+a.image+'"/>';a.content.hasClass("rsImg")?a.content=c(b):a.content.find(".rsImg").eq(0).replaceWith(b);!a.isLoaded&&(!a.isLoading&&a.holder)&&a.holder.html(a.content)}});c.rsModules.fullscreen=c.rsProto._q5})(jQuery);
// jquery.rs.global-caption v1.0
(function(b){b.extend(b.rsProto,{_d6:function(){var a=this;a.st.globalCaption&&(a.ev.on("rsAfterInit",function(){a.globalCaption=b('<div class="rsGCaption"></div>').appendTo(!a.st.globalCaptionInside?a.slider:a._e1);a.globalCaption.html(a.currSlide.caption)}),a.ev.on("rsBeforeAnimStart",function(){a.globalCaption.html(a.currSlide.caption)}))}});b.rsModules.globalCaption=b.rsProto._d6})(jQuery);
// jquery.rs.nav-auto-hide v1.0
(function(b){b.extend(b.rsProto,{_e6:function(){var a=this;if(a.st.navAutoHide&&!a.hasTouch)a.ev.one("rsAfterInit",function(){if(a._k5){a._k5.addClass("rsHidden");var b=a.slider;b.one("mousemove.controlnav",function(){a._k5.removeClass("rsHidden")});b.hover(function(){a._k5.removeClass("rsHidden")},function(){a._k5.addClass("rsHidden")})}})}});b.rsModules.autoHideNav=b.rsProto._e6})(jQuery);
// jquery.rs.tabs v1.0.2
(function(e){e.extend(e.rsProto,{_f6:function(){var a=this;"tabs"===a.st.controlNavigation&&(a.ev.on("rsBeforeParseNode",function(a,d,b){d=e(d);b.thumbnail=d.find(".rsTmb").remove();b.thumbnail.length?b.thumbnail=e(document.createElement("div")).append(b.thumbnail).html():(b.thumbnail=d.attr("data-rsTmb"),b.thumbnail||(b.thumbnail=d.find(".rsImg").attr("data-rsTmb")),b.thumbnail=b.thumbnail?'<img src="'+b.thumbnail+'"/>':"")}),a.ev.one("rsAfterPropsSetup",function(){a._g6()}),a.ev.on("rsOnAppendSlide",
function(c,d,b){b>=a.numSlides?a._k5.append('<div class="rsNavItem rsTab">'+d.thumbnail+"</div>"):a._l5.eq(b).before('<div class="rsNavItem rsTab">'+item.thumbnail+"</div>");a._l5=a._k5.children()}),a.ev.on("rsOnRemoveSlide",function(c,d){var b=a._l5.eq(d);b&&(b.remove(),a._l5=a._k5.children())}),a.ev.on("rsOnUpdateNav",function(){var c=a.currSlideId;a._n5&&a._n5.removeClass("rsNavSelected");c=a._l5.eq(c);c.addClass("rsNavSelected");a._n5=c}))},_g6:function(){var a=this,c;a._j5=!0;c='<div class="rsNav rsTabs">';
for(var d=0;d<a.numSlides;d++)c+='<div class="rsNavItem rsTab">'+a.slides[d].thumbnail+"</div>";c=e(c+"</div>");a._k5=c;a._l5=c.children(".rsNavItem");a.slider.append(c);a._k5.click(function(b){b=e(b.target).closest(".rsNavItem");b.length&&a.goTo(b.index())})}});e.rsModules.tabs=e.rsProto._f6})(jQuery);
// jquery.rs.thumbnails v1.0.5
(function(f){f.extend(f.rsProto,{_h6:function(){var a=this;"thumbnails"===a.st.controlNavigation&&(a._i6={drag:!0,touch:!0,orientation:"horizontal",navigation:!0,arrows:!0,arrowLeft:null,arrowRight:null,spacing:4,arrowsAutoHide:!1,appendSpan:!1,transitionSpeed:600,autoCenter:!0,fitInViewport:!0,firstMargin:!0,paddingTop:0,paddingBottom:0},a.st.thumbs=f.extend({},a._i6,a.st.thumbs),a._j6=!0,!1===a.st.thumbs.firstMargin?a.st.thumbs.firstMargin=0:!0===a.st.thumbs.firstMargin&&(a.st.thumbs.firstMargin=
a.st.thumbs.spacing),a.ev.on("rsBeforeParseNode",function(a,c,b){c=f(c);b.thumbnail=c.find(".rsTmb").remove();b.thumbnail.length?b.thumbnail=f(document.createElement("div")).append(b.thumbnail).html():(b.thumbnail=c.attr("data-rsTmb"),b.thumbnail||(b.thumbnail=c.find(".rsImg").attr("data-rsTmb")),b.thumbnail=b.thumbnail?'<img src="'+b.thumbnail+'"/>':"")}),a.ev.one("rsAfterPropsSetup",function(){a._k6()}),a._n5=null,a.ev.on("rsOnUpdateNav",function(){var e=f(a._l5[a.currSlideId]);e!==a._n5&&(a._n5&&
(a._n5.removeClass("rsNavSelected"),a._n5=null),a._l6&&a._m6(a.currSlideId),a._n5=e.addClass("rsNavSelected"))}),a.ev.on("rsOnAppendSlide",function(e,c,b){e="<div"+a._n6+' class="rsNavItem rsThumb">'+a._o6+c.thumbnail+"</div>";b>=a.numSlides?a._s3.append(e):a._l5.eq(b).before(e);a._l5=a._s3.children();a.updateThumbsSize()}),a.ev.on("rsOnRemoveSlide",function(e,c){var b=a._l5.eq(c);b&&(b.remove(),a._l5=a._s3.children(),a.updateThumbsSize())}))},_k6:function(){var a=this,e="rsThumbs",c=a.st.thumbs,
b="",g,d,h=c.spacing;a._j5=!0;a._e3="vertical"===c.orientation?!1:!0;a._n6=g=h?' style="margin-'+(a._e3?"right":"bottom")+":"+h+'px;"':"";a._i3=0;a._p6=!1;a._m5=!1;a._l6=!1;a._q6=c.arrows&&c.navigation;d=a._e3?"Hor":"Ver";a.slider.addClass("rsWithThumbs rsWithThumbs"+d);b+='<div class="rsNav rsThumbs rsThumbs'+d+'"><div class="'+e+'Container">';a._o6=c.appendSpan?'<span class="thumbIco"></span>':"";for(var j=0;j<a.numSlides;j++)d=a.slides[j],b+="<div"+g+' class="rsNavItem rsThumb">'+d.thumbnail+a._o6+
"</div>";b=f(b+"</div></div>");g={};c.paddingTop&&(g[a._e3?"paddingTop":"paddingLeft"]=c.paddingTop);c.paddingBottom&&(g[a._e3?"paddingBottom":"paddingRight"]=c.paddingBottom);b.css(g);a._s3=f(b).find("."+e+"Container");a._q6&&(e+="Arrow",c.arrowLeft?a._r6=c.arrowLeft:(a._r6=f('<div class="'+e+" "+e+'Left"><div class="'+e+'Icn"></div></div>'),b.append(a._r6)),c.arrowRight?a._s6=c.arrowRight:(a._s6=f('<div class="'+e+" "+e+'Right"><div class="'+e+'Icn"></div></div>'),b.append(a._s6)),a._r6.click(function(){var b=
(Math.floor(a._i3/a._t6)+a._u6)*a._t6;a._a4(b>a._n3?a._n3:b)}),a._s6.click(function(){var b=(Math.floor(a._i3/a._t6)-a._u6)*a._t6;a._a4(b<a._o3?a._o3:b)}),c.arrowsAutoHide&&!a.hasTouch&&(a._r6.css("opacity",0),a._s6.css("opacity",0),b.one("mousemove.rsarrowshover",function(){a._l6&&(a._r6.css("opacity",1),a._s6.css("opacity",1))}),b.hover(function(){a._l6&&(a._r6.css("opacity",1),a._s6.css("opacity",1))},function(){a._l6&&(a._r6.css("opacity",0),a._s6.css("opacity",0))})));a._k5=b;a._l5=a._s3.children();
a.msEnabled&&a.st.thumbs.navigation&&a._s3.css("-ms-touch-action",a._e3?"pan-y":"pan-x");a.slider.append(b);a._w3=!0;a._v6=h;c.navigation&&a._e&&a._s3.css(a._g+"transition-property",a._g+"transform");a._k5.on("click.rs",".rsNavItem",function(){a._m5||a.goTo(f(this).index())});a.ev.off("rsBeforeSizeSet.thumbs").on("rsBeforeSizeSet.thumbs",function(){a._w6=a._e3?a._c4:a._b4;a.updateThumbsSize(!0)})},updateThumbsSize:function(){var a=this,e=a._l5.first(),c={},b=a._l5.length;a._t6=(a._e3?e.outerWidth():
e.outerHeight())+a._v6;a._y3=b*a._t6-a._v6;c[a._e3?"width":"height"]=a._y3+a._v6;a._z3=a._e3?a._k5.width():a._k5.height();a._o3=-(a._y3-a._z3)-a.st.thumbs.firstMargin;a._n3=a.st.thumbs.firstMargin;a._u6=Math.floor(a._z3/a._t6);if(a._y3<a._z3)a.st.thumbs.autoCenter&&a._q3((a._z3-a._y3)/2),a.st.thumbs.arrows&&a._r6&&(a._r6.addClass("rsThumbsArrowDisabled"),a._s6.addClass("rsThumbsArrowDisabled")),a._l6=!1,a._m5=!1,a._k5.off(a._j1);else if(a.st.thumbs.navigation&&!a._l6&&(a._l6=!0,!a.hasTouch&&a.st.thumbs.drag||
a.hasTouch&&a.st.thumbs.touch))a._m5=!0,a._k5.on(a._j1,function(b){a._g2(b,!0)});a._e&&(c[a._g+"transition-duration"]="0ms");a._s3.css(c);if(a._w3&&(a.isFullscreen||a.st.thumbs.fitInViewport))a._e3?a._c4=a._w6-a._k5.outerHeight():a._b4=a._w6-a._k5.outerWidth()},setThumbsOrientation:function(a,e){this._w3&&(this.st.thumbs.orientation=a,this._k5.remove(),this.slider.removeClass("rsWithThumbsHor rsWithThumbsVer"),this._k6(),this._k5.off(this._j1),e||this.updateSliderSize(!0))},_q3:function(a){this._i3=
a;this._e?this._s3.css(this._x1,this._y1+(this._e3?a+this._z1+0:0+this._z1+a)+this._a2):this._s3.css(this._e3?this._x1:this._w1,a)},_a4:function(a,e,c,b,g){var d=this;if(d._l6){e||(e=d.st.thumbs.transitionSpeed);d._i3=a;d._x6&&clearTimeout(d._x6);d._p6&&(d._e||d._s3.stop(),c=!0);var h={};d._p6=!0;d._e?(h[d._g+"transition-duration"]=e+"ms",h[d._g+"transition-timing-function"]=c?f.rsCSS3Easing[d.st.easeOut]:f.rsCSS3Easing[d.st.easeInOut],d._s3.css(h),d._q3(a)):(h[d._e3?d._x1:d._w1]=a+"px",d._s3.animate(h,
e,c?"easeOutCubic":d.st.easeInOut));b&&(d._i3=b);d._y6();d._x6=setTimeout(function(){d._p6=!1;g&&(d._a4(b,g,!0),g=null)},e)}},_y6:function(){this._q6&&(this._i3===this._n3?this._r6.addClass("rsThumbsArrowDisabled"):this._r6.removeClass("rsThumbsArrowDisabled"),this._i3===this._o3?this._s6.addClass("rsThumbsArrowDisabled"):this._s6.removeClass("rsThumbsArrowDisabled"))},_m6:function(a,e){var c=0,b,f=a*this._t6+2*this._t6-this._v6+this._n3,d=Math.floor(this._i3/this._t6);this._l6&&(this._j6&&(e=!0,
this._j6=!1),f+this._i3>this._z3?(a===this.numSlides-1&&(c=1),d=-a+this._u6-2+c,b=d*this._t6+this._z3%this._t6+this._v6-this._n3):0!==a?(a-1)*this._t6<=-this._i3+this._n3&&a-1<=this.numSlides-this._u6&&(b=(-a+1)*this._t6+this._n3):b=this._n3,b!==this._i3&&(c=void 0===b?this._i3:b,c>this._n3?this._q3(this._n3):c<this._o3?this._q3(this._o3):void 0!==b&&(e?this._q3(b):this._a4(b))),this._y6())}});f.rsModules.thumbnails=f.rsProto._h6})(jQuery);
// jquery.rs.video v1.1.1
(function(f){f.extend(f.rsProto,{_z6:function(){var a=this;a._a7={autoHideArrows:!0,autoHideControlNav:!1,autoHideBlocks:!1,autoHideCaption:!1,disableCSS3inFF:!0,youTubeCode:'<iframe src="http://www.youtube.com/embed/%id%?rel=1&autoplay=1&showinfo=0&autoplay=1&wmode=transparent" frameborder="no"></iframe>',vimeoCode:'<iframe src="http://player.vimeo.com/video/%id%?byline=0&amp;portrait=0&amp;autoplay=1" frameborder="no" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>'};a.st.video=
f.extend({},a._a7,a.st.video);a.ev.on("rsBeforeSizeSet",function(){a._b7&&setTimeout(function(){var b=a._r1,b=b.hasClass("rsVideoContainer")?b:b.find(".rsVideoContainer");a._c7&&a._c7.css({width:b.width(),height:b.height()})},32)});var c=a._a.mozilla;a.ev.on("rsAfterParseNode",function(b,e,d){b=f(e);if(d.videoURL){a.st.video.disableCSS3inFF&&c&&(a._e=a._f=!1);e=f('<div class="rsVideoContainer"></div>');var g=f('<div class="rsBtnCenterer"><div class="rsPlayBtn"><div class="rsPlayBtnIcon"></div></div></div>');
b.hasClass("rsImg")?d.content=e.append(b).append(g):d.content.find(".rsImg").wrap(e).after(g)}});a.ev.on("rsAfterSlideChange",function(){a.stopVideo()})},toggleVideo:function(){return this._b7?this.stopVideo():this.playVideo()},playVideo:function(){var a=this;if(!a._b7){var c=a.currSlide;if(!c.videoURL)return!1;var b=a._d7=c.content,c=c.videoURL,e,d;c.match(/youtu\.be/i)||c.match(/youtube\.com/i)?(d=/^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/,(d=c.match(d))&&11==
d[7].length&&(e=d[7]),void 0!==e&&(a._c7=a.st.video.youTubeCode.replace("%id%",e))):c.match(/vimeo\.com/i)&&(d=/(www\.)?vimeo.com\/(\d+)($|\/)/,(d=c.match(d))&&(e=d[2]),void 0!==e&&(a._c7=a.st.video.vimeoCode.replace("%id%",e)));a.videoObj=f(a._c7);a.ev.trigger("rsOnCreateVideoElement",[c]);a.videoObj.length&&(a._c7=f('<div class="rsVideoFrameHolder"><div class="rsPreloader"></div><div class="rsCloseVideoBtn"><div class="rsCloseVideoIcn"></div></div></div>'),a._c7.find(".rsPreloader").after(a.videoObj),
b=b.hasClass("rsVideoContainer")?b:b.find(".rsVideoContainer"),a._c7.css({width:b.width(),height:b.height()}).find(".rsCloseVideoBtn").off("click.rsv").on("click.rsv",function(b){a.stopVideo();b.preventDefault();b.stopPropagation();return!1}),b.append(a._c7),a.isIPAD&&b.addClass("rsIOSVideo"),a._e7(!1),setTimeout(function(){a._c7.addClass("rsVideoActive")},10),a.ev.trigger("rsVideoPlay"),a._b7=!0);return!0}return!1},stopVideo:function(){var a=this;return a._b7?(a.isIPAD&&a.slider.find(".rsCloseVideoBtn").remove(),
a._e7(!0),setTimeout(function(){a.ev.trigger("rsOnDestroyVideoElement",[a.videoObj]);var c=a._c7.find("iframe");if(c.length)try{c.attr("src","")}catch(b){}a._c7.remove();a._c7=null},16),a.ev.trigger("rsVideoStop"),a._b7=!1,!0):!1},_e7:function(a){var c=[],b=this.st.video;b.autoHideArrows&&(this._c2&&(c.push(this._c2,this._d2),this._e2=!a),this._v5&&c.push(this._v5));b.autoHideControlNav&&this._k5&&c.push(this._k5);b.autoHideBlocks&&this.currSlide.animBlocks&&c.push(this.currSlide.animBlocks);b.autoHideCaption&&
this.globalCaption&&c.push(this.globalCaption);if(c.length)for(b=0;b<c.length;b++)a?c[b].removeClass("rsHidden"):c[b].addClass("rsHidden")}});f.rsModules.video=f.rsProto._z6})(jQuery);
// jquery.rs.visible-nearby v1.0.2
(function(d){d.rsProto._f7=function(){var a=this;a.st.visibleNearby&&a.st.visibleNearby.enabled&&(a._g7={enabled:!0,centerArea:0.6,center:!0,breakpoint:0,breakpointCenterArea:0.8,hiddenOverflow:!0,navigateByCenterClick:!1},a.st.visibleNearby=d.extend({},a._g7,a.st.visibleNearby),a.ev.one("rsAfterPropsSetup",function(){a._h7=a._e1.css("overflow","visible").wrap('<div class="rsVisibleNearbyWrap"></div>').parent();a.st.visibleNearby.hiddenOverflow||a._h7.css("overflow","visible");a._o1=a.st.controlsInside?
a._h7:a.slider}),a.ev.on("rsAfterSizePropSet",function(){var b,c=a.st.visibleNearby;b=c.breakpoint&&a.width<c.breakpoint?c.breakpointCenterArea:c.centerArea;a._h?(a._b4*=b,a._h7.css({height:a._c4,width:a._b4/b}),a._d=a._b4*(1-b)/2/b):(a._c4*=b,a._h7.css({height:a._c4/b,width:a._b4}),a._d=a._c4*(1-b)/2/b);c.navigateByCenterClick||(a._q=a._h?a._b4:a._c4);c.center&&a._e1.css("margin-"+(a._h?"left":"top"),a._d)}))};d.rsModules.visibleNearby=d.rsProto._f7})(jQuery);