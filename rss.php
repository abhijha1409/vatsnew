<?php include_once('config.php');
	  include_once('function.php');
	  include_once('header.php');
	  include_once('abc.php');
?>
<html>
<head>
<title>Site Map</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<style>
* {
  box-sizing: border-box;
}
.menu {
  float: left;
  width: 20%;
}
.menuitem {
  padding: 8px;
  margin-top: 7px;
  border-bottom: 1px solid #f1f1f1;
}
.main2 {
  float: left;
  width: 60%;
  padding: 62px 33px;
  overflow: hidden;
  font-size: 12px;
  background: white;
  width: 100%;
}
.right {
  background-color: lightblue;
  float: left;
  width: 20%;
  padding: 10px 15px;
  margin-top: 7px;
}

@media only screen and (max-width:800px) {
  /* For tablets: */
  .main {
    width: 80%;
    padding: 0;
  }
  .right {
    width: 100%;
  }
}
@media only screen and (max-width:500px) {
  /* For mobile phones: */
  .menu, .main, .right {
    width: 100%;
  }
}
.title {
    text-decoration: underline;
	
}
.btn2 {
    background-color: #007ea4;
    border: none;
    color: white;
    padding: 1px 16px;
    font-size: 16px;
    cursor: pointer;
}
 
</style>
</head>
<body>

<br>
<br>
<center><h1 class="title">RSS Feed</h1></center>
 <div class="main2">

<br>

<br>
<div>
<table style="border:none !important">
<tr>
<?php 
$res="SELECT * FROM category";	
$run=mysqli_query($dbcon,$res);
while($data=mysqli_fetch_assoc($run))
		
{ 
	?>
	
	<td><a href="single.php?cat_id=<?php echo $data['slug']; ?>"style="color:black" title="<?php echo $data['cat_title'];?>"><?php echo $data['cat_title'];?></a><td>
	<td><a href="catrss.php?cat_id=<?php  echo $data['cat_id']; ?>"<button type="button" class="btn2">Rss&nbsp;<i class="fa fa-rss"></i></button></a></td>
		</tr>	
<?php
}

?>

</table>
</div>
</div>
<div>
<font color="white">.</font>
</div>

</body>
<?php require_once('footer-new.php');?>
</html>
