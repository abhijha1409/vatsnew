
<?php
	session_start();
	require_once 'dbconnect.php';
	
	// if session is not set this will redirect to login page
    	  if( !isset($_SESSION['user']) ) {
		header("Location: login.php");
		exit;
	}
	 
	// select loggedin users detail
	$res=mysqli_query($conn,"SELECT * FROM admin WHERE userId=".$_SESSION['user']);
	$userRow=mysqli_fetch_array($res);
	
		//echo $userRow['userName']; 
	
				
?>

<!DOCTYPE html>
<html lang="en">
	<head>
	<title>Dashboard </title>
	<meta name="description" content="<?php echo $pageDescription; ?>">
	</head>

    <!-- Bootstrap -->
    <link href="../admin/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
	<link rel="stylesheet" href="../admin/assets/css/bootstrap.min.css" type="text/css"  />
    <link href="../admin/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    
    <!-- Custom Theme Style -->
    <link href="../admin/css/custom.min.css" rel="stylesheet">
    <!-- slug use js-->
     <script src='js/jquery.min.js'></script>
 <script src='http://tinymce.cachefly.net/4.2/tinymce.min.js'></script>
 <script src="js/index.js"></script>
  <!-- end slug use js-->
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="index.php" class="site_title"><i class="fa fa-paw"></i> <span>Dashboard</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
           
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                
                <ul class="nav side-menu">
                  <li><a><i class="fa fa-home"></i> Home<span class="fa fa-chevron-down"></span></a>
                   <ul class="nav child_menu">
					 <!-- <li><a href="meta.php">Add Meta Tag</a></li>-->
                      <li><a href="meta_all.php">All Meta Tag</a></li>
					  <li><a href="meta_trash_all.php">Trash</a></li>
                      </ul>
                      </li>
                   <li><a><i class="fa fa-edit"></i> Posts <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
					  <!--<li><a href="post.php">Add New</a></li>-->
                      <li><a href="post_all.php">All Posts</a></li>
					  <li><a href="post_trash_all.php">Trash</a></li>
                      </ul>
                  </li>
                  <li><a><i class="fa fa-desktop"></i> Media <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
					  <li><a href="uploads.php">Media All</a></li>
					  </ul>
					  </li>
                  <li><a><i class="fa fa-desktop"></i> Pages <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
					  <!--<li><a href="page.php">Add New</a></li>-->
                      <li><a href="page_all.php">All Pages</a></li>
                       <li><a href="page_trash_all.php">Trash</a></li>
                    </ul>
                  </li>
				  <li><a><i class="fa fa-clone"></i>Categories <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
					 <!-- <li><a href="category.php">Add Categories</a></li>-->
                      <li><a href="category_all.php">All Categories</a></li>
                       <li><a href="category_trash_all.php">Trash</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-graduation-cap"></i>Qualification <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
					 <!-- <li><a href="qualification.php">Add Qualification</a></li>-->
                      <li><a href="qualification_all.php">All Qualification</a></li>
                       <li><a href="qualification_trash_all.php">Trash</a></li>
                    </ul>
                  </li>
				  <li><a><i class="fa fa-files-o"></i>Area <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
					  <!--<li><a href="area.php">Add Area</a></li>-->
                      <li><a href="area_all.php">All Area</a></li>
                       <li><a href="area_trash_all.php">Trash</a></li>
                    </ul>
                  </li>
                   <li><a><i class="fa fa-credit-card custom"></i>Role<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
					  <!--<li><a href="role.php">Add Role</a></li>-->
                      <li><a href="role_all.php">All Role</a></li>
                       <li><a href="role_trash_all.php">Trash</a></li>
                    </ul>
                  </li>
				   
				  <li><a><i class="fa fa-bars"></i>Tag <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
					  <!--<li><a href="tag.php">Add Tag</a></li-->
                      <li><a href="tag_all.php">All Tag</a></li>
					   <li><a href="tag_trash_all.php">Trash</a></li>
                      <!--<li><a href="tag_sub_all.php">Sub Tag</a></li>-->
                      
                    </ul>
                  </li>
                 <!--<li><a><i class="fa fa-bars"></i>Tag Page <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="tag_sub_all.php">All Tag Page</a></li>
                    </ul>
                  </li>-->
                  
                  <li><a><i class="fa fa-table"></i> Users <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="users.php">All Users</a></li>
                      <!--<li><a href="add-user.php">Add New</a></li>-->
					 <!-- <li><a href="edit-user.php">Your Profile</a></li>-->
                    </ul>
                  </li>
				  <li><a><i class="fa fa-table"></i> Comment <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="comment.php">All Comment</a></li>
                      
                    </ul>
                  </li>
                   <li><a><i class="fa fa-table"></i> Web advertisement <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="home1_add.php">Home Page</a></li>
                      <li><a href="category_adds.php">Category Page</a></li>
                      <li><a href="single1_add.php">Single Page</a></li>
					  <li><a href="search_add.php">Search Page</a></li>
                      
                    </ul>
                  </li>
                  
                  
                </ul>
              </div>
              

            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>
            <!-- start -->
              <ul class="nav navbar-nav navbar-right">
             <li class="">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
			  <span class="glyphicon glyphicon-user"></span>&nbsp;Hi' <?php echo $userRow['userName']; ?>&nbsp;<span class="caret"></span></a>
              <ul class="dropdown-menu">
				<li><a href="Change_Password.php"><span class="glyphicon glyphicon-log-out"></span>&nbsp;Password Change</a></li>
				<li><a href="logout.php?logout"><span class="glyphicon glyphicon-log-out"></span>&nbsp;Sign Out</a></li>
				<li><a href="meta_all.php"><span class="glyphicon glyphicon-log-out"></span>&nbsp;Home Meta Tag</a></li>
              </ul>
            </li>
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->