<?php 
include_once('dbconnect.php');
include_once('header.php');
	?>
        
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
		  
	  <?php
	if(isset($_GET['tag_id'])){
	
		$tag_id=$_GET['tag_id'];
		$sql="UPDATE tag SET active = 0 WHERE id='$tag_id'";
		
		$run=mysqli_query($conn,$sql);
		
		if($run){ 
	echo "<div class='alert alert-danger' style='width:300px;'>Deleted Successfully</div>";
   ?>   
   <script>
   setTimeout(function () {
   window.location.href= 'tag_all.php'; // the redirect goes here

},2000); // 2 seconds time out
 </script>
   <?php 
	}else
		{
			echo "Not Delete";
		}
	}
	
// start pagination
	 $per_page=50;
	 if(isset($_GET['page'])){
		 $page=$_GET['page'];
	 }else{
		$page=1; 
	 }
	 $start_form=($page-1) * $per_page;
 // end pagination
?>
            
<!Doctype html>
<head>
  <title>All Tag</title>
</head>
<body>
    <a href='tag.php' class="btn btn-primary">Add New</a>
  <h4>All Tag</h4>
<div>
<form name="Item Search" method="post" action="search_tag.php" style=" margin-left:0px;";>  
		  <label> 
		  <input name="swords" type="text" size="15" maxlength="15"> 
		  </label>  
		   
		  <label> 
		  <input name="search" type="submit" id="search" value="Search"> 
		  </label>
		  </form>
		  <!-- start -->
		  
 <table class="table table-striped table-condensed table-bordered table-responsive">
    <thead class="btn-primary">
        <tr> 
		  <th>Sr.NO</th>
		 <th>Tag Name</th>
		 <th>Title</th>
	    <th>Description</th>
		 <th>Keywords</th>
		  <th>Edit</th>
		  <th>Delete</th>
		  
  
         </tr>
   </thead>
  <tbody>
   <?php 
   $sql="SELECT * FROM tag where active = 1 limit $start_form,$per_page";
    $counter = $start_form+1;
   $run=mysqli_query($conn,$sql);
   while($rows=mysqli_fetch_assoc($run)){
	   
   
   ?>
   <tr>
   <td><?php echo  $counter; ?></td>
   <td><?php echo $rows['tag_name']; ?></td>
   <td><?php echo $rows['meta_title']; ?></td>
   <td><?php echo $rows['meta_description']; ?></td>
   <td><?php echo $rows['meta_keywords']; ?></td>
   <td><a href="edit_tag.php?tag_id=<?php echo $rows['id']; ?>" class="btn btn-warning btn-xa navbar-btn btn-xs">Edit</a></td>
   <td><a href="tag_all.php?tag_id=<?php echo $rows['id']; ?>" class="btn btn-danger btn-xa navbar-btn btn-xs" onclick="return confirm('Are you sure you want to delete this item?');">Delete</a></td>
   

         </tr>
		 <?php
 $counter++;  
  }
   ?>
   
      </tbody>		 
       </table>
	   <!--pagination Start-->
	<ul class="pagination">
	   <?php
	   $pagination_sql="SELECT * FROM tag";
	   $run_pagination=mysqli_query($conn,$pagination_sql);
	   $count=mysqli_num_rows($run_pagination);
	   $total_pages=ceil($count/$per_page);
	   for($i=1;$i<=$total_pages;$i++)
	   {
		   echo '<li><a href="tag_all.php?page='.$i.'">'.$i.'</a></li>';
	   }
	   ?>
			
	</ul>
			<!--End pagination -->
	   
</div>	   
		 </body>
</html>
</div>
        </div>
        <!-- /page content -->
<?php include_once('footer.php');?>