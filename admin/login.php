<?php
	
	session_start();
	require_once 'dbconnect.php';
	
	
	// it will never let you open index(login) page if session is set
	if ( isset($_SESSION['user'])!="" ) {
		header("Location: index.php");
		exit;
	}
	
	$error = false;
	
	if( isset($_POST['btn-login']) ) {	
		
		// prevent sql injections/ clear user invalid inputs
		$userEmail = trim($_POST['userEmail']);
		$userEmail = strip_tags($userEmail);
		$userEmail = htmlspecialchars($userEmail);
		
		$userPass = trim($_POST['userPass']);
		$userPass = strip_tags($userPass);
		$userPass = htmlspecialchars($userPass);
		// prevent sql injections / clear user invalid inputs
		
		if(empty($userEmail)){
			$error = true;
			$emailError = "Please enter your userEmail address.";
		} else if ( !filter_var($userEmail,FILTER_VALIDATE_EMAIL) ) {
			$error = true;
			$emailError = "Please enter valid userEmail address.";
		}
		
		if(empty($userPass)){
			$error = true;
			$passError = "Please enter your password.";
		}
		
		// if there's no error, continue to login
		if (!$error) {
			
			$password = md5($userPass); // password hashing using SHA256
		     //$password = hash('sha256', $userPass); 
			$res=mysqli_query($conn,"SELECT userId, userName, userPass FROM admin WHERE userEmail='$userEmail'");
			$row=mysqli_fetch_array($res);
			$count = mysqli_num_rows($res); // if uname/pass correct it returns must be 1 row
			
			if( $count == 1 && $row['userPass']==$password ) {
				$_SESSION['user'] = $row['userId'];
				header("Location: index.php");
			} else {
				$errMSG = "Incorrect Credentials, Try again...";
			}
				
		}
		
	}
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Login</title>
<link rel="stylesheet" href="assets/css/bootstrap.min.css" type="text/css"  />
<link rel="stylesheet" href="style.css" type="text/css" />
</head>
<body>

<div class="container">

	<div id="login-form">
	<center>
    <form method="post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" autocomplete="off" style="width: 40%; margin-top:90px;">
    
    	<div class="col-md-12">
        
        	<div class="form-group">
			
            	<h3 class="">Sign In.</h3>
            </div>
        
        	<div class="form-group">
            	<hr />
            </div>
            
            <?php
			if ( isset($errMSG) ) {
				
				?>
				<div class="form-group">
            	<div class="alert alert-danger">
				<span class="glyphicon glyphicon-info-sign"></span> <?php echo $errMSG; ?>
                </div>
            	</div>
                <?php
			}
			?>
            
            <div class="form-group">
            	<div class="input-group">
                <span class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span></span>
            	<input type="email" name="userEmail" class="form-control" placeholder="Your Email" value="<?php echo $userEmail; ?>" maxlength="40" />
                </div>
                <span class="text-danger"><?php echo $emailError; ?></span>
            </div>
            
            <div class="form-group">
            	<div class="input-group">
                <span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
            	<input type="password" name="userPass" class="form-control" placeholder="Your Password" maxlength="15" />
                </div>
                <span class="text-danger"><?php echo $passError; ?></span>
            </div>
            
            <div class="form-group">
            	<hr />
            </div>
            
            <div class="form-group">
			
            	<button type="submit" class="btn btn-block btn-primary" name="btn-login">Sign In</button>
            </div>
            
            <div class="form-group">
            	<hr />
            </div>
            
            <div class="form-group">
            	<!--<a href="register.php">Sign Up Here...</a>-->
            </div>
			<div class="form-group">
            	<a href="forgot_pass.php">Forgot Password</a>
            </div>
        
        </div>
   
    </form>
	</center>
    </div>	

</div>

</body>
</html>

