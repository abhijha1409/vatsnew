<?php 
include_once('dbconnect.php');
include_once('header.php');
	?>
        
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
 <?php
	if(isset($_GET['meta_id'])){
		$meta_id=$_GET['meta_id'];
		$sql="UPDATE meta SET active = 0 WHERE meta_id='$meta_id'";
		$run=mysqli_query($conn,$sql);
		if($run){
	echo "<div class='alert alert-danger' style='width:300px;'>Deleted Successfully</div>";
   ?>   
   <script>
   setTimeout(function () {
   window.location.href= 'meta_all.php'; // the redirect goes here

},2000); // 2 seconds time out
 </script>
   <?php 
	}else
		{
			echo "Not Delete";
		}
	}
	
// start pagination
	 $per_page=50;
	 if(isset($_GET['page'])){
		 $page=$_GET['page'];
	 }else{
		$page=1; 
	 }
	 $start_form=($page-1) * $per_page;
 // end pagination
?>	
<!Doctype html>
<head>
</head>
<body>
  <h4>Home Page Meta Tag</h4>
<div>
  <div class="container">
   <a href='meta.php' class="btn btn-primary">Add New</a>
    <table class="table table-striped table-condensed table-bordered table-responsive">
          <thead class="btn-primary">
        <tr> 
		  <th>Sr.NO</th>
		 <th>Title</th>
		 <th>Description</th>
		 <th>Keywords</th>
		  <th>Action</th>
		  
         </tr>
   </thead>
  <tbody>
   <?php 
   $sql="SELECT * FROM meta where active = 1 ";
   $run=mysqli_query($conn,$sql);
   while($rows=mysqli_fetch_assoc($run)){
   ?>
   <tr>
   <td><?php echo $rows['meta_id']; ?></td>
   <td><?php echo $rows['meta_title']; ?></td>
   <td><?php echo $rows['meta_description']; ?></td>
   <td><?php echo $rows['meta_keywords']; ?></td>
   <td><a href="edit_meta.php?meta_id=<?php echo $rows['meta_id']; ?>" class="btn btn-warning btn-xa navbar-btn btn-xs">Edit</a>|
		<a href="meta_all.php?meta_id=<?php echo $rows['meta_id']; ?>" class="btn btn-danger btn-xa navbar-btn btn-xs" onclick="return confirm('Are you sure you want to delete this item?');">Delete</a></td>

         </tr>
		 <?php
   }
   ?>
   
      </tbody>		 
       </table>
	  </div></div>	   
		 </body>
</html>
</div>
        </div>
<?php include_once('footer.php');?>