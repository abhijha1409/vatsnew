<?php 
require_once('config.php');
include_once('dbconnect.php');
include_once('header.php');
	?>
 <!-- page content -->
        <div class="right_col" role="main">
          <div class="">

<body>

<div id="wrapper">

	<h2>Edit User</h2>
   

	<?php

	//if form has been submitted process it
	if(isset($_POST['submit'])){

		//collect form data
		extract($_POST);

		//very basic validation
		if($userName ==''){
			$error[] = '<div class="alert alert-warning">Please enter the username.</div>';
		}
		if($userEmail ==''){
			$error[] = 'Please enter the email address.';
		}

		if( strlen($userPass) > 0){

			if($userPass ==''){
				$error[] = 'Please enter the password.';
			}

			if($passwordConfirm ==''){
				$error[] = '<div class="alert alert-danger" style="width:250px;">Please confirm the password.</div>';
			}

			if($userPass != $passwordConfirm){
				$error[] = '<div class="alert alert-warning" style="width:200px; margin:0px;">Passwords do not match.</div>';
			}

		}
		

		

		if(!isset($error)){

			try {

				if(isset($userPass)){

					$hashedpassword = $user->password_hash($userPass, PASSWORD_BCRYPT);
					echo $hashedpassword;

					//update into database
					$stmt = $db->prepare('UPDATE admin SET userName = :userName,userEmail = :userEmail, userPass = :userPass WHERE userId = :userId') ;
					$stmt->execute(array(
						':userName' => $userName,
						':userPass' => $hashedpassword,
						':userEmail' => $userEmail,
						':userId' => $userId
					));


				} else {

					//update database
					$stmt = $db->prepare('UPDATE admin SET userName = :userName, userEmail = :userEmail WHERE userId = :userId') ;
					$stmt->execute(array(
						':userName' => $userName,
						':userEmail' => $userEmail,
						':userId' => $userId
					));

				}
				

				//redirect to index page
				header('Location: users.php?action=updated');
				exit;

			} catch(PDOException $e) {
			    echo $e->getMessage();
			}

		}

	}

	?>


	<?php
	//check for any errors
	if(isset($error)){
		foreach($error as $error){
			echo $error.'<br />';
		}
	}

		try {

			$stmt = $db->prepare('SELECT userId, userName, userEmail FROM admin WHERE userId = :userId') ;
			$stmt->execute(array(':userId' => $_GET['id']));
			$row = $stmt->fetch(); 

		} catch(PDOException $e) {
		    echo $e->getMessage();
		}

	?>
    <div class="bs-example">
	<form action='' method='post'>
	<div class="form-group">
		<input type='hidden' name='userId' value='<?php echo $row['userId'];?>'>
		</div>
          <div class="form-group">
		<label for="inputuser">Username</label>
		<input type='text' name='userName' value='<?php echo $row['userName'];?>'>
         </div>
		 <div class="form-group">
		<label for="inputEmail">Email</label>
		<input type='email' name='userEmail' value='<?php echo $row['userEmail'];?>'>
          </div>
		 <div class="form-group">
		<label for="inputPassword">Password</label>
		<input type='password' name='userPass' value=''>
        </div>
		<div class="form-group">
		<label for="inputConfirmPassword">Confirm Password</label>
		<input type='password' name='passwordConfirm' value=''>
         </div>
		 
		<button type='submit' name='submit' value='Update User' class="btn btn-primary">Update User</button>

	</form>
 </div>
</div> 

		  
		  </div>
        </div>
        <!-- /page content -->
<?php include_once('footer.php');?>
	
        