<!-- footer content -->
        <footer>
          <div class="pull-right">
            Thank you for creating
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <!--<script src="js/jquery.min.js"></script>-->
    <!-- Bootstrap -->
    <script src="js/bootstrap.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="js/bootstrap-progressbar.min.js"></script>
    
    <!-- Custom Theme Scripts -->
    <script src="js/custom.min.js"></script>

  </body>
</html>