<?php 
include_once('dbconnect.php');
include_once('header.php');
	?>
        
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
		  <?php
		if(isset($_GET['qualification_id'])){
			$qualification_id=$_GET['qualification_id'];
			$sql="UPDATE qualification SET active = 0 WHERE qualification_id='$qualification_id'";
			$run=mysqli_query($conn,$sql);
			if($run){
		echo "<div class='alert alert-danger' style='width:300px;'>Deleted Successfully</div>";
		?>
		<script>
   setTimeout(function () {
   window.location.href= 'qualification_all.php'; // the redirect goes here

},2000); // 2 seconds time out
 </script> 
		<?php
			}else{
				echo "Not Delete";
				
			}
		}
		// start pagination******
	 $per_page=25;
	 if(isset($_GET['page'])){
		 $page=$_GET['page'];
	 }else{
		$page=1; 
	 }
	 $start_form=($page-1) * $per_page;
 // end pagination******
         ?>
            
<!Doctype html>
<head>
  <title>All qualification</title>
</head>
<body>
   <a href='qualification.php' class="btn btn-primary">Add New</a> 
	  <form name="Item Search" method="post" action="search_qualification.php">
	    <input name="swords" type="text" name="search" id="search_name">
	    <input name="search" type="submit" id="search" value="Search">
	</form></br>
<div>

		  
 <table class="table table-striped table-condensed table-bordered table-responsive">
    <thead class="btn-primary">
        <tr> 
		  <th>Sr.NO</th>
		 <th>Qualification</th>
	     <th>Slug</th>
		 <th>Title</th>
		 <th>Description</th>
		 <th>Keywords</th>
		  <th>Edit</th>
		  <th>Delete</th>
		  
  
         </tr>
   </thead>
  <tbody>
   <?php 
   $sql="SELECT * FROM qualification where active = 1 order by qualification_id DESC limit $start_form,$per_page";
    $counter = $start_form+1;
   $run=mysqli_query($conn,$sql);
   while($rows=mysqli_fetch_assoc($run)){
	   
   
   ?>
   <tr>
   <td><?php echo $counter; ?></td>
   <td><?php echo $rows['qualification_name']; ?></td>
   <td><?php echo $rows['slug']; ?></td>
   <td><?php echo $rows['meta_title']; ?></td>
   <td><?php echo $rows['meta_description']; ?></td>
   <td><?php echo $rows['meta_keywords']; ?></td>
   <td><a href="edit_qualification.php?qualification_id=<?php echo $rows['qualification_id']; ?>" class="btn btn-warning btn-xa navbar-btn btn-xs">Edit</a></td>
   <td><a href="qualification_all.php?qualification_id=<?php echo $rows['qualification_id']; ?>" class="btn btn-danger btn-xa navbar-btn btn-xs" onclick="return confirm('Are you sure you want to delete this item?');">Delete</a></td>
   

         </tr>
		 <?php
    $counter++;
   }
   ?>
   
      </tbody>		 
       </table>
	   <!--pagination Start-->
	<ul class="pagination">
	   <?php
	   $pagination_sql="SELECT * FROM qualification";
	   $run_pagination=mysqli_query($conn,$pagination_sql);
	   $count=mysqli_num_rows($run_pagination);
	   $total_pages=ceil($count/$per_page);
	   for($i=1;$i<=$total_pages;$i++)
	   {
		   echo '<li><a href="qualification_all.php?page='.$i.'">'.$i.'</a></li>';
	   }
	   ?>
			
	</ul>
			<!--End pagination -->
	   
</div>	   
		 </body>
</html>
</div>
        </div>
        <!-- /page content -->
<?php include_once('footer.php');?>