<?php 
require_once('config.php');
include_once('dbconnect.php');
include_once('header.php');
	?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
           <?php
	//ob_start();
	//session_start();
	//if( isset($_SESSION['user'])!="" ){
		//header("Location: home.php");
	//}
	

	$error = false;

	if ( isset($_POST['signup']) ) {
		
		// clean user inputs to prevent sql injections
		$name = trim($_POST['name']);
		$name = strip_tags($name);
		$name = htmlspecialchars($name);
		
		$email = trim($_POST['email']);
		$email = strip_tags($email);
		$email = htmlspecialchars($email);
		
		$pass = trim($_POST['pass']);
		$pass = strip_tags($pass);
		$pass = htmlspecialchars($pass);
		
		// basic name validation
		if (empty($name)) {
			$error = true;
			$nameError = "Please enter your  name.";
		} else if (strlen($name) < 3) {
			$error = true;
			$nameError = "Name must have atleat 3 characters.";
		} else if (!preg_match("/^[a-zA-Z ]+$/",$name)) {
			$error = true;
			$nameError = "Name must contain alphabets and space.";
		}
		
		//basic email validation
		if ( !filter_var($email,FILTER_VALIDATE_EMAIL) ) {
			$error = true;
			$emailError = "Please enter valid email address.";
		} else {
			// check email exist or not
			$query = "SELECT userEmail FROM admin WHERE userEmail='$email'";
			$result = mysqli_query($conn, $query);
			$count = mysqli_num_rows($result);
			if($count!=0){
				$error = true;
				$emailError = "Provided Email is already in use.";
			}
		}
		// password validation
		if (empty($pass)){
			$error = true;
			$passError = "Please enter password.";
		} else if(strlen($pass) < 6) {
			$error = true;
			$passError = "Password must have atleast 6 characters.";
		}
		
		// password encrypt using SHA256();
		//$password = hash('sha256', $pass);
		$password = md5($pass);
		
		// if there's no error, continue to signup
		if( !$error ) {
			
			$query = "INSERT INTO admin(userName,userEmail,userPass) VALUES('$name','$email','$password')";
			$res = mysqli_query($conn, $query);
				
			if ($res) {
				$errTyp = "success";
				$errMSG = "Successfully add";
				unset($name);
				unset($email);
				unset($pass);
			} else {
				$errTyp = "danger";
				$errMSG = "Something went wrong, try again later...";	
			}
      ?>
			<script>
   setTimeout(function () {
   window.location.href= 'users.php'; // the redirect goes here

    },2000); // 2 seconds 
   </script>

			<?php			
				
		}
		
		
	}
?>

<body>

<div class="container">

	<div id="login-form">
    <form method="post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" autocomplete="off" >
    
    	<div class="col-md-12">
        
        	<div class="form-group">
            	<hr />
            </div>
            
            <?php
			if ( isset($errMSG) ) {
				
				?>
				<div class="form-group">
            	<div class="alert alert-<?php echo ($errTyp=="success") ? "success" : $errTyp; ?>">
				<span class="glyphicon glyphicon-info-sign"></span> <?php echo $errMSG; ?>
                </div>
            	</div>
                <?php
			}
			?>
            
            <div class="form-group">
			<!-- start-->
			<!--<div class="form-group">
		<label for="inputuser">Username</label>
		<input type='text' name='userName' class='form-control' placeholder='Username' value='<?php //if(isset($error)){ echo $_POST['userName'];}?>'>
         </div>
		 <!-- end-->
			
            	<div class="form-group">
				<label for="inputuser">Username</label>
            	<input type="text" name="name" class="form-control" placeholder="Enter Name" maxlength="50" value="<?php echo $name ?>" />
                </div>
                <span class="text-danger"><?php echo $nameError; ?></span>
            </div>
            
            <div class="form-group">
            	<label for="inputEmail">Email</label>
            	<input type="email" name="email" class="form-control" placeholder="Enter Your Email" maxlength="40" value="<?php echo $email ?>" />
                </div>
                <span class="text-danger"><?php echo $emailError; ?></span>
            </div>
            
            <div class="form-group">
            	<label for="inputPassword">Password</label>
            	<input type="password" name="pass" class="form-control" placeholder="Enter Password" maxlength="15" />
                </div>
                <span class="text-danger"><?php echo $passError; ?></span>
            </div>
            
            	<button type="submit" class="btn btn-primary" name="signup">Add User</button>
				<!--<button type='submit' name='submit' value='Add User'class="btn btn-primary">Add User</button>-->
            
        </div>
   
    </form>
	
    </div>	

</div>
		  
		  </div>
        </div>
        <!-- /page content -->
<?php include_once('footer.php');?>
	
        