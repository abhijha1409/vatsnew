<?php 
include_once('dbconnect.php');
include_once('header.php');
	?>
 
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
		  <?php
			if(isset($_GET['id'])){
				$id=$_GET['id'];
				$sql="Delete from sub_menu where id='$id'";
				$run=mysqli_query($conn,$sql);
				if($run){
			echo "<div class='alert alert-danger' style='width:300px;'>Deleted Successfully</div>";
			?>
					<script>
		   setTimeout(function () {
		   window.location.href= 'main_menu_all.php'; // the redirect goes here

		},2000); // 2 seconds 
		 </script> 
			<?php
				}else{
					echo "Not Delete";
					
				}
			}
		// start pagination******
		 $per_page=10;
		 if(isset($_GET['page'])){
			 $page=$_GET['page'];
		 }else{
			$page=1; 
		 }
		 $start_form=($page-1) * $per_page;
		 // end pagination******
		?>
            
<!Doctype html>
<head>
<title>All Sub Menus</title>
</head>
<body>
<h4>All Sub Menus</h4>
<div>
		  
  <table class="table table-striped table-condensed table-bordered table-responsive">
    <thead class="btn-primary">
        <tr> 
			  <!--<th>S.NO</th>-->
			 <th>menu Name</th>
			  <th>Edit</th>
			  <th>Delete</th>
  
        </tr>
    </thead>
   <tbody>
   <?php 
   $sql="SELECT * FROM sub_menu order by id DESC limit $start_form,$per_page";
   $run=mysqli_query($conn,$sql);
   while($rows=mysqli_fetch_assoc($run)){
	   
   
   ?>
   <tr>
   <!--<td><?php //echo $rows['id']; ?></td>-->
   <td><?php echo $rows['menu_name']; ?></td>
   <td><a href="edit_sub_menu.php?id=<?php echo $rows['id']; ?>" class="btn btn-warning btn-xa navbar-btn btn-xs">Edit</a></td>
   <td><a href="sub_menu_all.php?id=<?php echo $rows['id']; ?>" class="btn btn-danger btn-xa navbar-btn btn-xs" onclick="return confirm('Are you sure you want to delete this item?');">Delete</a></td>
   

         </tr>
		 <?php
   }
   ?>
   
    </tbody>
 </table>
	   <!--pagination Start-->
	<ul class="pagination">
	   <?php
	   $pagination_sql="SELECT * FROM sub_menu";
	   $run_pagination=mysqli_query($conn,$pagination_sql);
	   $count=mysqli_num_rows($run_pagination);
	   $total_pages=ceil($count/$per_page);
	   for($i=1;$i<=$total_pages;$i++)
	   {
		   echo '<li><a href="sub_menu_all.php?page='.$i.'">'.$i.'</a></li>';
	   }
	   ?>
			
	</ul>
			<!--End pagination -->
</div>	   
</body>
</html>
</div>
        </div>
        <!-- /page content -->
<?php include_once('footer.php');?>