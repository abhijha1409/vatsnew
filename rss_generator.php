<?php
error_reporting(E_ALL);
include_once("config.php");

//header('Content-Type: text/xml; charset=utf-8', true); //set document header content type to be XML
$xml = new DOMDocument("1.0", "UTF-8"); // Create new DOM document.

//create "RSS" element
$rss = $xml->createElement("rss"); 
$rss_node = $xml->appendChild($rss); //add RSS element to XML node
$rss_node->setAttribute("version","2.0"); //set RSS version
                                                                                                                                                                                           
//set attributes
$rss_node->setAttribute("xmlns:dc","http://www.vatsnew.co.in/dc/elements/1.1/");//xmlns:dc (info http://j.mp/1mHIl8e )
$rss_node->setAttribute("xmlns:content","http://www.vatsnew.co.in/rss/1.0/modules/content/"); //xmlns:content (info http://j.mp/1og3n2W)
//$rss_node->setAttribute("xmlns:atom","http://www.w3.org/2005/Atom");//xmlns:atom (http://j.mp/1tErCYX )

//Create RFC822 Date format to comply with RFC822
$date_f = date("D, d M Y H:i:s T", time());
$build_date = gmdate(DATE_RFC2822, strtotime($date_f));

//create "channel" element under "RSS" element
$channel = $xml->createElement("channel");  
$channel_node = $rss_node->appendChild($channel);
 
//add general elements under "channel" node
$channel_node->appendChild($xml->createElement("title", "http://www.vatsnew.co.in  News Headlines")); //title
$channel_node->appendChild($xml->createElement("description", "The latest News from http://www.vatsnew.co.in listing of Government, Private Recruitment, Jobs, Careers, Vacancy jobs"));  //description
$channel_node->appendChild($xml->createElement("link", "http://www.vatsnew.co.in")); //website link 
$channel_node->appendChild($xml->createElement("language", "en-us"));  //language
$channel_node->appendChild($xml->createElement("lastBuildDate", $build_date));  //last build date
$channel_node->appendChild($xml->createElement("generator", "PHP DOMDocument")); //generator

//Fetch records from the database
//connect to MySQL - mysqli($mysql_host, $mysql_username, $mysql_password, $mysql_database);
//$mysqli = new mysqli('localhost','root','','vatsnew');
$mysqli = new MySQLi(DBHOST,DBUSER,DBPASS,DBNAME);

//Output any connection error
if ($mysqli->connect_error) {
    die('Error : ('. $mysqli->connect_errno .') '. $mysqli->connect_error);
}

//MySQL query, we pull records from site_contents table
//$results = $mysqli->query("SELECT id, title, content, published FROM site_contents");
$results   = $mysqli->query("SELECT title,slug,description,date as dt FROM post");


if($results){ //we have records 
    while($row = $results->fetch_object()) //loop through each row
    {
      
	  $link = "http://www.vatsnew.co.in/".$row->slug.".html";
	  $item_node = $channel_node->appendChild($xml->createElement("item")); //create a new node called "item"
      $title_node = $item_node->appendChild($xml->createElement("title", $row->title)); //Add Title under "item"
      $link_node = $item_node->appendChild($xml->createElement("link", $link)); //add link node under "item"
      
      //Unique identifier for the item (GUID)
      $guid_link = $xml->createElement("guid", $row->slug);  
      $guid_link->setAttribute("isPermaLink","false");
      $guid_node = $item_node->appendChild($guid_link); 
     
      //create "description" node under "item"
      $description_node = $item_node->appendChild($xml->createElement("description"));  
      
      //fill description node with CDATA content
      $description_contents = $xml->createCDATASection(htmlentities($row->description));  
      $description_node->appendChild($description_contents); 
    
      //Published date
      $date_rfc = gmdate(DATE_RFC2822, strtotime($row->dt));
      $pub_date = $xml->createElement("pubDate", $date_rfc);  
      $pub_date_node = $item_node->appendChild($pub_date); 

    }
}



$td_date = date("Y-m-d  h:m:s"); 
$results   = $mysqli->query("SELECT `meta_title` as title,`tag_name` as link, `tag_name` as slug,`meta_description` as description FROM `tag`");
			 //title
			 //link
			 //slug
			 //description.    /jobs/govt-job.html
			 
if($results){ //we have records 
    while($row = $results->fetch_object()) //loop through each row
    {
      
	  $link = "http://www.vatsnew.co.in/tag/".lcfirst($row->slug)."-jobs".".html";
	  $item_node = $channel_node->appendChild($xml->createElement("item")); //create a new node called "item"
      $title_node = $item_node->appendChild($xml->createElement("title", $row->title)); //Add Title under "item"
      $link_node = $item_node->appendChild($xml->createElement("link", $link)); //add link node under "item"
      
     //Unique identifier for the item (GUID)
      $guid_link = $xml->createElement("guid", $row->slug);  
      $guid_link->setAttribute("isPermaLink","false");
      $guid_node = $item_node->appendChild($guid_link); 
     
      //create "description" node under "item"
      $description_node = $item_node->appendChild($xml->createElement("description"));  
      
      //fill description node with CDATA content
      $description_contents = $xml->createCDATASection(htmlentities($row->description));  
      $description_node->appendChild($description_contents); 
    
      //Published date
      $date_rfc = gmdate(DATE_RFC2822, strtotime($td_date));
      $pub_date = $xml->createElement("pubDate", $date_rfc);  
      $pub_date_node = $item_node->appendChild($pub_date); 

    }
}



//Category RSS
$td_date = date("Y-m-d  h:m:s"); 
$results   = $mysqli->query("SELECT `meta_title` as title,`slug` as link, `slug` as slug,`meta_description` as description FROM `category`");
			 //title
			 //link
			 //slug
			 //description.    /jobs/govt-job.html
			 
if($results){ //we have records 
    while($row = $results->fetch_object()) //loop through each row
    {
      
	  $link = "http://www.vatsnew.co.in/jobs/".lcfirst($row->slug).".html";
	  $item_node = $channel_node->appendChild($xml->createElement("item")); //create a new node called "item"
      $title_node = $item_node->appendChild($xml->createElement("title", $row->title)); //Add Title under "item"
      $link_node = $item_node->appendChild($xml->createElement("link", $link)); //add link node under "item"
      
     //Unique identifier for the item (GUID)
      $guid_link = $xml->createElement("guid", $row->slug);  
      $guid_link->setAttribute("isPermaLink","false");
      $guid_node = $item_node->appendChild($guid_link); 
     
      //create "description" node under "item"
      $description_node = $item_node->appendChild($xml->createElement("description"));  
      
      //fill description node with CDATA content
      $description_contents = $xml->createCDATASection(htmlentities($row->description));  
      $description_node->appendChild($description_contents); 
    
      //Published date
      $date_rfc = gmdate(DATE_RFC2822, strtotime($td_date));
      $pub_date = $xml->createElement("pubDate", $date_rfc);  
      $pub_date_node = $item_node->appendChild($pub_date); 

    }
}


//Qualification RSS
$td_date = date("Y-m-d  h:m:s"); 
$results   = $mysqli->query("SELECT `meta_title` as title,`slug` as link, `slug` as slug,`meta_description` as description FROM `qualification`");
			 //title
			 //link
			 //slug
			 //description.    /jobs/govt-job.html
			 
if($results){ //we have records 
    while($row = $results->fetch_object()) //loop through each row
    {
      
	  $link = "http://www.vatsnew.co.in/hiring/".lcfirst($row->slug).".html";
	  $item_node = $channel_node->appendChild($xml->createElement("item")); //create a new node called "item"
      $title_node = $item_node->appendChild($xml->createElement("title", $row->title)); //Add Title under "item"
      $link_node = $item_node->appendChild($xml->createElement("link", $link)); //add link node under "item"
      
     //Unique identifier for the item (GUID)
      $guid_link = $xml->createElement("guid", $row->slug);  
      $guid_link->setAttribute("isPermaLink","false");
      $guid_node = $item_node->appendChild($guid_link); 
     
      //create "description" node under "item"
      $description_node = $item_node->appendChild($xml->createElement("description"));  
      
      //fill description node with CDATA content
      $description_contents = $xml->createCDATASection(htmlentities($row->description));  
      $description_node->appendChild($description_contents); 
    
      //Published date
      $date_rfc = gmdate(DATE_RFC2822, strtotime($td_date));
      $pub_date = $xml->createElement("pubDate", $date_rfc);  
      $pub_date_node = $item_node->appendChild($pub_date); 

    }
}




//Area RSS
$td_date = date("Y-m-d  h:m:s"); 
$results   = $mysqli->query("SELECT `meta_title` as title,`slug` as link, `slug` as slug,`meta_description` as description FROM `area`");
			 //title
			 //link
			 //slug
			 //description.    /jobs/govt-job.html
			 
if($results){ //we have records 
    while($row = $results->fetch_object()) //loop through each row
    {
      
	  $link = "http://www.vatsnew.co.in/hiring/".lcfirst($row->slug).".html";
	  $item_node = $channel_node->appendChild($xml->createElement("item")); //create a new node called "item"
      $title_node = $item_node->appendChild($xml->createElement("title", $row->title)); //Add Title under "item"
      $link_node = $item_node->appendChild($xml->createElement("link", $link)); //add link node under "item"
      
     //Unique identifier for the item (GUID)
      $guid_link = $xml->createElement("guid", $row->slug);  
      $guid_link->setAttribute("isPermaLink","false");
      $guid_node = $item_node->appendChild($guid_link); 
     
      //create "description" node under "item"
      $description_node = $item_node->appendChild($xml->createElement("description"));  
      
      //fill description node with CDATA content
      $description_contents = $xml->createCDATASection(htmlentities($row->description));  
      $description_node->appendChild($description_contents); 
    
      //Published date
      $date_rfc = gmdate(DATE_RFC2822, strtotime($td_date));
      $pub_date = $xml->createElement("pubDate", $date_rfc);  
      $pub_date_node = $item_node->appendChild($pub_date); 

    }
}



//Role RSS
$td_date = date("Y-m-d  h:m:s"); 
$results   = $mysqli->query("SELECT `meta_title` as title,`slug` as link, `slug` as slug,`meta_description` as description FROM `role`");
			 //title
			 //link
			 //slug
			 //description.    /jobs/govt-job.html
			 
if($results){ //we have records 
    while($row = $results->fetch_object()) //loop through each row
    {
      
	  $link = "http://www.vatsnew.co.in/opening/".lcfirst($row->slug).".html";
	  $item_node = $channel_node->appendChild($xml->createElement("item")); //create a new node called "item"
      $title_node = $item_node->appendChild($xml->createElement("title", $row->title)); //Add Title under "item"
      $link_node = $item_node->appendChild($xml->createElement("link", $link)); //add link node under "item"
      
     //Unique identifier for the item (GUID)
      $guid_link = $xml->createElement("guid", $row->slug);  
      $guid_link->setAttribute("isPermaLink","false");
      $guid_node = $item_node->appendChild($guid_link); 
     
      //create "description" node under "item"
      $description_node = $item_node->appendChild($xml->createElement("description"));  
      
      //fill description node with CDATA content
      $description_contents = $xml->createCDATASection(htmlentities($row->description));  
      $description_node->appendChild($description_contents); 
    
      //Published date
      $date_rfc = gmdate(DATE_RFC2822, strtotime($td_date));
      $pub_date = $xml->createElement("pubDate", $date_rfc);  
      $pub_date_node = $item_node->appendChild($pub_date); 

    }
}




$xml->save("rss.xml");

header("location:http://www.vatsnew.co.in/rss.xml");
?>
